<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	
	<a id="deconnexion" href="deconnexion">deconnexion</a>
	
	<div class="row" id="headerMenu">
	<c:choose>
		<c:when test="${sessionScope.type.equals(\"eleve\") }">
			
			<ul class="menu">
				<li><a href="accueil">Home</a></li>
				<li><a href="RechercheStage">Recherche de stage </a></li>
				<li><a href="#">Mon stage</a></li>
				<li>
					<a href="#">Feedback </a>
					<ul class="hidden">
						<li><a href="#">ajouter un feedback</a></li>
						<li><a href="#">consulter mes feedback</a></li>
					</ul>
				</li>
				
				<li><a href="#">profil</a></li>
			</ul>
		</c:when>
		<c:when test="${sessionScope.type.equals(\"professeur\") }">
			<ul class="menu">
				<li><a href="accueil">Home</a></li>
				<li>
					<a href="#">Administation</a>
					<ul class="hidden">
						<li><a href="FormContact">Contact</a></li>
						<li><a href="FormEleve">Eleve</a></li>
						<li><a href="GestionnaireFormulaire">Formulaire</a></li>
						<li><a href="SaisieEntreprise">Entreprise</a></li>
						
					</ul>
				
				</li>
				<li><a href="Planning">Planning</a></li>
				<li>
					<a href="#">Stage </a>
					<ul class="hidden">
						<li><a href="FormPropositionDeStage">ajouter une proposition de stage</a></li>
						<li><a href="FormulaireStage">ajouter un stage</a></li>
						<li><a href="Evaluations"> consulter les �valuations</a></li>
					</ul>
				</li>
				<li>
					<a href="Recherche">Recherche</a>
					<ul class="hidden">
						<li><a href="RechercheContact">Contact</a></li>
						<li><a href="RechercheStage">Stage</a></li>
						<li><a href="RechercheEntreprise">Entreprise</a></li>
					</ul>
				</li>
				
				<li><a href="#">Profil</a></li>
			</ul>
		</c:when>
		<c:otherwise>
			<ul class="menu">
				<li><a href="accueil">Home</a></li>
				<li><a href="#">Recherche de stage </a></li>
				<li><a href="#">Mon stage</a></li>
				<li>
					<a href="#">Feedback </a>
					<ul class="hidden">
						<li><a href="#">ajouter un feedback</a></li>
						<li><a href="#">condulter mes feedback</a></li>
					</ul>
				</li>
				
				<li><a href="#">profil</a></li>
			</ul>
		
		</c:otherwise>
		
	
	
	
	
	</c:choose>
	</div>