<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="css/foundation.css">

<link rel="stylesheet" href="css/app.css">
<link rel="stylesheet" href="css/header.css"/>
<link rel="stylesheet" href="css/all.css"/>
<link rel="stylesheet" href="css/style.css"/>


<title>Saisie Entreprise</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="modif" id="editEntr">
		<h2>Ajout d'une entreprise</h2>
		<form method="POST" action="addEntreprise?location=SaisieEntreprise">
			<jsp:include page="FormEntreprise.jsp" />	
		</form>
	</div>
</body>
</html>