<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>

    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/header.css"/>
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/style.css"/>

</head>
<body>
<jsp:include page="header.jsp"/>
<div class="row" id="ajProp">


 <form method="POST" action="ajouterProp">	
<!-- <form method="POST" action="upload" enctype="multipart/form-data">-->
	<fieldset class="fieldset">
		<legend>description du stage</legend>	
	<div class="row">
		 <label>Sujet*
    		<textarea name="sujet"placeholder="Les interfaces graphiques en java" required></textarea>
  		</label>
	</div>
	<div class="row">
		 <label>Description
    			<textarea name="description"placeholder="Les interfaces graphiques en java"></textarea>
  		</label>
	</div>
	<div class="row">
		<div class="small-4 colums">
			 <label>Annee*
     				<input type="number" name="annee" placeholder="2018" required min="2017">
   			</label>
  			</div>
  			
  			
  			<!--  <div class="large-6 colums">
  				<label for="fichier" class="button">ajouter un fichier</label>
				<input type="file" id="fichier" class="show-for-sr">
  			</div>-->
	</div>
	<div class="row">
			<div class="large-12 columns" id="listeEntreprise">
			<fieldset class="fieldset">
 				<legend>Entreprise</legend>
 				<div class="row" id="scroll">
	 				<c:forEach var="ent" items="${entreprise}">
	 					
					<div class="large-6 columns">
						<input type="radio" name="Entreprise" value="${ent.getIdEntreprise()}" id="${ent.getIdEntreprise()}" required>
						<label for="${ent.getIdEntreprise()}">${ent.getNom()}</label>
					</div>		
					</c:forEach>
 				</div>
			</fieldset>
			</div>
 	</div>
 	</fieldset>
	<div class="row" id="ajoutPropo">
		<div class="small-3 small-centered columns">
         <input type="submit"  class="button expand" value="Ajouter"/>
       </div>
	</div>	
</form>
</div>


                <%--<div class="large-6 colums">--%>
                    <%--<label for="fichier" class="button">ajouter un fichier</label>--%>
                    <%--<input type="file" id="fichier" class="show-for-sr">--%>
                <%--</div>--%>
            </div>
            <div class="row">
                <div class="large-12 columns" id="listeEntreprise">
                    <fieldset class="fieldset">
                        <legend>Entreprise</legend>
                        <div class="row" id="scroll">
                            <c:forEach var="ent" items="${entreprise}">

                                <div class="large-6 columns">
                                    <input type="radio" name="Entreprise" value="${ent.getIdEntreprise()}"
                                           id="${ent.getIdEntreprise()}" required>
                                    <label for="${ent.getIdEntreprise()}">${ent.getNom()}</label>
                                </div>
                            </c:forEach>
                        </div>
                    </fieldset>
                </div>
            </div>
        </fieldset>
        <div class="row" id="ajoutPropo">
            <div class="small-3 small-centered columns">
                <input type="submit" class="button expand" value="Ajouter"/>
            </div>
        </div>
    </form>
</div>


<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>

<script src="js/app.js"></script>
</body>
</html>