<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/header.css"/>
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/style.css"/>

    <title>Evaluation</title>
</head>

<body>
<jsp:include page="header.jsp"/>
<!--  <div class="listeEva">
		<div class="row">
  			<div class="medium-3 columns">
				<ul class="tabs vertical" id="descEva" data-tabs>
					<c:forEach var="eva" items="${eval}" varStatus="count">
					<c:choose>
						<c:when test="${ count.count==1}">
							<li class="tabs-title is-active"><a href="#panel${count.count}" aria-selected="true">Evaluation de ${eva.getConserneMaitreDeStage().getNom()} ${eva.getConserneMaitreDeStage().getPrenom()} </a></li>
						</c:when>
						<c:otherwise>
							<li class="tabs-title"><a href="#panel${count.count}">Evaluation de ${eva.getConserneMaitreDeStage().getNom()} ${eva.getConserneMaitreDeStage().getPrenom()}</a></li>
						</c:otherwise>
					
					</c:choose>
					</c:forEach>
			    </ul>
			</div>
			<div class="medium-9 columns">
				<div class="tabs-content vertical" data-tabs-content="descEva">
					<c:forEach var="eva2" items="${eval}" varStatus="count2">
					<c:choose>
						<c:when test="${ count2.count==1}">
							 <div class="tabs-panel is-active" id="#panel${count2.count}">
        						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      						</div>
						</c:when>
						<c:otherwise>
							 <div class="tabs-panel" id="#panel${count2.count}">
        						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      						</div>							
						</c:otherwise>
					
					</c:choose>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
-->

</body>
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>

<script src="js/app.js"></script>
</html>