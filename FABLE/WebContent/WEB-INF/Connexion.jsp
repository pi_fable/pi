<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/all.css">
    <title>connexion</title>
</head>
<body>
<div class="large-3 large-centered columns">
    <div class="login-box">
        <div class="row">
            <div class="large-12 columns">
                <form action="page2" method="POST">
                    <div class="row">
                        <div class="large-12 columns">
                            <input type="email" name="username" id="mail" placeholder="Username"
                                   value="admin.admin@hers.be"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <input type="password" name="password" placeholder="Password" value="admin"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 large-centered columns">
                            <input type="submit" class="button expand" value="Log In"/>
                        </div>
                    </div>
                    <c:choose>
                        <c:when test="${empty erreur}">
                            <div class="alert callout" id="box_error" style="display:none;">
                                <p id="erreur">${erreur}</p>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="alert callout" id="box_error" style="display:block;">
                                <p id="erreur">${erreur}</p>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/app.js"></script>
</body>
</html>