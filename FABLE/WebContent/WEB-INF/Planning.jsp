<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/foundation-datepicker.css">
    <link rel="stylesheet" href="css/header.css"/>
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/planning.js"></script>

    <title>Planning</title>
</head>
<body>

<c:choose>
    <c:when test="${!empty EvenMod}">
        <div class="modif" id="changeEvent">
            <form method="POST" action="modifEvent">
                <h3>modification d'un �v�nement</h3>
                <div class="row">
                    <div class="small-6 columns">
                        <div class="row">
                            <div class="small-6 columns">
                                <label>N� EVENEMENT
                                    <input type="text" name="idEven" value="${EvenMod.getNumEven()}" readonly>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-6 columns">
                                <label>Date:*
                                    <input type="text" name="date" class="span2" value="${EvenMod.getDateEven()}"
                                           id="dp1" required>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Sujet*
                                    <textarea name="sujet" placeholder="Les interfaces graphiques en java"
                                              required>${EvenMod.getSujet() }</textarea>
                                </label>

                            </div>
                        </div>
                    </div>
                    <div class="small-6 columns">
                        <fieldset class="fieldset">
                            <legend>Adresse*</legend>
                            <div class="row collapse">

                                <div class="small-6 columns">

                                    <input type="number" name="numRue" placeholder="n� rue *"
                                           value="${EvenMod.getAdrs().getNumRue() }" required>

                                </div>
                                <div class="small-6 columns">

                                    <input type="text" name="rue" placeholder="rue *"
                                           value="${EvenMod.getAdrs().getRue() }" required>

                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="small-6 columns">
                                    <input type="number" name="CodePostal" placeholder="code postal *"
                                           value="${EvenMod.getAdrs().getCodePostal() }" required></br>
                                </div>
                                <div class="small-6 columns">

                                    <input type="text" name="ville" placeholder="ville *"
                                           value="${EvenMod.getAdrs().getVille() }" required>

                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-6 large-centered columns">
                                    <input type="text" name="pays" placeholder="pays *"
                                           value="${EvenMod.getAdrs().getPays() }" required>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Description
                            <textarea name="desc"
                                      placeholder="comprendre le mod�le mvc ...">${EvenMod.getDescription() } </textarea>
                        </label>
                    </div>
                </div>
                <div class="row">

                    <div class="large-12 columns" id="listeContact">
                        <fieldset class="fieldset">
                            <legend>Contact</legend>
                            <div class="row" id="scroll">
                                <c:forEach var="contact" items="${Contact}" varStatus="cpt">

                                <div class="large-6 columns">
                                    <c:set var="find" value="false"/>
                                    <c:forEach var="test" items="${EvenMod.getListContact() }" varStatus="status">
                                        <c:if test="${test.equals(contact)}">
                                            <c:set var="status.index" value="${EvenMod.getListContact().size() }"/>
                                            <c:set var="find" value="true"/>
                                        </c:if>
                                    </c:forEach>

                                    <c:choose>
                                        <c:when test="${find}">
                                            <input type="checkbox" name="Contact" value="${contact.getIdContact()}"
                                                   id="${contact.getIdContact()}" display="none" checked>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" name="Contact" value="${contact.getIdContact()}"
                                                   id="${contact.getIdContact()}" display="none">
                                        </c:otherwise>

                                    </c:choose>

                                    <label for="${contact.getIdContact()}">${contact.getNom()} ${contact.getPrenom()}</label>
                                </div>

                                </c:forEach>
                        </fieldset>


                    </div>
                </div>

                <div class="row">
                    <div class="large-12 large-centered columns">
                        <input href="" type="submit" class="button expand" value="modifier"/>
                    </div>
                </div>

            </form>
        </div>


    </c:when>

    <c:otherwise>
        <jsp:include page="header.jsp"/>
        <a id="ajouterEvenement" data-open="ajouterEvent">
            <img src="Image/AddEvent.png" id="addEvent" alt="Ajouter">
        </a>
        <div class="large reveal" data-reveal id="ajouterEvent">
            <form method="POST" action="addEvent">
                <h3>Ajout d'un �v�nement</h3>
                <div class="row">
                    <div class="small-6 columns">
                        <div class="row">
                            <div class="small-6 columns">
                                <label>Date:*
                                    <input type="text" name="date" class="span2" id="dp1" required>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Sujet*
                                    <textarea name="sujet" placeholder="Les interfaces graphiques en java"
                                              required></textarea>
                                </label>

                            </div>
                        </div>
                    </div>
                    <div class="small-6 columns">
                        <fieldset class="fieldset">
                            <legend>Adresse*</legend>
                            <div class="row collapse">

                                <div class="small-6 columns">

                                    <input type="number" name="numRue" placeholder="n� rue *" required>

                                </div>
                                <div class="small-6 columns">

                                    <input type="text" name="rue" placeholder="rue *" required>

                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="small-6 columns">
                                    <input type="number" name="CodePostal" placeholder="code postal *" required></br>
                                </div>
                                <div class="small-6 columns">

                                    <input type="text" name="ville" placeholder="ville *" required>

                                </div>
                            </div>
                            <div class="row collapse">
                                <div class="large-6 large-centered columns">
                                    <input type="text" name="pays" placeholder="pays *" required>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Description
                            <textarea name="desc" placeholder="comprendre le mod�le mvc ..."></textarea>
                        </label>
                    </div>
                </div>
                <div class="row">

                    <div class="large-12 columns" id="listeContact">
                        <fieldset class="fieldset">
                            <legend>Contact</legend>
                            <div class="row" id="scroll">
                                <c:forEach var="contact" items="${Contact}" varStatus="cpt">

                                <div class="large-6 columns">
                                    <input type="checkbox" name="Contact" value="${contact.getIdContact()}"
                                           id="${contact.getIdContact()}" display="none">
                                    <label for="${contact.getIdContact()}">${contact.getNom()} ${contact.getPrenom()}</label>
                                </div>

                                </c:forEach>
                        </fieldset>


                    </div>
                </div>

                <div class="row">
                    <div class="large-12 large-centered columns">
                        <input href="" type="submit" class="button expand" value="Ajouter"/>
                    </div>
                </div>

            </form>
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="row">
            <div class="columns" id="filtre">
                <form action="Planning">
                    <h4>FILTRER</h4>
                    <div class="head">
                        <p>FILTRER PAR SUJET</p>
                    </div>
                    <div class="param">
                        <input type="search" name="subject" value="${sujet}" placeholder="java" id="subject">
                    </div>
                    <div class="head">
                        <p>FILTRER PAR ANNEE</p>
                    </div>
                    <div class="param">
                        <input type="number" name="year" value="${Year }" placeholder="2017" id="year">
                    </div>
                    <div class="head">
                        <p>FILTRER PAR PERIODE </p>
                    </div>
                    <div class="param">
                        <input type="text" name="dateDeb" value="${dateDeb}" placeholder="01-01-2017" id="dp2">
                        <input type="text" name="dateFin" value="${dateFin }" placeholder="01-06-2017" id="dp3">
                    </div>
                    <div class="row" id="filtrePlan">
                        <input type="submit" class="button expand" value="rechercher"/>
                        <input type="button" class="button expand" value="reset" onclick="reset();"/>
                    </div>
                </form>
            </div>
            <div class="columns" id="planning">
                <c:forEach var="event" items="${Planning}" varStatus="count">

                    <div class="large-6 columns">

                        <a href="editEvent?id=${event.getNumEven()}" id="modifierEvent" data-open="modifierEvent">
                            <img src="Image/modifier.png" id="modEvent" alt="modifier">
                        </a>
                        <a href="suppEvent?id=${event.getNumEven()}" id="supprimerEvent">
                            <img src="Image/supprimer.png" id="supEvent" alt="supprimer">
                        </a>

                        <article class="event" id="${event.getSujet()}">

                            <div class="event-date">
                                <p class="event-month">${event.getMois()}</p>
                                <p class="event-day">${event.getJour()}</p>
                            </div>

                            <div class="event-desc">
                                <h4 class="event-desc-header">${event.getSujet()}</h4>
                                <p class="event-desc-detail"><span
                                        class="event-desc-time"></span>${event.getDescription()}</p>

                                <div id="hidden${count.count}" style="display:none;">
                                    <p class="information">
                                        <strong><h6>Lieu:</h6></strong>
                                        </br>
                                            ${event.getAdrs().getNumRue()}
                                        rue ${event.getAdrs().getRue()}
                                        </br>
                                            ${event.getAdrs().getCodePostal()}
                                            ${event.getAdrs().getVille()}

                                    </p>
                                    <p class="information">
                                        <strong>Personnes Concern�es:</strong></br>
                                        <c:forEach var="participant" items="${event.getListContact()}">

                                            ${participant.getNom()}
                                            ${participant.getPrenom()}
                                            <c:if test="${!participant.getEntrep().getNom().equals('')}">
                                                ${participant.getEntrep().getNom()}
                                            </c:if>
                                            </br>
                                        </c:forEach>
                                    </p>
                                </div>


                                <a class="detailbutton" onclick="detail('${count.count}');"><img
                                        src="Image/FlecheBas.png" id="button${count.count}" alt="detail"> </a>

                            </div>

                        </article>
                        <hr>
                    </div>


                </c:forEach>

            </div>
        </div>
    </c:otherwise>
</c:choose>


</body>
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>

<script src="js/app.js"></script>
<script src="js/foundation-datepicker.js"></script>


<script>
    $(function () {
        $('#dp1').fdatepicker({
            initialDate: '01-01-2017',
            format: 'dd-mm-yyyy',
            disableDblClickSelection: true,
            leftArrow: '<<',
            rightArrow: '>>',
            closeIcon: 'X',
            closeButton: true
        });
    });
    $(function () {
        $('#dp2').fdatepicker({

            format: 'dd-mm-yyyy',
            disableDblClickSelection: true,
            leftArrow: '<<',
            rightArrow: '>>',
            closeIcon: 'X',
            closeButton: true
        });
    });
    $(function () {
        $('#dp3').fdatepicker({

            format: 'dd-mm-yyyy',
            disableDblClickSelection: true,
            leftArrow: '<<',
            rightArrow: '>>',
            closeIcon: 'X',
            closeButton: true
        });
    });


</script>
</html>
