<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/header.css"/>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/style.css"/>

    <script src="js/Verification.js"></script>
    <title>Formulaire ${type}</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	
	<div id="ajoutContact">
	<form method="POST" action="addContact">
		<div class="row">
		<fieldset class="fieldset">
		  		<legend>Information personnelle</legend>
		<div class="row">
			<div class="large-6 columns">
				<input type="text" name="nom" placeholder="nom *" required>
			</div>
			<div class="large-6 columns">
				<input type="text" name="prenom" placeholder="prenom *" required>
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<input type="tel" name="telephone" placeholder="telephone">
			</div>
			<div class="large-6 columns">
				<input type="email" name="email" placeholder="email " id="email">
				
			</div>
		</div>
		<div class="row">
		
			<div class="large-6 columns">
				<input type="text" name="Poste" placeholder="Poste " >
			</div>
			<div class="large-6 columns">
				<c:choose>
				<c:when test="${type=='Contact'}">
					En faire un maitre de stage:
					oui<input type="radio" name="MaitreDeStage" onclick="setMaitreDeStage();"></br>
				</c:when>
				<c:otherwise>
					<input type="radio" name="MaitreDeStage" display="none"></br>
					<script>setMaitreDeStage();</script>
				</c:otherwise>
			
			</c:choose>
			</div>
		</div>
		</fieldset>
		</div>
		<div class="row">
		 	<div class="large-12 columns" id="listeEntreprise">
		 		<fieldset class="fieldset">
		  			<legend>Entreprise</legend>
		  			<a id="addEntreprise" data-open="newEntreprise"><img src="Image/ajouter.png" alt="+"></a>
		  			<div class="row" id="scroll">
		  				<c:forEach var="ent" items="${ListeEntreprise}">
		  					
							<div class="large-6 columns">
								<input type="radio" name="Entreprise" value="${ent.getIdEntreprise()}" id="${ent.getIdEntreprise()}">
								<label for="${ent.getIdEntreprise()}">${ent.getNom()}</label>
							</div>		
						</c:forEach>
		  			</div>
		  			
		  			
				</fieldset>
		 	</div>
		</div>
			<div class="row">
	 		<div class="large-12 large-centered columns">
	          <input type="submit"  class="button expand" value="Ajouter"/>
	        </div>
 			</div>	
	</form>
	</div>
	<div class="large reveal"  data-reveal id="newEntreprise">
		<h2>Ajout d'une entreprise</h2>
		<form method="POST" action="addEntreprise?location=FormContact">
			<jsp:include page="FormEntreprise.jsp" />	
		</form>
		<button class="close-button" data-close aria-label="Close modal" type="button">
    		<span aria-hidden="true">&times;</span>
  		</button>
	</div>
	
	
	
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>

<script src="js/app.js"></script>
</body>
</html>