<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/foundation-datepicker.css">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/header.css"/>
    <link rel="stylesheet" href="css/all.css"/>
    <script src="js/planning.js"></script>
    <title>Ajout Formulaire</title>
</head>
<body>
<jsp:include page="header.jsp"/>
<p></p>
<div class="row">
    <div class="small-9 columns small-centered">
        <div class="top-bar">
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <li class="menu-text">Gestionnaire Formulaire</li>
                    <li>
                        <form action="saisieFormulaire" method="get">
                            <button class="button" type="submit" data-toggle="example-dropdown-1">Creer Formulaire
                            </button>
                        </form>
                    </li>
                    <li>
                        <form action="listerFormulaire" method="get">
                            <button class="button" type="submit" data-toggle="example-dropdown-1">Lister formulaire
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="top-bar-right">
                <form action="rechercherFormulaire" method="get">
                    <input type="text" name="motCle" value="${bean.mc }"/>
                    <button type="submit">Chercher</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="small-9 columns small-centered">
        <form action="ajoutFormulaire">
            <table>
                <tr>
                    <th>Cr�ation Formulaire</th>
                    <th></th>
                </tr>
                <tr>
                    <td>TITRE</td>
                    <td><input type="text" name="titre" value="${bean.form.titre }"></td>
                </tr>
                <tr>
                    <td>PREIODE</td>
                    <td><input type="text" name="periode" value="${bean.form.periode }"></td>
                </tr>
                <tr>
                    <td>DATE LIMITE</td>
                    <td>
                        <label>Date:*
                            <input type="text" name="date" class="span2" value="10-3-2017" id="dp1" required>
                        </label>
                    </td>

                </tr>
                <tr>
                    <td></td>
                    <td>
                        <form action="ajoutFormulaire" method="get">
                            <button class="button" type="submit" data-toggle="example-dropdown-1">Ajouter formulaire
                            </button>
                        </form>
                    </td>
                </tr>
                <%-- <c:forEach items="${bean.listForm}" var="p">
                      <tr>
                          <td>${p.idForm }</td>
                          <td>${p.titre}</td>
                          <td>${p.periode}</td>
                          <td>${p.dateLimite }</td>
                      </tr>
                  </c:forEach> --%>
            </table>
        </form>
        <hr>
    </div>
</div>
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>

<script src="js/app.js"></script>
<script src="js/foundation-datepicker.js"></script>


<script>
    $(function () {
        $('#dp1').fdatepicker({
            initialDate: '01-01-2017',
            format: 'dd-mm-yyyy',
            disableDblClickSelection: true,
            leftArrow: '<<',
            rightArrow: '>>',
            closeIcon: 'X',
            closeButton: true
        });
    });
</script>
</body>
</html>
l>