<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


	
		<div class="row">
			<div class="small-6 columns">
				<input type="text" name="nomEnt" id="nomEnt" placeholder="nom *" required>
			</div>
			<div class="small-6 columns">
				<input type="text" name="urlSite" placeholder="URL SITE " >
			</div>
		</div>
		<div class="row">
			<fieldset class="fieldset">
					<legend>Adresse*</legend>
					<div class="row collapse">
						
						<div class="small-6 columns">
						
						<input type="number" name="numRue" placeholder="n� rue *" required>
						
						</div>
						<div class="small-6 columns">
						
						<input type="text" name="rue" placeholder="rue *" required>
						
						</div>
					</div>	
					<div class="row collapse">
						<div class="small-6 columns">
							<input type="number" name="CodePostal" placeholder="code postal *" required></br>
						</div>
						<div class="small-6 columns">
						
							<input type="text" name="ville" placeholder="ville *" required>
						
						</div>
					</div>
					<div class="row collapse">
						<div class="large-6 large-centered columns">
							<input type="text" name="pays" placeholder="pays *" required>
						</div>
					</div>
				</fieldset>	
		
		</div>
		<div class="row">
		 	<div class="large-12 columns" id="listeSecteur">
		 		<fieldset class="fieldset">
		  			<legend>Secteurs d'activit�</legend>
		  			<a id="addActivite" data-open="newSecteur"><img src="Image/ajouter.png" alt="+"></a>
		  			<div class="small reveal"  data-reveal id="newSecteur" data-multiple-opened="true">
						<div class="row">
							<div class="small-12 columns">
								<input type="text" name="SecteurActivite" placeholder="secteur *" value="" id="sectAct">
							</div>
						</div>
						<button id="btnSecteur" class="close-button" data-close aria-label="Close modal" type="button">
    						<span aria-hidden="true">&times;</span>
  						</button>
  					<div class="row">
	 					<div class="large-12 large-centered columns">
	          				<input type="button"  class="button expand" value="Ajouter" onclick="addSecteur();"/>
	        			</div>
 					</div>	
					</div>
		  			<div class="row" id="scroll1">
		  				<c:forEach var="sect" items="${ListeSecteur}">
		  					
							<div class="small-6 columns">
								<input type="checkbox" name="secteur" value="${sect.getIdActivite()}" id="act${sect.getIdActivite()}" >
								<label for="act${sect.getIdActivite()}">${sect.getNomActivite()}</label>
							</div>		
						</c:forEach>
		  			</div>
				</fieldset>
		 	</div>
		</div>
		<div class="row">
		 	<div class="large-12 columns" id="listeTechnologie">
		 		<fieldset class="fieldset" id="tests">
		  			<legend>Technologie</legend>
		  			<a id="addTechnologie" data-open="newTechnologie"><img src="Image/ajouter.png" alt="+"></a>
		  			<div class="small reveal"  data-reveal id="newTechnologie" data-multiple-opened="true">
						<div class="row">
							<div class="small-12 columns">
								<input type="text" name="Technologie" placeholder="technologie *" value="" id="techno">
							</div>
						</div>
						<button id="btnTech" class="close-button" data-close aria-label="Close modal" type="button">
				    		<span aria-hidden="true">&times;</span>
				  		</button>
				  		<div class="row">
					 		<div class="large-10 large-centered columns">
					          <input type="button"   class="button expand" value="Ajouter" onclick="addTechnologie();"/>
					        </div>
				 		</div>	
					</div>
		  			<div class="row" id="scroll2">	
		  				<c:forEach var="tech" items="${ListeTechnologie}">
		  					
							<div class="small-6 columns">
								<input type="checkbox" name="Technologie" value="${tech.getIdTechnologie()}" id="tech${tech.getIdTechnologie()}">
								<label for="tech${tech.getIdTechnologie()}">${tech.getNom()}</label>
							</div>		
						</c:forEach>
		  			</div>
				</fieldset>
		 	</div>
		</div>
		<div class="row">
	 		<div class="large-12 large-centered columns">
	          <input type="submit"  class="button expand" value="Ajouter"/>
	        </div>
 		</div>	
		
		
		
	