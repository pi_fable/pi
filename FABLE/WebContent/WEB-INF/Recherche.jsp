<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <link rel="stylesheet" href="css/app.css">
        <link rel="stylesheet" href="css/foundation.css">

        <link rel="stylesheet" href="css/header.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/accordion.css">
        <link rel="stylesheet" href="css/all.css"/>

        <!-- avec du JS accordion devient box et vise versa en fonction de l'affichage demandé par l'utilisateur -->

        <title>Recherche de stage</title>
    </head>
<body onload="refresh();">
<jsp:include page="header.jsp"/>


<div class="off-canvas position-left" id="offCanvas" data-off-canvas data-close-on-click="false">
    <!-- Your menu or Off-canvas content goes here -->
    <button class="close-button" aria-label="Close menu" type="button" onclick="hide(true);" data-close>
        <span aria-hidden="true">&times;</span>
    </button>
    <!-- Menu -->
    <div class="bgd">
    <h4>Recherche</h4>
    <div><p>Recherche</p></div>
        <form action="<% request.getServletPath(); %>">
            <div>
                <input type="search" name="mc" value="${mc}" placeholder="Entreprise" id="mc">
            </div>

            <div><p>Annee</p></div>
            <div>
                <input type="number" name="year" value="${Year }" placeholder="2017" id="year">
            </div>

            <div><p>Technologie</p></div>
            <div>
                <ul class="checkboxList">
                    <c:forEach var="techno" items="${tech}">
                        <li><input  type=checkbox name="tech" >${techno.getNom()}</li>
                    </c:forEach>
                </ul>
            </div>

            <div><p>Localité</p></div>
            <div>
                <ul class="radioList">
                    <li><input  type=radio name="pays" id="Belgique">Belgique</li>
                    <li><input  type=radio name="pays" id="France">France</li>
                    <li><input  type=radio name="pays" id="Luxembourg">Luxembourg</li>
                    <%--Autre--%>
                    <li>
                        <input type="radio" name="pays" value="autre" id="autre"><input type="text" id="autre2" name="autre" onclick="swap();"></li>
                </ul>
            </div>
                <button type="submit" class="button" onclick="hide(true);" data-close>Search</button>
        </form>
    </div>
</div>
<div class="off-canvas-content" data-off-canvas-content>
    <!-- Your page content lives here -->
    <div>

        <form class="stickyRight" id="btn" action="<% request.getServletPath(); %>">
            <a class="btnElem" data-toggle="offCanvas" onclick="hide(false);">Filtre</a>
            <a id="displayCP" class="btnElem">0 / 2</a>

            <button type="button" class="button btnElem" disabled id="cpButton" onclick="showCP();"
                data-open="showCP">Comparer
            </button>

        </form>
    </div>
</div>


<div class="displayList">
    <c:choose>
        <c:when test="${Type.equals('Entreprise')}">
            <div class="tab-group">
                <c:forEach var="entrep" items="${List}">
                    <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                        <li class="accordion-item " data-accordion-item id="${entrep.idEntreprise}">
                            <a class="accordion-title" onclick="add(${entrep.idEntreprise});">${entrep.nom}</a>
                            <div class="accordion-content" data-tab-content>
                                <p>Site : ${entrep.URLSite}</p>
                                <p>Adresse : ${entrep.adrs.getCodePostal()} ${entrep.adrs.getVille()}</p>
                                <p>${entrep.adrs.getRue()} ${entrep.adrs.getNumRue()}</p>
                            </div>
                        </li>
                    </ul>
                </c:forEach>
            </div>
        </c:when>

        <c:when test="${Type.equals('Stage')}">
            <div class="tab-group">
                <c:forEach var="stage" items="${List}">
                    <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                        <li class="accordion-item " data-accordion-item id="${stage.idProp}">
                            <a class="accordion-title" onclick="add(${stage.idProp});">${stage.sujet}</a>
                            <div class="accordion-content" data-tab-content>
                                <p>Entreprise : ${stage.entrep.getNom()}</p>
                                <p>Description : ${stage.description}</p>
                                <p>Année : ${stage.annee}</p>
                            </div>
                        </li>
                    </ul>
                </c:forEach>
            </div>
        </c:when>

        <c:when test="${Type.equals('Contact')}">
            <div class="tab-group">
                <c:forEach var="con" items="${List}">
                    <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                        <li class="accordion-item " data-accordion-item id="${con.idContact}">
                            <a class="accordion-title" onclick="add(${con.idContact});">${con.nom} ${con.prenom}</a>
                            <div class="accordion-content" data-tab-content>
                                <p>Num : ${con.telephone}</p>
                                <p>Mail : ${con.adresseMail}</p>
                                <p>Adresse : ${con.poste} </p>
                            </div>
                        </li>
                    </ul>
                </c:forEach>
            </div>
        </c:when>
    </c:choose>
</div>

<div class="large reveal" id="showCP" data-reveal>
    <h1>Compare</h1>
    <table id="table">
        <tr>
            <th></th>
            <th>Column 1 Heading</th>
            <th>Column 2 Heading</th>
        </tr>
        <tr>
            <td>Row 1: Col 1</td>
            <td>Row 1: Col 2</td>
            <td>Row 1: Col 3</td>
        </tr>
    </table>

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>



<script src="./js/vendor/jquery.js"></script>
<script src="./js/vendor/foundation.js"></script>
<script src="./js/app.js"></script>

<script src="./js/recherche.js"></script>


</body>
</html>
 	