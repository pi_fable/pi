<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/foundation-datepicker.css">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/all.css"/>
    <link rel="stylesheet" href="css/header.css"/>
    <script src="js/planning.js"></script>
    <title>Gestionnaire Formulaire</title>
</head>
<body>
<jsp:include page="header.jsp"/>
<p></p>
<div class="row">
    <div class="small-9 columns small-centered">
        <div class="top-bar">
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <li class="menu-text">Gestionnaire Formulaire</li>
                    <li>
                        <form action="saisieFormulaire" method="get">
                            <button class="button" type="submit" data-toggle="example-dropdown-1">Creer Formulaire
                            </button>
                        </form>
                    </li>
                    <li>
                        <form action="listerFormulaire" method="get">
                            <button class="button" type="submit" data-toggle="example-dropdown-1">Lister formulaire
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="top-bar-right">
                <form action="rechercherFormulaire" method="get">
                    <input type="text" name="motCle" value="${bean.mc }"/>
                    <button type="submit">Chercher</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="small-9 columns small-centered">
        <table>
            <tr>
                <th>ID</th>
                <th>TITRE</th>
                <th>PERIODE</th>
                <th>DATE LIMITE</th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${bean.listForm}" var="p">
                <tr>
                    <td>${p.idForm }</td>
                    <td>${p.titre}</td>
                    <td>${p.periode}</td>
                    <td>${p.dateLimite }</td>
                    <td><a href="/deleteFormulaire?id=${p.idForm}">supprimer</a></td>
                    <td><a href="/updateFormulaire?id=${p.idForm}">editer</a></td>
                </tr>
            </c:forEach>
        </table>
        <hr>
    </div>
</div>
</body>
</html>