<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/foundation-datepicker.css">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/header.css"/>
    <link rel="stylesheet" href="css/all.css"/>

</head>
<body>
<jsp:include page="header.jsp"/>
<div id="ValidationStage">
    <form method="POST" action="validerStage">
        <div class="row">
            <fieldset class="fieldset">
                <legend>${etat}</legend>
                <div class="row">
                    <label>Sujet*

                        <textarea name="sujet" placeholder="Les interfaces graphiques en java" required></textarea>
                    </label>
                </div>
                <div class="row">
                    <label>Description
                        <textarea name="description" placeholder="Les interfaces graphiques en java"></textarea>
                    </label>
                </div>
                <div class="row">
                    <div class="small-4 colums">
                        <label>Annee*
                            <input type="number" name="annee" placeholder="2018" required min="2017">
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns" id="listeEntreprise">
                        <fieldset class="fieldset">
                            <legend>Entreprise</legend>
                            <div class="row" id="scroll">
                                <c:forEach var="ent" items="${entreprise}">

                                    <div class="large-6 columns">
                                        <input type="radio" name="Entreprise" value="${ent.getIdEntreprise()}"
                                               id="${ent.getIdEntreprise()}" required>
                                        <label for="${ent.getIdEntreprise()}">${ent.getNom()}</label>
                                    </div>
                                </c:forEach>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </fieldset>


        </div>
        <div class="row">
            <fieldset class="fieldset">
                <legend>Validation du stage</legend>
                <div class="small-6 columns">
                    <label>Date de d�but
                        <input type="text" name="dateDeb" class="span2" id="dp1">
                    </label>
                </div>
                <div class="small-6 columns">
                    <label>Date de fin
                        <input type="text" name="dateFin" class="span2" id="dp2">
                    </label>
                </div>
                <div class="row">
                    <div class="large-12 columns" id="listeContact">
                        <fieldset class="fieldset">
                            <legend>Eleve</legend>
                            <div class="row" id="scroll">
                                <c:forEach var="el" items="${eleve}">

                                    <div class="large-6 columns">
                                        <input type="radio" name=eleve value="${el.getIdEleve()}"
                                               id="el${el.getIdEleve()}" required>
                                        <label for="el${el.getIdEleve()}">${el.getNom()} ${el.getPrenom()}</label>
                                    </div>
                                </c:forEach>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns" id="listeContact">
                        <fieldset class="fieldset">
                            <legend>Maitre de stage</legend>
                            <div class="row" id="scroll">
                                <c:forEach var="cont" items="${contact}">

                                    <div class="large-6 columns">
                                        <input type="radio" name="Contact" value="${cont.getIdContact()}"
                                               id="cont${cont.getIdContact()}" required>
                                        <label for="cont${cont.getIdContact()}">${cont.getNom()} ${cont.getPrenom()}</label>
                                    </div>
                                </c:forEach>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </fieldset>


        </div>
        <div class="row" id="validStage">
            <div class="small-3 small-centered columns">
                <input type="submit" class="button expand" value="Valider ce stage"/>
            </div>
        </div>


    </form>


</div>
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>

<script src="js/app.js"></script>
<script src="js/foundation-datepicker.js"></script>
<script>
    $(function () {
        $('#dp1').fdatepicker({
            initialDate: '01-01-2017',
            format: 'dd-mm-yyyy',
            disableDblClickSelection: true,
            leftArrow: '<<',
            rightArrow: '>>',
            closeIcon: 'X',
            closeButton: true
        });
    });
    $(function () {
        $('#dp2').fdatepicker({
            initialDate: '01-01-2017',
            format: 'dd-mm-yyyy',
            disableDblClickSelection: true,
            leftArrow: '<<',
            rightArrow: '>>',
            closeIcon: 'X',
            closeButton: true
        });
    });
</script>
</body>
</html>