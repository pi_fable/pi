function setMaitreDeStage() {
    var email = document.getElementById("email");
    email.required = true;
    email.placeholder = "email *";
}
function addSecteur() {
    var value = document.getElementById("sectAct").value;
    if (value != "") {
        var divToAppend = document.getElementById("scroll1");

        var div = document.createElement("div");
        div.className = "large-6 columns";
        div.innerHTML = '<input type="checkbox" name="secteur" value="' + value + '" id="' + value + '" required checked><label for="' + value + '">' + value + '</label>';

        divToAppend.appendChild(div);
        document.getElementById("btnSecteur").click();
        document.getElementById("sectAct").value = "";
    }

}
function addTechnologie() {
    var value = document.getElementById("techno").value;
    if (value != "") {
        var divToAppend = document.getElementById("scroll2");

        var div = document.createElement("div");
        div.className = "large-6 columns";
        div.innerHTML = '<input type="checkbox" name="Technologie" value="' + value + '" id="' + value + '" required checked><label for="' + value + '">' + value + '</label>';
        divToAppend.appendChild(div);
        document.getElementById("btnTech").click();
        document.getElementById("techno").value = "";
    }
}