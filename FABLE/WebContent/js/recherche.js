var cptCheckbox = 0;
var tabComparable = []; //contient les id des deux stages à comparer

function hide(value){

    var button = document.getElementById("btn");

    if(value) button.classList.remove("hide");
    else button.classList.add("hide");
}

function add(idd) {
    var elem = document.getElementById(idd);
    if (elem.classList.contains("is-active")) {
        cptCheckbox--;
        removeID(idd);
    }
    else {
        cptCheckbox++;
        tabComparable.push(idd);
    }
    isComparable();
}
function swap() {
    var radio = document.getElementById("autre");
    radio.checked = true;
}
/* retire l'id du tableau */
function removeID(idd) {
    for (var i = 0; i < tabComparable.length; i++) {
        if (tabComparable[i] == idd) {
            tabComparable[i] = 0;
        }
        if (tabComparable[i] == 0 && tabComparable.length - 1 > i) {
            tabComparable[i] = tabComparable[i + 1];
            tabComparable[i + 1] = 0;
        }
    }
    tabComparable.pop();
}

/* Fonction qui MAJ le compteur et qui rend le bouton activable */
function isComparable() {
    var nbr = document.getElementById("displayCP");
    nbr.innerText = cptCheckbox.toString() + " / 2";
    var button = document.getElementById("cpButton");

    if (cptCheckbox == 2) button.disabled = false;
    else button.disabled = true;
}

/* Fonction qui permet : on refresh, de vider le tabCompable et de disable le bouton*/
function refresh() {
    var tab = document.getElementsByName("tabs");
    var bt = document.getElementById("compareButton");
    bt.disabled = true;
    cptCheckbox = 0;
    while (tabComparable.length > 0)
        tabComparable.pop();
    for (var i = 0; i < tab.length; i++)
        tab[i].checked = false;
}

function showCP() {
    var gauche = document.getElementById(tabComparable[0]).parentNode;
    var droite = document.getElementById(tabComparable[1]).parentNode;
    onCompare(gauche, droite);
}

function onCompare(gauche, droite) {

    var gaucheC = gauche.getElementsByTagName("p");
    var droiteC = droite.getElementsByTagName("p");
    var tab_gauche = [gaucheC.length];
    var tab_droite = [droiteC.length];
    for (var i = 0; i < gaucheC.length; i++)
        tab_gauche[i] = gaucheC[i].innerText.split(" : ")[1];
    for (var i = 0; i < droiteC.length; i++)
        tab_droite[i] = droiteC[i].innerText.split(" : ")[1];

    var tableau = document.getElementById("table");
    var tbody = document.createElement("tbody");

    removeTable(tableau);

    /*HEAD Tableau */
    var tr_head = document.createElement("tr");
    var th_tab = [3];
    for (var i = 0; i < 3; i++)
        th_tab[i] = document.createElement("th");
    th_tab[1].innerText = gauche.getElementsByTagName("a")[0].innerText; //titre gauche
    th_tab[2].innerText = droite.getElementsByTagName("a")[0].innerText; //titre droite
    for (var i = 0; i < 3; i++)
        tr_head.appendChild(th_tab[i]);
    tbody.appendChild(tr_head);
    /*HEAD Tableau Fin*/

    /*CONTAINS Tableau */
    var nbr = tab_gauche.length;
    for (var i = 0; i < nbr; i++) {
        var tr_body = document.createElement("tr");
        var tmp = [3];
        for (var j = 0; j < 3; j++) tmp[j] = document.createElement("td");
        tmp[0].innerText = gaucheC[i].innerText.split(" : ")[0]; // ex, Entreprise :
        tmp[1].innerText = tab_gauche[i];
        tmp[2].innerText = tab_droite[i];
        for (var j = 0; j < 3; j++)
            tr_body.appendChild(tmp[j]);

        tbody.appendChild(tr_body);
    }
    /*CONTAINS Tableau Fin*/

    tableau.appendChild(tbody);
}

function removeTable(tableau) {
    while (tableau.firstChild)
        tableau.removeChild(tableau.firstChild);
}

/** Fonction inutile pour l'instant **/

function switchList(type) {
    var linkL = document.getElementById("switch");
    if (type == "current")
        linkL.setAttribute("href", "css/accordion.css");
    else
        linkL.setAttribute("href", "css/boxlist.css");
}
