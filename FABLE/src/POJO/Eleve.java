package POJO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Eleve {
    private int idEleve;
    private String Email;
    private String nom;
    private String prenom;
    private String motDePasse;
    private Boolean isStudent;
    private Stage sonStage;
    private Adresse sonAdresse;
    private String lienPhoto;
    private List<PropositionDeStage> listStgaeInteresse;

    public Eleve(int idEleve, String email, String nom, String prenom, String motDePasse, Boolean isStudent,
                 Stage sonStage, Adresse sonAdresse) {
        super();
        this.idEleve = idEleve;
        Email = email;
        this.nom = nom;
        this.prenom = prenom;
        this.motDePasse = motDePasse;
        this.isStudent = isStudent;
        this.sonStage = sonStage;
        this.sonAdresse = sonAdresse;
    }

    public Eleve(ResultSet set) throws SQLException {
        this.idEleve = set.getInt(1);
        Email = set.getString(2);
        this.nom = set.getString(3);
        this.prenom = set.getString(4);
        this.motDePasse = set.getString(5);
        this.isStudent = set.getBoolean(6);
        this.sonStage = null;
        if ((Integer) set.getInt("idAdresse") == null) this.sonAdresse = null;
        else
            this.sonAdresse = new Adresse(set.getInt("idAdresse"), set.getString("rue"), set.getInt("NumRue"), set.getInt("CodePostal"), set.getString("Ville"), set.getString("Pays"));
        this.lienPhoto = set.getString("lienPhoto");
    }

    public Eleve() {
        // TODO Auto-generated constructor stub
        super();
    }

    public String getLienPhoto() {
        return lienPhoto;
    }

    public void setLienPhoto(String lienPhoto) {
        this.lienPhoto = lienPhoto;
    }

    public int getIdEleve() {
        return idEleve;
    }

    public void setIdEleve(int idEleve) {
        this.idEleve = idEleve;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Boolean getIsStudent() {
        return isStudent;
    }

    public void setIsStudent(Boolean isStudent) {
        this.isStudent = isStudent;
    }

    public Stage getSonStage() {
        return sonStage;
    }

    public void setSonStage(Stage sonStage) {
        this.sonStage = sonStage;
    }

    public Adresse getSonAdresse() {
        return sonAdresse;
    }

    public void setSonAdresse(Adresse sonAdresse) {
        this.sonAdresse = sonAdresse;
    }

    public List<PropositionDeStage> getListStgaeInteresse() {
        return listStgaeInteresse;
    }

    public void setListStgaeInteresse(List<PropositionDeStage> listStgaeInteresse) {
        this.listStgaeInteresse = listStgaeInteresse;
    }

    @Override
    public String toString() {
        return "Eleve [idEleve=" + idEleve + ", Email=" + Email + ", nom=" + nom + ", prenom=" + prenom
                + ", motDePasse=" + motDePasse + ", isStudent=" + isStudent + ", sonStage=" + sonStage + ", sonAdresse="
                + sonAdresse + ", listStgaeInteresse=" + listStgaeInteresse + "]";
    }


}
