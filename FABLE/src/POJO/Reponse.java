package POJO;

public class Reponse {
    private Question concerneQuestion;
    private Evaluation concerneEva;
    private String contenu;

    public Reponse(Question concerneQuestion, Evaluation concerneEva, String contenu) {
        super();
        this.concerneQuestion = concerneQuestion;
        this.concerneEva = concerneEva;
        this.contenu = contenu;
    }

    public Question getConcerneQuestion() {
        return concerneQuestion;
    }

    public void setConcerneQuestion(Question concerneQuestion) {
        this.concerneQuestion = concerneQuestion;
    }

    public Evaluation getConcerneEva() {
        return concerneEva;
    }

    public void setConcerneEva(Evaluation concerneEva) {
        this.concerneEva = concerneEva;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    @Override
    public String toString() {
        return "Reponse [concerneQuestion=" + concerneQuestion + ", concerneEva=" + concerneEva + ", contenu=" + contenu
                + "]";
    }


}
