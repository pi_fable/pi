package POJO;

public class SecteurActivite {
    private int idActivite;
    private String nomActivite;

    public SecteurActivite(int idActivite, String nomActivite) {
        super();
        this.idActivite = idActivite;
        this.nomActivite = nomActivite;
    }

    public SecteurActivite() {

    }

    public int getIdActivite() {
        return idActivite;
    }

    public void setIdActivite(int idActivite) {
        this.idActivite = idActivite;
    }

    public String getNomActivite() {
        return nomActivite;
    }

    public void setNomActivite(String nomActivite) {
        this.nomActivite = nomActivite;
    }

    @Override
    public String toString() {
        return "SecteurActivite [idActivite=" + idActivite + ", nomActivite=" + nomActivite + "]";
    }

}
