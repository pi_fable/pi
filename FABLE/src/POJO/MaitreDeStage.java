package POJO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MaitreDeStage extends Contact {

    private int idMaitre;
    private String motDePasse;

    public MaitreDeStage(int idContact, String nom, String prenom, String telephone, String adresseMail, String poste,
                         Entreprise entrep, int idMaitre, String motDePasse) {
        super(idContact, nom, prenom, telephone, adresseMail, poste, entrep);
        this.idMaitre = idMaitre;
        this.motDePasse = motDePasse;
    }

    public MaitreDeStage() {

    }

    public MaitreDeStage(ResultSet set) throws SQLException {
        super(set.getInt("idContact"), set.getString("nom"), set.getString("prenom"), set.getString("telephone"), set.getString("AdresseMail"), set.getString("Poste"), null);
        idMaitre = set.getInt("idMaitre");
        motDePasse = set.getString("idContact");
    }

    public int getIdMaitre() {
        return idMaitre;
    }

    public void setIdMaitre(int idMaitre) {
        this.idMaitre = idMaitre;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    @Override
    public String toString() {
        return "MaitreDeStage [idMaitre=" + idMaitre + ", motDePasse=" + motDePasse + "]";
    }


}
