package POJO;

import java.time.LocalDate;
import java.util.List;

public class Evenement {

    private int NumEven;
    private LocalDate DateEven;
    private String Sujet;
    private String Description;
    private Adresse adrs;
    private Planning plan;

    private List<Contact> listContact;

    public Evenement() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Evenement(int numEven, LocalDate dateEven, String sujet, String description, Adresse adrs, Planning plan) {
        super();
        NumEven = numEven;
        DateEven = dateEven;
        Sujet = sujet;
        Description = description;
        this.adrs = adrs;
        this.plan = plan;
    }

    public int getNumEven() {
        return NumEven;
    }

    public void setNumEven(int numEven) {
        NumEven = numEven;
    }

    public LocalDate getDateEven() {
        return DateEven;
    }

    public void setDateEven(LocalDate dateEven) {
        DateEven = dateEven;
    }

    public int getJour() {
        //LocalDate s=DateEven.toLocalDate();
        return DateEven.getDayOfMonth();
    }

    public String getMois() {
        String res = "";
        //LocalDate s=DateEven.toLocalDate();
        int mois = DateEven.getMonthValue();

        switch (mois) {
            case 1:
                res = "Jan";
                break;
            case 2:
                res = "Fev";
                break;
            case 3:
                res = "Mar";
                break;
            case 4:
                res = "Avr";
                break;
            case 5:
                res = "Mai";
                break;
            case 6:
                res = "Juin";
                break;
            case 7:
                res = "Juil";
                break;
            case 8:
                res = "Aou";
                break;
            case 9:
                res = "Sep";
                break;
            case 10:
                res = "Oct";
                break;
            case 11:
                res = "Nov";
                break;
            case 12:
                res = "Dec";
                break;
        }
        return res;
    }

    public String getSujet() {
        return Sujet;
    }

    public void setSujet(String sujet) {
        Sujet = sujet;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Adresse getAdrs() {
        return adrs;
    }

    public void setAdrs(Adresse adrs) {
        this.adrs = adrs;
    }

    public Planning getPlan() {
        return plan;
    }

    public void setPlan(Planning plan) {
        this.plan = plan;
    }

    public List<Contact> getListContact() {

        return listContact;
    }

    public void setListContact(List<Contact> listContact) {
        this.listContact = listContact;
    }

    @Override
    public String toString() {
        return "Evenement [NumEven=" + NumEven + ", DateEven=" + DateEven + ", Sujet=" + Sujet + ", Description="
                + Description + ", adrs=" + adrs + ", plan=" + plan + ", listContact=" + listContact + "]";
    }


}
