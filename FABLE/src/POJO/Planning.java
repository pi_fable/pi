package POJO;

public class Planning {
    private int AnneeScolaire;

    public Planning(int anneeScolaire) {
        super();
        AnneeScolaire = anneeScolaire;
    }

    public int getAnneeScolaire() {
        return AnneeScolaire;
    }

    public void setAnneeScolaire(int anneeScolaire) {
        AnneeScolaire = anneeScolaire;
    }

    @Override
    public String toString() {
        return "Planning [AnneeScolaire=" + AnneeScolaire + "]\n";
    }


}
