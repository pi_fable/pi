package POJO;

public class Technologie {
    private int idTech;
    private String nom;

    public Technologie(String nom, int id) {
        super();
        this.nom = nom;
        this.idTech = id;
    }

    public Technologie() {

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdTechnologie() {
        // TODO Auto-generated method stub
        return idTech;
    }

    public void setIdTechnologie(int id) {
        this.idTech = id;
    }

    @Override
    public String toString() {
        return "Technologie [idTech=" + idTech + ", nom=" + nom + "]\n";
    }

}
