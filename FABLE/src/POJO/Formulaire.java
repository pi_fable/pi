package POJO;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class Formulaire {
    private int idForm;
    private String titre;
    private String periode;
    private LocalDate dateLimite;//changer date limite de �valuation !!!!!!

    private List<Question> listQuestion;

    public Formulaire(int idForm, String titre, String periode, LocalDate dateLimite) {
        super();
        this.idForm = idForm;
        this.titre = titre;
        this.periode = periode;
        this.dateLimite = dateLimite;
    }

    public Formulaire() {
        // TODO Auto-generated constructor stub
        super();
    }

    public int getIdForm() {
        return idForm;
    }

    public void setIdForm(int idForm) {
        this.idForm = idForm;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public LocalDate getDateLimite() {
        return dateLimite;
    }

    public void setDateLimite(LocalDate dateLimite) {
        this.dateLimite = dateLimite;
    }

    public List<Question> getListQuestion() {
        return listQuestion;
    }

    public void setListQuestion(List<Question> listQuestion) {
        this.listQuestion = listQuestion;
    }

    @Override
    public String toString() {
        return "Formulaire [idForm=" + idForm + ", titre=" + titre + ", periode=" + periode + ", dateLimite="
                + dateLimite + ", listQuestion=" + listQuestion + "]";
    }

}
