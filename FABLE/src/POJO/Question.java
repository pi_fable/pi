package POJO;

public class Question {
    private int numQuestion;
    private String types;
    private String intitule;
    private Boolean obligatoire;

    public Question(int numQuestion, String types, String intitule, Boolean obligatoire) {
        super();
        this.numQuestion = numQuestion;
        this.types = types;
        this.intitule = intitule;
        this.obligatoire = obligatoire;
    }

    public Question() {
        // TODO Auto-generated constructor stub
        super();
    }

    public int getNumQuestion() {
        return numQuestion;
    }

    public void setNumQuestion(int numQuestion) {
        this.numQuestion = numQuestion;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Boolean getObligatoire() {
        return obligatoire;
    }

    public void setObligatoire(Boolean obligatoire) {
        this.obligatoire = obligatoire;
    }

    @Override
    public String toString() {
        return "Question [numQuestion=" + numQuestion + ", types=" + types + ", intitule=" + intitule + ", obligatoire="
                + obligatoire + "]";
    }


}
