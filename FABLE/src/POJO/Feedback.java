package POJO;

import java.sql.Date;

public class Feedback {
    private int numFeedback;
    private Date dateRemise;
    private String message;
    private String lienFichier;
    private Stage concerneStage;
    private Eleve conserneEleve;

    public Feedback(int numFeedback, Date dateRemise, String message, String lienFichier, Stage concerneStage,
                    Eleve conserneEleve) {
        super();
        this.numFeedback = numFeedback;
        this.dateRemise = dateRemise;
        this.message = message;
        this.lienFichier = lienFichier;
        this.concerneStage = concerneStage;
        this.conserneEleve = conserneEleve;
    }

    public Feedback() {
        // TODO Auto-generated constructor stub
        super();
    }

    public int getNumFeedback() {
        return numFeedback;
    }

    public void setNumFeedback(int numFeedback) {
        this.numFeedback = numFeedback;
    }

    public Date getDateRemise() {
        return dateRemise;
    }

    public void setDateRemise(Date dateRemise) {
        this.dateRemise = dateRemise;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLienFichier() {
        return lienFichier;
    }

    public void setLienFichier(String lienFichier) {
        this.lienFichier = lienFichier;
    }

    public Stage getConcerneStage() {
        return concerneStage;
    }

    public void setConcerneStage(Stage concerneStage) {
        this.concerneStage = concerneStage;
    }

    public Eleve getConserneEleve() {
        return conserneEleve;
    }

    public void setConserneEleve(Eleve conserneEleve) {
        this.conserneEleve = conserneEleve;
    }

    @Override
    public String toString() {
        return "Feedback [numFeedback=" + numFeedback + ", dateRemise=" + dateRemise + ", message=" + message
                + ", lienFichier=" + lienFichier + ", concerneStage=" + concerneStage + ", conserneEleve="
                + conserneEleve + "]";
    }


}
