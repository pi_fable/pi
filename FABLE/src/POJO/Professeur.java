package POJO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Professeur {
    private int idProf;
    private String nom;
    private String prenom;
    private String email;
    private String motDePasse;

    public Professeur(int idProf, String nom, String prenom, String email, String motDePasse) {
        super();
        this.idProf = idProf;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.motDePasse = motDePasse;
    }

    public Professeur(ResultSet set) throws SQLException {
        email = set.getString(4);
        nom = set.getString(2);
        prenom = set.getString(3);
    }

    public Professeur() {
        // TODO Auto-generated constructor stub
        super();
    }

    public int getIdProf() {
        return idProf;
    }

    public void setIdProf(int idProf) {
        this.idProf = idProf;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    @Override
    public String toString() {
        return "Professeur [idProf=" + idProf + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email
                + ", motDePasse=" + motDePasse + "]";
    }


}
