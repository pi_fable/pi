package POJO;

import java.util.List;

public class Contact {
    private int idContact;
    private String nom;
    private String prenom;
    private String telephone;
    private String AdresseMail;
    private String Poste;

    private Entreprise entrep;
    private List<Evenement> listEvent;

    public Contact(int idContact, String nom, String prenom, String telephone, String adresseMail, String poste,
                   Entreprise entrep) {
        super();
        this.idContact = idContact;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        AdresseMail = adresseMail;
        Poste = poste;
        this.entrep = entrep;
    }

    public Contact() {
        // TODO Auto-generated constructor stub
        super();
    }

    public int getIdContact() {
        return idContact;
    }

    public void setIdContact(int idContact) {
        this.idContact = idContact;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAdresseMail() {
        return AdresseMail;
    }

    public void setAdresseMail(String adresseMail) {
        AdresseMail = adresseMail;
    }

    public String getPoste() {
        return Poste;
    }

    public void setPoste(String poste) {
        Poste = poste;
    }

    public Entreprise getEntrep() {
        return entrep;
    }

    public void setEntrep(Entreprise entrep) {
        this.entrep = entrep;
    }

    public List<Evenement> getListEvent() {
        return listEvent;
    }

    public void setListEvent(List<Evenement> listEvent) {
        this.listEvent = listEvent;
    }

    @Override
    public String toString() {
        return "Contact [idContact=" + idContact + ", nom=" + nom + ", prenom=" + prenom + ", telephone=" + telephone
                + ", AdresseMail=" + AdresseMail + ", Poste=" + Poste + ", entrep=" + entrep + ", listEvent="
                + listEvent + "]\n";
    }

    public boolean equals(Contact c) {

        return idContact == c.idContact;
    }


}
