package POJO;

import java.time.LocalDate;

public class Stage {
    private int idStage;
    private LocalDate DateDebut;
    private LocalDate DateFin;
    private Boolean Repertorier;
    private PropositionDeStage propStage;
    private MaitreDeStage maitreDeStage;
    private Eleve eleveStage;

    public Stage(int idStage, LocalDate dateDebut, LocalDate dateFin, Boolean repertorier, PropositionDeStage propStage) {
        super();
        this.idStage = idStage;
        DateDebut = dateDebut;
        DateFin = dateFin;
        Repertorier = repertorier;
        this.propStage = propStage;
    }

    public Stage() {
        // TODO Auto-generated constructor stub
        super();
    }

    public MaitreDeStage getMaitreDeStage() {
        return maitreDeStage;
    }

    public void setMaitreDeStage(MaitreDeStage maitreDeStage) {
        this.maitreDeStage = maitreDeStage;
    }

    public int getIdStage() {
        return idStage;
    }

    public void setIdStage(int idStage) {
        this.idStage = idStage;
    }

    public LocalDate getDateDebut() {
        return DateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        DateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return DateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        DateFin = dateFin;
    }

    public Boolean getRepertorier() {
        return Repertorier;
    }

    public void setRepertorier(Boolean repertorier) {
        Repertorier = repertorier;
    }

    public PropositionDeStage getPropStage() {
        return propStage;
    }

    public void setPropStage(PropositionDeStage propStage) {
        this.propStage = propStage;
    }

    public Eleve getEleveStage() {
        return eleveStage;
    }

    public void setEleveStage(Eleve eleveStage) {
        this.eleveStage = eleveStage;
    }

    @Override
    public String toString() {
        return "Stage [idStage=" + idStage + ", DateDebut=" + DateDebut + ", DateFin=" + DateFin + ", Repertorier="
                + Repertorier + ", propStage=" + propStage + ", eleveStage=" + eleveStage + "]";
    }


}
