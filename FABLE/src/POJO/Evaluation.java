package POJO;

import java.sql.Date;

public class Evaluation {
    private int numEva;
    private Date dateRemise;
    private int Cotation;
    private Stage conserneStage;
    private MaitreDeStage conserneMaitreDeStage;

    public Evaluation(int numEva, Date dateRemise, int cotation, Stage conserneStage,
                      MaitreDeStage conserneMaitreDeStage) {
        super();
        this.numEva = numEva;
        this.dateRemise = dateRemise;
        Cotation = cotation;
        this.conserneStage = conserneStage;
        this.conserneMaitreDeStage = conserneMaitreDeStage;
    }

    public Evaluation() {
        // TODO Auto-generated constructor stub
        super();
    }

    public int getNumEva() {
        return numEva;
    }

    public void setNumEva(int numEva) {
        this.numEva = numEva;
    }

    public Date getDateRemise() {
        return dateRemise;
    }

    public void setDateRemise(Date dateRemise) {
        this.dateRemise = dateRemise;
    }

    public int getCotation() {
        return Cotation;
    }

    public void setCotation(int cotation) {
        Cotation = cotation;
    }

    public Stage getConserneStage() {
        return conserneStage;
    }

    public void setConserneStage(Stage conserneStage) {
        this.conserneStage = conserneStage;
    }

    public MaitreDeStage getConserneMaitreDeStage() {
        return conserneMaitreDeStage;
    }

    public void setConserneMaitreDeStage(MaitreDeStage conserneMaitreDeStage) {
        this.conserneMaitreDeStage = conserneMaitreDeStage;
    }

    @Override
    public String toString() {
        return "Evaluation [numEva=" + numEva + ", dateRemise=" + dateRemise + ", Cotation=" + Cotation
                + ", conserneStage=" + conserneStage + ", conserneMaitreDeStage=" + conserneMaitreDeStage + "]";
    }


}
