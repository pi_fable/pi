package POJO;

import java.util.List;

public class Entreprise {

    private int idEntreprise;
    private String URLSite;
    private String nom;
    private Adresse adrs;

    private List<SecteurActivite> listSectAct;
    private List<Technologie> listTechno;

    public Entreprise(int idEntreprise, String uRLSite, String nom, Adresse adrs) {
        super();
        this.idEntreprise = idEntreprise;
        URLSite = uRLSite;
        this.nom = nom;
        this.adrs = adrs;
    }

    public Entreprise() {
        super();
        // TODO Auto-generated constructor stub
    }

    public int getIdEntreprise() {
        return idEntreprise;
    }

    public void setIdEntreprise(int idEntreprise) {
        this.idEntreprise = idEntreprise;
    }

    public String getURLSite() {
        return URLSite;
    }

    public void setURLSite(String uRLSite) {
        URLSite = uRLSite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdrs() {
        return adrs;
    }

    public void setAdrs(Adresse adrs) {
        this.adrs = adrs;
    }

    public List<SecteurActivite> getListSectAct() {
        return listSectAct;
    }

    public void setListSectAct(List<SecteurActivite> listSectAct) {
        this.listSectAct = listSectAct;
    }

    public List<Technologie> getListTechno() {
        return listTechno;
    }

    public void setListTechno(List<Technologie> listTechno) {
        this.listTechno = listTechno;
    }

    @Override
    public String toString() {
        return "Entreprise [idEntreprise=" + idEntreprise + ", URLSite=" + URLSite + ", nom=" + nom + ", adrs=" + adrs
                + ", listSectAct=" + listSectAct + ", listTechno=" + listTechno + "]\n";
    }


}
