package POJO;

public class Adresse {

    private int idAdresse;
    private String rue;
    private int NumRue;
    private int CodePostal;
    private String Ville;
    private String Pays;

    public Adresse() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Adresse(int idAdresse, String rue, int numRue, int codePostal, String ville, String pays) {
        super();
        this.idAdresse = idAdresse;
        this.rue = rue;
        NumRue = numRue;
        CodePostal = codePostal;
        Ville = ville;
        Pays = pays;
    }

    public int getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(int idAdresse) {
        this.idAdresse = idAdresse;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getNumRue() {
        return NumRue;
    }

    public void setNumRue(int numRue) {
        NumRue = numRue;
    }

    public int getCodePostal() {
        return CodePostal;
    }

    public void setCodePostal(int codePostal) {
        CodePostal = codePostal;
    }

    public String getVille() {
        return Ville;
    }

    public void setVille(String ville) {
        Ville = ville;
    }

    public String getPays() {
        return Pays;
    }

    public void setPays(String pays) {
        Pays = pays;
    }

    @Override
    public String toString() {
        return "Adresse [idAdresse=" + idAdresse + ", rue=" + rue + ", NumRue=" + NumRue + ", CodePostal=" + CodePostal
                + ", Ville=" + Ville + ", Pays=" + Pays + "]\n";
    }

    public boolean equals(Adresse a) {
        return rue.equals(a.rue) && NumRue == a.NumRue && Ville.equals(a.Ville) && CodePostal == a.CodePostal && Pays.equals(a.Pays);
    }

}
