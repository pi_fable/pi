package POJO;

import java.util.List;

public class PropositionDeStage {

    private int idProp;
    private String sujet;
    private int annee;
    private String description;
    private String types;
    private Entreprise Entrep;
    private String lienFichier;

    private List<Technologie> listTechno;

    public PropositionDeStage(int idProp, String sujet, int annee, String description, String types, Entreprise entrep,
                              String lienFichier) {
        super();
        this.idProp = idProp;
        this.sujet = sujet;
        this.annee = annee;
        this.description = description;
        this.types = types;
        Entrep = entrep;
        this.lienFichier = lienFichier;
    }

    public PropositionDeStage() {
        // TODO Auto-generated constructor stub
        super();
    }

    public int getIdProp() {
        return idProp;
    }

    public void setIdProp(int idProp) {
        this.idProp = idProp;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public Entreprise getEntrep() {
        return Entrep;
    }

    public void setEntrep(Entreprise entrep) {
        Entrep = entrep;
    }

    public String getLienFichier() {
        return lienFichier;
    }

    public void setLienFichier(String lienFichier) {
        this.lienFichier = lienFichier;
    }

    public List<Technologie> getListTechno() {
        return listTechno;
    }

    public void setListTechno(List<Technologie> listTechno) {
        this.listTechno = listTechno;
    }

    @Override
    public String toString() {
        return "PropositionDeStage [idProp=" + idProp + ", sujet=" + sujet + ", annee=" + annee + ", description="
                + description + ", types=" + types + ", Entrep=" + Entrep + ", lienFichier=" + lienFichier
                + ", listTechno=" + listTechno + "]";
    }


}
