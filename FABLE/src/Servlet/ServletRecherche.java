package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAOPackage.DAO;
import DAOPackage.DAO_Contact;
import DAOPackage.DAO_Entreprise;
import DAOPackage.DAO_PropositionDeStage;
import Metier.Controlleur;
import POJO.Contact;
import POJO.Entreprise;
import POJO.PropositionDeStage;
import POJO.Technologie;

@WebServlet(urlPatterns = {"/Recherche", "/RechercheContact", "/RechercheEntreprise", "/RechercheStage", "/byKeyWord"})
public class ServletRecherche extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Controlleur cont = (Controlleur) request.getSession().getAttribute("controleur");
        //ent = const.getEntreprise()
        String res2 = request.getServletPath();
        String mc = request.getParameter("searchWord");

        if (res2.equals("/RechercheEntreprise")) {

            DAO<Entreprise> dao = new DAO_Entreprise((Connection) request.getSession().getAttribute("Connexion"));
            List<Entreprise> ent;

            if (mc == null) ent = dao.getList();
            else ent = dao.getListByMotcle(mc);

            if (ent.size() == 0)
                request.setAttribute("erreur", "Aucune entreprise trouv�e");
            else
                request.setAttribute("List", ent);

            request.setAttribute("Type", "Entreprise");
        }

        if (res2.equals("/RechercheStage")) {

            List<Technologie> tech = cont.getAllTechnologie();
            List<PropositionDeStage> sta;
            String [] param_tech  = request.getParameterValues("tech");
            String param_pays = request.getParameter("pays");
            if(param_pays != null && param_pays.equals("autre"))  param_pays = request.getParameter("autre");

            if (mc == null && param_tech == null /* ... && */ ) sta = cont.getAllPropositionDeStage();
            else{
                String [][] tableau = new String[3][];
                tableau[0] = new String [1];
                tableau[0][0] = mc;
                tableau[1] = param_tech;
                tableau[2] = new String [1];
                tableau[2][0] = param_pays;
                sta = cont.rechercherPropositionDeStage(tableau);
            }


            if (sta.size() == 0)
                request.setAttribute("erreur", "Aucune proposition de stage trouv�e");
            else
                request.setAttribute("List", sta);


            request.setAttribute("Type", "Stage");
            request.setAttribute("tech", tech);

        }

        if (res2.equals("/RechercheContact")) {

            DAO<Contact> dao = new DAO_Contact((Connection) request.getSession().getAttribute("Connexion"));
            List<Contact> con;

            if (mc == null) con = dao.getList();
            else con = dao.getListByMotcle(mc);

            if (con.size() == 0)
                request.setAttribute("erreur", "Aucun contact trouv�");
            else
                request.setAttribute("List", con);
            request.setAttribute("Type", "Contact");
        }


        this.getServletContext().getRequestDispatcher("/WEB-INF/Recherche.jsp").forward(request, response);
    }

}