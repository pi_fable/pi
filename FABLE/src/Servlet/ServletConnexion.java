package Servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAOPackage.ConnexionBD;
import Metier.Controlleur;
import POJO.Eleve;
import POJO.MaitreDeStage;
import POJO.Professeur;

@WebServlet("/page2")
public class ServletConnexion extends HttpServlet {
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
			String pseudo=request.getParameter("username");
			String mdp=request.getParameter("password");
			ConnexionBD connexion=new ConnexionBD();
			HttpSession session=request.getSession();
			Controlleur cont=new Controlleur(connexion.getConnexion());
			boolean connexionOk=false;
			try {
				String[]res=pseudo.split("@");
				if(res[1].equals("hers.be")){
					
					Professeur p=connexion.ConnexionProfesseur(pseudo, mdp);
					if(p!=null){
						session.setAttribute("user", p);
						session.setAttribute("type", "professeur");
						session.setAttribute("Connexion", connexion.getConnexion());
						connexionOk=true;
					}
					
				}
				else if(res[1].equals("student.hers.be") | res[1].equals("eco.hers.be")){
					Eleve e=connexion.ConnexionEleve(pseudo, mdp);
					if(e!=null){
						session.setAttribute("user", e);
						session.setAttribute("type", "eleve");
						session.setAttribute("Connexion", connexion.getConnexion());
						connexionOk=true;
					}
				}
				else{
					
					MaitreDeStage m=connexion.ConnexionMaitreDeStage(pseudo, mdp);
					if(m!=null){
						session.setAttribute("user", m);
						session.setAttribute("type", "maitre de stage");
						session.setAttribute("Connexion", connexion.getConnexion());
						connexionOk=true;
					}
				}
				session.setAttribute("controleur", cont);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(connexionOk)this.getServletContext().getRequestDispatcher("/WEB-INF/Affichage.jsp").forward(request, response);
			else{
				try {
					connexion.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				request.setAttribute("erreur", "identifiants incorrects!!!!");
				this.getServletContext().getRequestDispatcher("/WEB-INF/Connexion.jsp").forward(request, response);
			}
			
		
			
	}
}
