package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.time.LocalDate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAOPackage.DAO;
import DAOPackage.DAO_Eleve;
import DAOPackage.DAO_Entreprise;
import DAOPackage.DAO_MaitreDeStage;
import DAOPackage.DAO_PropositionDeStage;
import DAOPackage.DAO_Stage;
import Metier.Controlleur;
import POJO.Eleve;
import POJO.Entreprise;
import POJO.Evaluation;
import POJO.MaitreDeStage;
import POJO.PropositionDeStage;
import POJO.Stage;


@WebServlet(urlPatterns = {"/validerStage", "/ajouterProp", "/modifierProp", "/modifierStage", "/supprimerProp", "/supprimerStage"})
public class ServletStage extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        String res1 = request.getRequestURL().toString();
        String[] res = res1.split("/");
        String curJSP = res[res.length - 1];
        Controlleur contr = (Controlleur) request.getSession().getAttribute("controleur");
        if (curJSP.equals("validerStage")) {

            String d1 = request.getParameter("dateDeb");
            String d2 = request.getParameter("dateFin");
            LocalDate[] dateStage = getDate(d1, d2);
            String sujet = request.getParameter("sujet");
            String description = request.getParameter("description");
            int annee = Integer.parseInt(request.getParameter("annee"));
            int idEleve = Integer.parseInt(request.getParameter("eleve"));
            int idEntreprise = Integer.parseInt(request.getParameter("Entreprise"));
            int idContact = Integer.parseInt(request.getParameter("Contact"));
            Entreprise tmp = contr.getEntreprise(idEntreprise);
            PropositionDeStage prop;
            if (request.getSession().getAttribute("typeValidation").equals("old")) {
                prop = contr.getPropositionDeStage(Integer.parseInt("" + request.getSession().getAttribute("idProp")));

                prop.setSujet(sujet);
                prop.setDescription(description);
                prop.setAnnee(annee);
                prop.setEntrep(tmp);
                contr.updatePropositionDeStage(prop);
            } else {
                prop = new PropositionDeStage(0, sujet, annee, description, "externe", tmp, "");
                contr.addPropositionDeStage(prop);
                prop.setIdProp(contr.getLastIdCreateProp());
            }

            Eleve e = contr.getProfil(idEleve);
            MaitreDeStage m = contr.findByIdContact(idContact);
            if (m == null) {
                m = new MaitreDeStage();
                m.setIdContact(idContact);
                m.setMotDePasse("password" + idContact);
                contr.addMaitreDeStage(m);
            }
            Stage s = new Stage(0, dateStage[0], dateStage[1], false, prop);
            s.setEleveStage(e);
            s.setMaitreDeStage(m);
            contr.addStage(s);
            Evaluation eva = new Evaluation();
            eva.setConserneMaitreDeStage(m);
            eva.setConserneStage(s);
            contr.addEvaluation(eva);

            request.getRequestDispatcher("/affichage").forward(request, response);


        } else if (curJSP.equals("ajouterProp")) {
            String sujet = request.getParameter("sujet");
            String description = request.getParameter("description");
            int annee = Integer.parseInt(request.getParameter("annee"));
            int idEntreprise = Integer.parseInt(request.getParameter("Entreprise"));
            Entreprise tmp = contr.getEntreprise(idEntreprise);
            PropositionDeStage prop = new PropositionDeStage(0, sujet, annee, description, "interne", tmp, null);
            contr.addPropositionDeStage(prop);
            request.setAttribute("ajout", "la proposition de stage a �t� bien enregistr�e");
            request.getRequestDispatcher("/FormPropositionDeStage").forward(request, response);
        } else if (curJSP.equals("modifierProp")) {
            String sujet = request.getParameter("sujet");
            String description = request.getParameter("description");
            int annee = Integer.parseInt(request.getParameter("annee"));
            int idEntreprise = Integer.parseInt(request.getParameter("Entreprise"));
            int idProp = Integer.parseInt(request.getParameter("idProp"));
            Entreprise tmp = contr.getEntreprise(idEntreprise);
            PropositionDeStage p = new PropositionDeStage(idProp, sujet, annee, description, "interne", tmp, null);
            contr.updatePropositionDeStage(p);
            request.getRequestDispatcher("/recherche.jsp?prop=" + idProp).forward(request, response);

        } else if (curJSP.equals("modifierStage")) {

        } else if (curJSP.equals("supprimerProp")) {
            int id = Integer.parseInt(request.getParameter("idProp"));
            contr.removePropositionDeStage(id);
            response.sendRedirect("recherche.jsp");
        } else if (curJSP.equals("supprimerStage")) {
            int id = Integer.parseInt(request.getParameter("idStage"));
            contr.removeStage(id);
            response.sendRedirect("");
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

    private LocalDate[] getDate(String d1, String d2) {
        LocalDate date1 = null;
        LocalDate date2 = null;
        if (d1 != null & d2 != null) {
            String[] tmp = d1.split("-");
            d1 = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
            date1 = LocalDate.parse(d1);
            tmp = d2.split("-");
            d2 = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
            date2 = LocalDate.parse(d2);
        } else if (d1 != null) {
            String[] tmp = d1.split("-");
            d1 = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
            date1 = LocalDate.parse(d1);
        } else {
            String[] tmp = d2.split("-");
            d2 = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
            date2 = LocalDate.parse(d2);
        }
        LocalDate[] res = {date1, date2};
        return res;
    }

}
