package Servlet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;



import DAOPackage.DAO;
import DAOPackage.DAO_Adresse;
import DAOPackage.DAO_Contact;
import DAOPackage.DAO_Eleve;
import DAOPackage.DAO_Entreprise;
import DAOPackage.DAO_Evenement;
import DAOPackage.DAO_MaitreDeStage;
import DAOPackage.DAO_PropositionDeStage;
import DAOPackage.DAO_Stage;
import POJO.Adresse;
import POJO.Contact;
import POJO.Eleve;
import POJO.Entreprise;
import POJO.Evenement;
import POJO.MaitreDeStage;
import POJO.Planning;
import POJO.PropositionDeStage;
import POJO.Stage;

@WebServlet(urlPatterns={"/addContact"})
public class ServletAjouter extends HttpServlet {
	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		String res1=request.getRequestURL().toString();
		String[]res=res1.split("/");
		String curJSP=res[res.length-1];
		
		if(curJSP.equals("addContact")){
			String nom=request.getParameter("nom");
			String prenom=request.getParameter("prenom");
			String telephone=request.getParameter("telephone");
			String email=request.getParameter("email");
			String poste=request.getParameter("Poste");
			String idEntreprise=request.getParameter("idEntreprise");
			String isMaitreDeStage=request.getParameter("MaitreDeStage");
			if(isMaitreDeStage==null){
				DAO<Contact> dao=new DAO_Contact((Connection)request.getSession().getAttribute("Connexion"));
				Contact c=new Contact(0,nom, prenom, telephone, email, poste, null);
				dao.create(c);
				request.setAttribute("Etat","Ma�tre de stage ajout�!!!");
			}
			else{
				DAO<MaitreDeStage> dao=new DAO_MaitreDeStage((Connection)request.getSession().getAttribute("Connexion"));
				MaitreDeStage m=new MaitreDeStage(0,nom, prenom, telephone, email, poste, null,0,"");
				dao.create(m);
				request.setAttribute("Etat","contact ajout�!!!");
			}
		}
		else if(curJSP.equals("addEvent")){
			String date=request.getParameter("date");
			String sujet=request.getParameter("sujet");
			String desc=request.getParameter("desc");
			String rue=request.getParameter("rue");
			String numRue=request.getParameter("numRue");
			String codePostal=request.getParameter("CodePostal");
			String ville=request.getParameter("ville");
			String pays=request.getParameter("pays");
			String[]contact=request.getParameterValues("Contact");
			String[]tmp=date.split("-");
			date=tmp[2]+"-"+tmp[1]+"-"+tmp[0];
			LocalDate date1=LocalDate.parse(date);
			
			DAO<Evenement> dao=new DAO_Evenement((Connection)request.getSession().getAttribute("Connexion"));
			DAO<Adresse>dao1=new DAO_Adresse((Connection)request.getSession().getAttribute("Connexion"));
			List<Adresse> adresses=dao1.getList();
			Adresse a=new Adresse(0, rue, Integer.parseInt(numRue), Integer.parseInt(codePostal), ville, pays);
			boolean ok=false;
			int i;
			for (i = 0; i < adresses.size() && !ok; i++) {
				if(adresses.get(i).equals(a))ok=true;
			}
			if(!ok){
				dao1.create(a);
				a.setIdAdresse(adresses.get(adresses.size()-1).getIdAdresse()+1);
			}
			else a.setIdAdresse(adresses.get(i-1).getIdAdresse());
			Evenement even=new Evenement(0, date1, sujet, desc,a, new Planning(date1.getYear()));
			DAO<Contact> dao2=new DAO_Contact((Connection)request.getSession().getAttribute("Connexion"));
			List<Contact>participant=new ArrayList<Contact>();
			for (String cont : contact) {
				
				participant.add(dao2.find(Integer.parseInt(cont)));
			}
			even.setListContact(participant);
			dao.create(even);
			
			curJSP="/Planning";
		}
		else if(curJSP.equals("validerStage")){
			
			String d1=request.getParameter("dateDeb");
			String d2=request.getParameter("dateFin");
			LocalDate[]dateStage=getDate(d1, d2);
			if(dateStage[0]!=null && dateStage[1]!=null && dateStage[0].isBefore(dateStage[1])){
				String sujet=request.getParameter("sujet");
				String description=request.getParameter("description");
				int annee=Integer.parseInt(request.getParameter("annee"));
				int idEleve=Integer.parseInt(request.getParameter("eleve"));
				int idEntreprise=Integer.parseInt(request.getParameter("Entreprise"));
				int idContact=Integer.parseInt(request.getParameter("Contact"));
				
				DAO<Entreprise> dao=new DAO_Entreprise((Connection)request.getSession().getAttribute("Connexion"));
				Entreprise tmp=dao.find(idEntreprise);
				PropositionDeStage prop;
				DAO_PropositionDeStage dao5=new DAO_PropositionDeStage((Connection)request.getSession().getAttribute("Connexion"));
				if(request.getSession().getAttribute("typeValidation").equals("old")){
					
					prop=dao5.find(Integer.parseInt(""+request.getSession().getAttribute("idProp")));
					prop.setSujet(sujet);
					prop.setDescription(description);
					prop.setAnnee(annee);
					prop.setEntrep(tmp);
					dao5.update(prop);
				}
				else{
					prop=new PropositionDeStage(0, sujet, annee, description, "externe", tmp, "");
					dao5.create(prop);
					prop.setIdProp(dao5.getLastIdCreate());
				}
				DAO<Eleve> dao2=new DAO_Eleve((Connection)request.getSession().getAttribute("Connexion"));
				Eleve e=dao2.find(idEleve);
				DAO_MaitreDeStage dao3=new DAO_MaitreDeStage((Connection)request.getSession().getAttribute("Connexion"));
				MaitreDeStage m=dao3.findByIdContact(idContact);
				if(m==null){
					m=new MaitreDeStage();
					m.setIdContact(idContact);
					m.setMotDePasse("password"+idContact);
					dao3.create(m);
				}
				Stage s=new Stage(0, dateStage[0], dateStage[1], false, prop);
				s.setEleveStage(e);
				s.setMaitreDeStage(m);
				DAO<Stage> dao4=new DAO_Stage((Connection)request.getSession().getAttribute("Connexion"));
				dao4.create(s);
			}
		
			
			
			curJSP="/Affichage";
		}
		this.getServletContext().getRequestDispatcher(curJSP).forward(request, response);
		
		
	}
	private LocalDate[] getDate(String d1,String d2){
		LocalDate date1=null;
		LocalDate date2=null;
		if(d1!=null & d2!=null){
			String[]tmp=d1.split("-");
			d1=tmp[2]+"-"+tmp[1]+"-"+tmp[0];
			 date1=LocalDate.parse(d1);
			tmp=d2.split("-");
			d2=tmp[2]+"-"+tmp[1]+"-"+tmp[0];
			 date2=LocalDate.parse(d2);
		}
		else if(d1!=null){
			String[]tmp=d1.split("-");
			d1=tmp[2]+"-"+tmp[1]+"-"+tmp[0];
			 date1=LocalDate.parse(d1);
		}
		else{
			String[]tmp=d2.split("-");
			d2=tmp[2]+"-"+tmp[1]+"-"+tmp[0];
			date2=LocalDate.parse(d2);
		}
		LocalDate[]res={date1,date2};
		return res;
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		doPost(request, response);
	}
}
