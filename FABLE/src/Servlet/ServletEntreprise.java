package Servlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Metier.Controlleur;
import POJO.Adresse;
import POJO.Entreprise;
import POJO.SecteurActivite;
import POJO.Technologie;



@WebServlet(urlPatterns={"/addEntreprise", "/modifEntreprise", "/editEntreprise", "/deleteEntreprise"})
public class ServletEntreprise extends HttpServlet{


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		String url=request.getRequestURL().toString();
		String []res=url.split("/");
		String fct=res[res.length-1];
		Controlleur contr=(Controlleur)request.getSession().getAttribute("controleur");
	
		if(fct.equals("addEntreprise")){
			String nom=request.getParameter("nomEnt");
			String urlSite=request.getParameter("urlSite");
			String numRue=request.getParameter("numRue");
			String rue=request.getParameter("rue");
			String codePostal=request.getParameter("CodePostal");
			String ville=request.getParameter("ville");
			String pays=request.getParameter("pays");
			Adresse a=new Adresse(0, rue, Integer.parseInt(numRue), Integer.parseInt(codePostal), ville, pays);
			Entreprise e=new Entreprise(0, urlSite, nom, a);
			String [] tabTech=request.getParameterValues("Technologie");
			
			
			String [] tabAct=request.getParameterValues("secteur");
			
			contr.addEntreprise(e,tabTech,tabAct);
			String location=request.getParameter("location");
			response.sendRedirect(location);
		}
		else if(fct.equals("editEntreprise")){
			String idEntreprise=request.getParameter("idEntreprise");
			Entreprise e=contr.getEntreprise(Integer.parseInt(idEntreprise));
			request.setAttribute("Entreprise", e);
			request.getRequestDispatcher("/WEB-INF/Entreprise.jsp");
			
		}
		else if(fct.equals("deleteEntreprise")){
			int id=Integer.parseInt(request.getParameter("idEntreprise"));
			contr.removeEntreprise(id);
			/////response.sendRedirect("");
		}
		
		else if(fct.equals("modifEntreprise")){
			
			String nom=request.getParameter("nomEnt");
			String urlSite=request.getParameter("urlSite");
			String numRue=request.getParameter("numRue");
			String codePostal=request.getParameter("CodePostal");
			String ville=request.getParameter("ville");
			String pays=request.getParameter("pays");
			String rue=request.getParameter("rue");
			int id=Integer.parseInt(request.getParameter("idEntreprise"));
			Adresse a=new Adresse(0, rue, Integer.parseInt(numRue), Integer.parseInt(codePostal), ville, pays);
			Entreprise e=new Entreprise(id, urlSite, nom, a);
			String [] tabTech=request.getParameterValues("Technologie");
			
			
			String [] tabAct=request.getParameterValues("secteur");
			
			
			contr.updateEntreprise(e,tabTech,tabAct);
		}
	

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	
	
}