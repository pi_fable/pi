package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAOPackage.DAO;
import DAOPackage.DAO_Contact;
import DAOPackage.DAO_Eleve;
import DAOPackage.DAO_Entreprise;
import DAOPackage.DAO_Evaluation;
import DAOPackage.DAO_Evenement;
import DAOPackage.DAO_MaitreDeStage;
import DAOPackage.DAO_PropositionDeStage;
import DAOPackage.DAO_SecteurActivite;
import DAOPackage.DAO_Technologie;
import POJO.Contact;
import POJO.Eleve;
import POJO.Entreprise;
import POJO.Evaluation;
import POJO.Evenement;
import POJO.MaitreDeStage;
import POJO.PropositionDeStage;
import POJO.SecteurActivite;
import POJO.Technologie;

//@WebServlet("/FormContact")
//@WebServlet("/Recherche")
@WebServlet(urlPatterns = {"/Profil", "/accueil", "/deconnexion"})
public class ServletControlleur extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String res1 = request.getRequestURL().toString();
        String[] res = res1.split("/");
        String curJSP = res[res.length - 1];
        if (curJSP.equals("accueil")) {
            this.getServletContext().getRequestDispatcher("/WEB-INF/Affichage.jsp").forward(request, response);
        } else if (curJSP.equals("deconnexion")) {
            request.getSession().invalidate();
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
