package Servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.BEAN_gestionnaireFormulaire;
import DAOPackage.ConnexionBD;
import Metier.Controlleur;
import POJO.Formulaire;


@WebServlet(name = "cs", urlPatterns = {"/ServletGestionnaireFromulaire", "/listerFormulaire", "/rechercherFormulaire", "/saisieFormulaire", "/ajoutFormulaire", "/updateFormulaire", "/deleteFormulaire"})

public class ServletGestionnaireFromulaire extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    //Controlleur m�tier
    private Controlleur metier;

    public void init() throws ServletException {
        //Connection BD
        System.err.println("INIT ServletGestionnaireFormulaire");
        ConnexionBD connect = new ConnexionBD();
        metier = new Controlleur(connect.getConnexion());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getServletPath();
        System.err.println(path);
        /*String res1=request.getRequestURL().toString();
		String[]res=res1.split("/");
		System.err.println(res[res.length-1]);
		*/
        //if(path.equals(""))
        if (path.equals("/rechercherFormulaire")) {
            String motCle = request.getParameter("motCle");
            BEAN_gestionnaireFormulaire bean = new BEAN_gestionnaireFormulaire();
            bean.setMc(motCle);
            System.err.println(motCle);
            List<Formulaire> listFrom = metier.getAllFormulaire();
            //System.err.println(listFrom);
            bean.setListForm(listFrom);
            request.setAttribute("bean", bean);
            request.getRequestDispatcher("/WEB-INF/GestionnaireFormulaire.jsp").forward(request, response);

        } else if (path.equals("/listerFormulaire")) {
            BEAN_gestionnaireFormulaire bean = new BEAN_gestionnaireFormulaire();
            List<Formulaire> listFrom = metier.getAllFormulaire();
            //System.err.println(listFrom);
            bean.setListForm(listFrom);
            request.setAttribute("bean", bean);
            request.getRequestDispatcher("/WEB-INF/GestionnaireFormulaire.jsp").forward(request, response);
        } else if (path.equals("/saisieFormulaire")) {
            //new Formulaire(idForm, titre, periode, dateLimite)
            String titre = "";
            String periode = "";
            String testDate = "20-04-2017";
            String[] tmp = testDate.split("-");
            testDate = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
            LocalDate dateLimit = LocalDate.parse(testDate);
            BEAN_gestionnaireFormulaire bean = new BEAN_gestionnaireFormulaire();
            Formulaire formulaire = new Formulaire(0, titre, periode, dateLimit);

            bean.setForm(formulaire);
            System.err.println(path + " " + bean.getForm());
            request.setAttribute("bean", bean);
            request.getRequestDispatcher("/WEB-INF/SaisieFormulaire.jsp").forward(request, response);

        } else if (path.equals("/ajoutFormulaire")) {
            System.err.println(path);
            //Integer id = Integer.parseInt(request.getParameter("id"));
            //Integer id = 0;
            String titre = request.getParameter("titre");
            String periode = request.getParameter("periode");
            String dateTemp = request.getParameter("date");
            String[] tmp = dateTemp.split("-");
            dateTemp = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
            LocalDate dateLimite = LocalDate.parse(dateTemp);
            BEAN_gestionnaireFormulaire bean = new BEAN_gestionnaireFormulaire();
            Formulaire formulaire = new Formulaire(0, titre, periode, dateLimite);

            metier.addFormulaire(formulaire);

            bean.setForm(formulaire);
            System.err.println(path + " " + bean.getForm());
            //request.setAttribute("bean", bean);
            request.getRequestDispatcher("GestionnaireFormulaire").forward(request, response);

        } else if (path.equals("/updateFormulaire")) {
            System.err.println(path);
            Integer id = Integer.parseInt(request.getParameter("id"));
            //Integer id = 0;
            String titre = request.getParameter("titre");
            String periode = request.getParameter("periode");
            String dateTemp = request.getParameter("dateLimite");
            String[] tmp = dateTemp.split("-");
            dateTemp = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
            LocalDate dateLimite = LocalDate.parse(dateTemp);
            BEAN_gestionnaireFormulaire bean = new BEAN_gestionnaireFormulaire();
            Formulaire formulaire = new Formulaire(id, titre, periode, dateLimite);

            bean.setForm(formulaire);
            System.err.println(path + " " + bean.getForm());
            request.setAttribute("bean", bean);
            request.getRequestDispatcher("/WEB-INF/SaisieFormulaire.jsp").forward(request, response);
        } else if (path.equals("/deleteFormulaire")) {
            System.err.println(path);
            //Integer id = Integer.parseInt(request.getParameter("id"));


            //if(metier.removeFormulaire(e))
        } else {
            response.sendError(response.SC_NOT_FOUND);
        }
    }

    //Cr�ation Controlleur m�tier
    //private Controlleur metier = new Controlleur(connect.getConnexion());


}