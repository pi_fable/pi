package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.BEAN_gestionnaireFormulaire;
import DAOPackage.DAO;
import DAOPackage.DAO_Evaluation;
import Metier.Controlleur;
import Metier.PDFCreator;
import POJO.*;



@WebServlet(urlPatterns={"/FormContact","/SaisieEntreprise","/administration","/FormEleve","/listeEleve","/FormulaireStage","/Evaluations","/FormPropositionDeStage","/GestionnaireFormulaire"})
public class ServletContrProfesseur extends HttpServlet {
    private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String res1=request.getRequestURL().toString();
		String[]res=res1.split("/");
		String curJSP=res[res.length-1];
		Controlleur cont=(Controlleur)request.getSession().getAttribute("controleur");
		if(curJSP.equals("FormContact") | curJSP.equals("FormMaitreDeStage") ){
			
			List<Entreprise> ent=cont.getAllEntreprise();
			
			List<Technologie> tech=cont.getAllTechnologie();
			
			List<SecteurActivite> act=cont.getAllSecteurActivite();
			request.setAttribute("ListeEntreprise", ent);
			request.setAttribute("ListeTechnologie", tech);
			request.setAttribute("ListeSecteur", act);
			if(curJSP.equals("FormMaitreDeStage"))request.setAttribute("type", "Maitre de stage");
			else request.setAttribute("type", "Contact");
		}
		else if(curJSP.equals("FormulaireStage")){
			String valider=request.getParameter("valider");
			
			if(valider==null){
				
				List<Entreprise> ent=cont.getAllEntreprise();
				request.setAttribute("entreprise", ent);
				request.setAttribute("etat", "Description du stage");
				request.getSession().setAttribute("typeValidation", "new");
			}
			else{
				String idProp=request.getParameter("idProp");///!!!!  � red�finir par apr�s
				
				PropositionDeStage p=cont.getPropositionDeStage((Integer.parseInt(idProp)));
				request.setAttribute("proposition", p);
				request.setAttribute("etat", "Proposition � valider");
				request.getSession().setAttribute("typeValidation", "old");
				request.getSession().setAttribute("idProp", p.getIdProp());
			}  
	
			List<Eleve> eleve=cont.getAllEleve();
			List<Contact> contact=cont.getAllContact();
			request.setAttribute("eleve", eleve);
			
			request.setAttribute("contact", contact);
		}
		else if(curJSP.equals("Evaluations")){
			
			List<Evaluation> listEve=cont.getAllEvaluation();
			PDFCreator p=new PDFCreator(listEve.get(0));
			request.setAttribute("eval", listEve);
		}
		else if(curJSP.equals("consulterEva")){
			
		}
		else if(curJSP.equals("FormPropositionDeStage")){
			
			List<Entreprise> ent=cont.getAllEntreprise();
			request.setAttribute("entreprise", ent);
		}
		else if(curJSP.equals("GestionnaireFormulaire")){
			BEAN_gestionnaireFormulaire bean = new BEAN_gestionnaireFormulaire();
			List<Formulaire> listFrom = cont.getAllFormulaire();
			//System.err.println(listFrom);
			bean.setListForm(listFrom);
			request.setAttribute("bean", bean);
		}
		else if(curJSP.equals("SaisieEntreprise")){
			
			
			List<Technologie> tech=cont.getAllTechnologie();
			
			List<SecteurActivite> act=cont.getAllSecteurActivite();
			
			request.setAttribute("ListeTechnologie", tech);
			request.setAttribute("ListeSecteur", act);
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/"+curJSP+".jsp").forward(request, response);
	}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        String res1 = request.getRequestURL().toString();
        String[] res = res1.split("/");
        String curJSP = res[res.length - 1];
        Controlleur cont = (Controlleur) request.getSession().getAttribute("controleur");
        if (curJSP.equals("FormContact") | curJSP.equals("FormMaitreDeStage")) {

            List<Entreprise> ent = cont.getAllEntreprise();

            List<Technologie> tech = cont.getAllTechnologie();

            List<SecteurActivite> act = cont.getAllSecteurActivite();
            request.setAttribute("ListeEntreprise", ent);
            request.setAttribute("ListeTechnologie", tech);
            request.setAttribute("ListeSecteur", act);
            if (curJSP.equals("FormMaitreDeStage")) request.setAttribute("type", "Maitre de stage");
            else request.setAttribute("type", "Contact");
        } else if (curJSP.equals("FormulaireStage")) {
            String valider = request.getParameter("valider");

            if (valider == null) {

                List<Entreprise> ent = cont.getAllEntreprise();
                request.setAttribute("entreprise", ent);
                request.setAttribute("etat", "Description du stage");
                request.getSession().setAttribute("typeValidation", "new");
            } else {
                String idProp = request.getParameter("idProp");///!!!!  � red�finir par apr�s

                PropositionDeStage p = cont.getPropositionDeStage((Integer.parseInt(idProp)));
                request.setAttribute("proposition", p);
                request.setAttribute("etat", "Proposition � valider");
                request.getSession().setAttribute("typeValidation", "old");
                request.getSession().setAttribute("idProp", p.getIdProp());
            }

            List<Eleve> eleve = cont.getAllEleve();
            List<Contact> contact = cont.getAllContact();
            request.setAttribute("eleve", eleve);

            request.setAttribute("contact", contact);
        } else if (curJSP.equals("Evaluations")) {

            List<Evaluation> listEve = cont.getAllEvaluation();
            PDFCreator p = new PDFCreator(listEve.get(0));
            request.setAttribute("eval", listEve);
        } else if (curJSP.equals("consulterEva")) {

        } else if (curJSP.equals("FormPropositionDeStage")) {

            List<Entreprise> ent = cont.getAllEntreprise();
            request.setAttribute("entreprise", ent);
        } else if (curJSP.equals("GestionnaireFormulaire")) {
            BEAN_gestionnaireFormulaire bean = new BEAN_gestionnaireFormulaire();
            List<Formulaire> listFrom = cont.getAllFormulaire();
            //System.err.println(listFrom);
            bean.setListForm(listFrom);
            request.setAttribute("bean", bean);
        }
        this.getServletContext().getRequestDispatcher("/WEB-INF/" + curJSP + ".jsp").forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
