package Servlet;

import java.io.IOException;
import java.sql.Connection;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAOPackage.DAO;
import DAOPackage.DAO_Adresse;
import DAOPackage.DAO_Contact;
import DAOPackage.DAO_Evenement;
import DAOPackage.DAO_Planning;
import Metier.Controlleur;
import POJO.Adresse;
import POJO.Contact;
import POJO.Evenement;
import POJO.Planning;

@WebServlet(urlPatterns = {"/addEvent", "/modifEvent", "/Planning", "/suppEvent", "/editEvent"})
public class ServletPlanning extends HttpServlet {
    private static final long serialVersionUID = 1L;


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        String res1 = request.getRequestURL().toString();
        String[] res = res1.split("/");
        String curJSP = res[res.length - 1];
        Controlleur contr = (Controlleur) request.getSession().getAttribute("controleur");
        if (curJSP.equals("addEvent")) {
            String date = request.getParameter("date");
            String sujet = request.getParameter("sujet");
            String desc = request.getParameter("desc");
            String rue = request.getParameter("rue");
            String numRue = request.getParameter("numRue");
            String codePostal = request.getParameter("CodePostal");
            String ville = request.getParameter("ville");
            String pays = request.getParameter("pays");
            String[] contact = request.getParameterValues("Contact");
            LocalDate date1 = LocalDate.parse(contr.convert(date));
            Adresse a = new Adresse(0, rue, Integer.parseInt(numRue), Integer.parseInt(codePostal), ville, pays);
            Evenement even = new Evenement(0, date1, sujet, desc, a, new Planning(date1.getYear()));
            contr.addEvenement(even, contact);

            request.getRequestDispatcher("/Planning").forward(request, response);
        } else if (curJSP.equals("modifEvent")) {
            String date = request.getParameter("date");
            String sujet = request.getParameter("sujet");
            String desc = request.getParameter("desc");
            String rue = request.getParameter("rue");
            String numRue = request.getParameter("numRue");
            String codePostal = request.getParameter("CodePostal");
            String ville = request.getParameter("ville");
            String pays = request.getParameter("pays");
            String[] contact = request.getParameterValues("Contact");
            int id = Integer.parseInt(request.getParameter("idEven"));
            LocalDate date1 = LocalDate.parse(contr.convert(date));
            Adresse a = new Adresse(0, rue, Integer.parseInt(numRue), Integer.parseInt(codePostal), ville, pays);
            Evenement even = new Evenement(id, date1, sujet, desc, a, new Planning(date1.getYear()));
            contr.updateEvenement(even, contact);

            response.sendRedirect("Planning");
        } else if (curJSP.equals("suppEvent")) {
            int id = Integer.parseInt(request.getParameter("id"));
            contr.removeEvenement(id);
            response.sendRedirect("Planning");
        } else if (curJSP.equals("Planning")) {
            String sujet = request.getParameter("subject");
            String annee = request.getParameter("year");
            String dateDeb = request.getParameter("dateDeb");
            String dateFin = request.getParameter("dateFin");
            List<Evenement> even;
            if (sujet == null && annee == null && dateDeb == null && dateFin == null) {
                even = contr.getAllEvenement(LocalDate.now().getYear());
                request.setAttribute("Year", LocalDate.now().getYear());
            } else {
                even = contr.searchEvenementBy(sujet, annee, dateDeb, dateFin);
            }

            List<Contact> con = contr.getAllContact();
            request.setAttribute("sujet", sujet);
            request.setAttribute("Year", annee);
            request.setAttribute("dateDeb", dateDeb);
            request.setAttribute("dateFin", dateFin);
            request.setAttribute("Planning", even);
            request.setAttribute("Contact", con);
            request.getRequestDispatcher("/WEB-INF/Planning.jsp").forward(request, response);
        } else if (curJSP.equals("editEvent")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Evenement e = contr.getEvenement(id);
            List<Contact> con = contr.getAllContact();
            request.setAttribute("Contact", con);
            request.setAttribute("EvenMod", e);
            request.getRequestDispatcher("/WEB-INF/Planning.jsp").forward(request, response);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }
}
