package Servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


@WebServlet("/upload")
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class UploadServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        try {
            ServletFileUpload fileUpload = new ServletFileUpload();
            FileItemIterator items = (FileItemIterator) fileUpload.getItemIterator(request);

            while (items.hasNext()) {
                FileItemStream item = items.next();
                if (item.isFormField()) {
                    /* Traiter les champs classiques ici (input type="text|radio|checkbox|etc", select, etc). */
                    String nomChamp = item.getFieldName();
                    String valeurChamp = item.getFieldName();
					/* ... (traitement � faire) */
                } else {
                    System.err.println("test");
					/* Traiter les champs de type fichier (input type="file"). */
                    InputStream contenuFichier = item.openStream();
                    OutputStream out = new FileOutputStream(new File("../../PropStage/test.png"));
                    int read = 0;
                    final byte[] bytes = new byte[1024];

                    while ((read = contenuFichier.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }

                    contenuFichier.close();
                    out.close();
			        
					/* ... (traitement � faire) */
                }
            }
        } catch (Exception e) {
            throw new ServletException("�chec de l'analyse de la requ�te multipart.", e);
        }


        request.getRequestDispatcher("/affichage").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
