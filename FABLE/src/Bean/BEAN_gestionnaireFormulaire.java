package Bean;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import POJO.Formulaire;

public class BEAN_gestionnaireFormulaire {

    private String mc;
    private List<Formulaire> listForm = new ArrayList<Formulaire>();
    private Formulaire form = new Formulaire();

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public List<Formulaire> getListForm() {
        return listForm;
    }

    public void setListForm(List<Formulaire> listForm) {
        this.listForm = listForm;
    }

    public Formulaire getForm() {
        return form;
    }

    public void setForm(Formulaire form) {
        this.form = form;
    }


}