package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Contact;
import POJO.MaitreDeStage;

public class DAO_MaitreDeStage extends DAO<MaitreDeStage> {

    private String find_by_id = "select * from maitredestage where idMaitre=?";
    private String create = "insert into maitredestage(motDePasse, idContact) values(?,?)";
    private String update = "update maitredestage set motDePasse=?, idContact=? where idMaitre=?";
    private String delete = "delete from maitredestage where idMaitre = ?";
    private String find_all = "select * from maitredestage";
    private String find_by_mc = "select * from maitredestage where idContact in (select idContact from Contact where nom like ?) ";

    public DAO_MaitreDeStage(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public MaitreDeStage find(int Id) {
        //find_by_id = "select * from MaitreDeStage where idMaitre=?";
        MaitreDeStage pojoMaitreDeStage = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                //recherche heritage contact
                DAO_Contact daoContact = new DAO_Contact(connection);
                Contact pojoContact = daoContact.find(rs.getInt("idContact"));

                pojoMaitreDeStage = new MaitreDeStage(pojoContact.getIdContact(), pojoContact.getNom(), pojoContact.getPrenom(), pojoContact.getTelephone(), pojoContact.getAdresseMail(), pojoContact.getPoste(), pojoContact.getEntrep(), rs.getInt("idMaitre"), rs.getString("motDePasse"));

            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoMaitreDeStage == null) throw new RuntimeException("Feedback " + Id + " introuvable");
        return pojoMaitreDeStage;
    }

    public MaitreDeStage findByIdContact(int id) {
        MaitreDeStage pojoMaitreDeStage = null;
        String request = "select *from maitredestage where idcontact=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(request);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                //recherche heritage contact
                DAO_Contact daoContact = new DAO_Contact(connection);
                Contact pojoContact = daoContact.find(rs.getInt("idContact"));

                pojoMaitreDeStage = new MaitreDeStage(pojoContact.getIdContact(), pojoContact.getNom(), pojoContact.getPrenom(), pojoContact.getTelephone(), pojoContact.getAdresseMail(), pojoContact.getPoste(), pojoContact.getEntrep(), rs.getInt("idMaitre"), rs.getString("motDePasse"));

            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return pojoMaitreDeStage;
    }

    @Override
    public void create(MaitreDeStage objectToInsertInDB) {
        //create = "insert into MaitreDeStage(motDePasse, idContact) values(?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setString(1, objectToInsertInDB.getMotDePasse());
            ps.setInt(2, objectToInsertInDB.getIdContact());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(MaitreDeStage objectToUpdateInDB) {
        //update = "update MaitreDeStage set motDePasse=?, idContact=? where idMaitre=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setString(1, objectToUpdateInDB.getMotDePasse());
            ps.setInt(2, objectToUpdateInDB.getIdContact());
            ps.setInt(3, objectToUpdateInDB.getIdMaitre());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(MaitreDeStage objectToDeleteFromDB) {
        //delete = "delete from MaitreDeStage where idMaitre = ?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdMaitre());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<MaitreDeStage> getList() {
        //find_all = "select * from MaitreDeStage";
        List<MaitreDeStage> maitreDeStageList = new ArrayList<MaitreDeStage>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                DAO_Contact daoContact = new DAO_Contact(connection);
                Contact pojoContact = daoContact.find(rs.getInt("idContact"));

                MaitreDeStage pojoMaitreDeStage = new MaitreDeStage(pojoContact.getIdContact(), pojoContact.getNom(), pojoContact.getPrenom(), pojoContact.getTelephone(), pojoContact.getAdresseMail(), pojoContact.getPoste(), pojoContact.getEntrep(), rs.getInt("isMaitre"), rs.getString("motDePasse"));

                maitreDeStageList.add(pojoMaitreDeStage);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return maitreDeStageList;
    }

    @Override
    public List<MaitreDeStage> getListByMotcle(String mc) {
        //find_by_mc = "select * from MaitreDeStage where idContact in (select idContact from Contact where nom like ?) ";
        List<MaitreDeStage> maitreDeStageList = new ArrayList<MaitreDeStage>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ps.setString(1, "%" + mc + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                DAO_Contact daoContact = new DAO_Contact(connection);
                Contact pojoContact = daoContact.find(rs.getInt("idContact"));

                MaitreDeStage pojoMaitreDeStage = new MaitreDeStage(pojoContact.getIdContact(), pojoContact.getNom(), pojoContact.getPrenom(), pojoContact.getTelephone(), pojoContact.getAdresseMail(), pojoContact.getPoste(), pojoContact.getEntrep(), rs.getInt("isMaitre"), rs.getString("motDePasse"));

                maitreDeStageList.add(pojoMaitreDeStage);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return maitreDeStageList;
    }

}
