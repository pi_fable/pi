package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Evaluation;

public class DAO_Evaluation extends DAO<Evaluation> {

    private String find_by_id = "select * from evaluation where numEva=?";
    private String create = "insert into evaluation(dateRemise, Cotation, idStage, idMaitre) values(?,?,?,?)";
    private String update = "update evaluation set dateRemise=?, Cotation=?, idStage=?, idMaitre=? where numEva=?";
    private String delete = "delete from evaluation where numEva = ?";
    private String find_all = "select * from evaluation";
    private String find_by_mc = "select * from evaluation where dateRemise like ?"; //ATTENTION


    public DAO_Evaluation(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub

    }

    @Override
    public Evaluation find(int Id) {
        //find_by_id = "select * from evaluation where numEva=?";
        Evaluation pojoEvaluation = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoEvaluation = new Evaluation();
                pojoEvaluation.setNumEva(rs.getInt("numEva"));
                ;
                pojoEvaluation.setDateRemise(rs.getDate("dateRemise"));
                pojoEvaluation.setCotation(rs.getInt("Cotation"));
                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoEvaluation.setConserneStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Maitre de stage
                DAO_MaitreDeStage daoMaitreDeStage = new DAO_MaitreDeStage(connection);
                pojoEvaluation.setConserneMaitreDeStage(daoMaitreDeStage.find(rs.getInt("idMaitre")));

            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoEvaluation == null) throw new RuntimeException("Evaluation " + Id + " introuvable");
        return pojoEvaluation;
    }

    @Override
    public void create(Evaluation objectToInsertInDB) {
        //create = "insert into Evaluation(dateRemise, Cotation, idStage, idMaitre) values(?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setDate(1, objectToInsertInDB.getDateRemise());
            ps.setInt(2, objectToInsertInDB.getCotation());
            ps.setInt(3, objectToInsertInDB.getConserneStage().getIdStage());
            ps.setInt(4, objectToInsertInDB.getConserneMaitreDeStage().getIdMaitre());


            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(Evaluation objectToUpdateInDB) {
        //update = "update evaluation set dateRemise=?, Cotation=?, idStage=?, idMaitre=? where numEva=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setDate(1, objectToUpdateInDB.getDateRemise());
            ps.setInt(2, objectToUpdateInDB.getCotation());
            ps.setInt(3, objectToUpdateInDB.getConserneStage().getIdStage());
            ps.setInt(4, objectToUpdateInDB.getConserneMaitreDeStage().getIdMaitre());

            ps.setInt(5, objectToUpdateInDB.getNumEva());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Evaluation objectToDeleteFromDB) {
        //delete = "delete from evaluation where numFeedback = ?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getNumEva());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Evaluation> getList() {
        //find_all = "select * from evaluation";
        List<Evaluation> evaluationList = new ArrayList<Evaluation>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Evaluation pojoEvaluation = new Evaluation();
                pojoEvaluation.setNumEva(rs.getInt("numEva"));
                ;
                pojoEvaluation.setDateRemise(rs.getDate("dateRemise"));
                pojoEvaluation.setCotation(rs.getInt("Cotation"));
                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoEvaluation.setConserneStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Maitre de stage
                DAO_MaitreDeStage daoMaitreDeStage = new DAO_MaitreDeStage(connection);
                pojoEvaluation.setConserneMaitreDeStage(daoMaitreDeStage.find(rs.getInt("idMaitre")));

                evaluationList.add(pojoEvaluation);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return evaluationList;
    }

    @Override
    public List<Evaluation> getListByMotcle(String mc) {
        //find_by_mc = "select * from evaluation where dateRemise like ?";
        List<Evaluation> evaluationList = new ArrayList<Evaluation>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ps.setString(1, "%" + mc + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Evaluation pojoEvaluation = new Evaluation();
                pojoEvaluation.setNumEva(rs.getInt("numEva"));
                ;
                pojoEvaluation.setDateRemise(rs.getDate("dateRemise"));
                pojoEvaluation.setCotation(rs.getInt("Cotation"));
                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoEvaluation.setConserneStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Maitre de stage
                DAO_MaitreDeStage daoMaitreDeStage = new DAO_MaitreDeStage(connection);
                pojoEvaluation.setConserneMaitreDeStage(daoMaitreDeStage.find(rs.getInt("idMaitre")));

                evaluationList.add(pojoEvaluation);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return evaluationList;
    }

}
