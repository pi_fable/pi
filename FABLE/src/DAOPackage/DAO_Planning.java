package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Contact;
import POJO.Evenement;
import POJO.Planning;

public class DAO_Planning extends DAO<Planning> {

    private String find_by_id = "select * from planning where AnneeScolaire=?";
    private String create = "insert into planning(AnneeScolaire) values(?)";
    private String update = "update planning set AnneeScolaire=? where AnneeScolaire=?";//ATTENTION
    private String delete = "delete from planning where AnneeScolaire = ?";
    private String find_all = "select * from planning";
    //private String find_by_mc = "select * from Planning where AnneeScolaire =?";

    public DAO_Planning(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Planning find(int Id) {
        //find_by_id = "select * Planning adresse where AnneeScolaire=?";
        Planning pojoPlanning = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoPlanning = new Planning(rs.getInt("AnneeScolaire"));
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoPlanning == null) throw new RuntimeException("Planning " + Id + " introuvable");
        return pojoPlanning;
    }

    @Override
    public void create(Planning objectToInsertInDB) {
        //create = "insert into Planning(AnneeScolaire) values(?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setInt(1, objectToInsertInDB.getAnneeScolaire());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    //A changer!!
    @Override
    public void update(Planning objectToUpdateInDB) {
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setInt(1, objectToUpdateInDB.getAnneeScolaire());
            ps.setInt(2, objectToUpdateInDB.getAnneeScolaire());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Planning objectToDeleteFromDB) {
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getAnneeScolaire());

            this.deleteEvenement(objectToDeleteFromDB);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public List<Planning> getList() {
        //private String find_all = "select * from Planning";
        List<Planning> plannings = new ArrayList<Planning>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Planning pojoPlanning = new Planning(rs.getInt("AnneeScolaire"));
                plannings.add(pojoPlanning);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return plannings;
    }

    @Override
    public List<Planning> getListByMotcle(String mc) {
        /*
		List<Adresse> adrs = new ArrayList<Adresse>();
		try {
			PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
			ps.setString(1, "%"+mc+"%");
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Adresse adr = new Adresse(rs);
				
				adrs.add(adr);
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return adrs;
		*/
        return null;
    }

    public List<Evenement> getAllEvenement(int annee) {
        String query = "select * from evenement where AnneeScolaire=? order by DateEven asc";
        List<Evenement> events = new ArrayList<Evenement>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, annee);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Evenement pojoEvent = new Evenement();
                pojoEvent.setNumEven(rs.getInt("NumEven"));
                pojoEvent.setDateEven(rs.getDate("DateEven").toLocalDate());
                pojoEvent.setSujet(rs.getString("Sujet"));
                pojoEvent.setDescription(rs.getString("Description"));

                //Ajout Adresse
                DAO_Adresse daoAdr = new DAO_Adresse(connection);

                pojoEvent.setAdrs(daoAdr.find(rs.getInt("idAdresse")));

                //Ajout Planning;
                DAO_Planning daoPlanning = new DAO_Planning(connection);

                pojoEvent.setPlan(daoPlanning.find(rs.getInt("AnneeScolaire")));

                //Ajout participant
                DAO_Evenement daoEvent = new DAO_Evenement(connection);
                pojoEvent.setListContact(daoEvent.getAllParticipant(rs.getInt("NumEven")));

                events.add(pojoEvent);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return events;
    }

    private void deleteEvenement(Planning objectDelete) {
        DAO_Evenement daoEvent = new DAO_Evenement(connection);
        for (Evenement element : this.getAllEvenement(objectDelete.getAnneeScolaire())) {
            daoEvent.delete(element);
        }
    }


}
