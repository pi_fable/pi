package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.SecteurActivite;

public class DAO_SecteurActivite extends DAO<SecteurActivite> {
    private String find_by_id = "select * from secteuractivite where idActivite=?";
    private String create = "insert into secteuractivite(nomActivite) values(?)";
    private String update = "update secteuractivite set nomActivite=? where idActivite=?";
    private String delete = "delete from secteuractivite where idActivite = ?";
    private String find_all = "select * from secteuractivite";
    private String find_by_mc = "select * from secteuractivite where nomActivite like ?";

    public DAO_SecteurActivite(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public SecteurActivite find(int Id) {
        //find_by_id = "select * from SecteurActivite where idActivite=?";
        SecteurActivite pojoActivite = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoActivite = new SecteurActivite();
                pojoActivite.setIdActivite(rs.getInt("idActivite"));
                pojoActivite.setNomActivite(rs.getString("nomActivite"));
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoActivite == null) throw new RuntimeException("SecteurActivite " + Id + " introuvable");
        return pojoActivite;
    }

    @Override
    public void create(SecteurActivite objectToInsertInDB) {
        //create = "insert into SecteurActivite(nomActivite) values(?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setString(1, objectToInsertInDB.getNomActivite());

	@Override
	public List<SecteurActivite> getList() {
		//find_all = "select * from SecteurActivite";
		List<SecteurActivite> sectActs = new ArrayList<SecteurActivite>();
		try {
			PreparedStatement ps = this.connection.prepareStatement(find_all);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				SecteurActivite pojoSectAct = new SecteurActivite();
				pojoSectAct.setIdActivite(rs.getInt("idActivite"));
				pojoSectAct.setNomActivite(rs.getString("nomActivite"));
				
				sectActs.add(pojoSectAct);
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return sectActs;
	}
	public int getLastIdCreate(){
		int id=0;
		try {
			PreparedStatement ps=this.connection.prepareStatement("");
			ResultSet r=ps.executeQuery("SELECT LAST_INSERT_ID()");
			r.next();
			id=r.getInt(1);
			ps.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	@Override
	public List<SecteurActivite> getListByMotcle(String mc) {
		//find_by_mc = "select * from SecteurActivite where nomActivite like ?";
		List<SecteurActivite> sectActs = new ArrayList<SecteurActivite>();
		try {
			PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
			ps.setString(1, "%"+mc+"%");
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				SecteurActivite pojoSectAct = new SecteurActivite();
				pojoSectAct.setIdActivite(rs.getInt("idActivite"));
				pojoSectAct.setNomActivite(rs.getString("nomActivite"));
				sectActs.add(pojoSectAct);
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return sectActs;
	}
}