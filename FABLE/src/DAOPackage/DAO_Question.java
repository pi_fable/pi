package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Question;

public class DAO_Question extends DAO<Question> {

    private String find_by_id = "select * from question where numQuestion=?";
    private String create = "insert into question(types, intitule, obligatoire) values(?,?,?)";
    private String update = "update question set types=?, intitule=?, obligatoire=? where numQuestion=?";
    private String delete = "delete from question where numQuestion = ?";
    private String find_all = "select * from question";
    private String find_by_mc = "select * from question where titre intitule like ?";

    public DAO_Question(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Question find(int Id) {
        //find_by_id = "select * from Question where numQuestion=?";
        Question pojoQuestion = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoQuestion = new Question();
                pojoQuestion.setNumQuestion(rs.getInt("numQuestion"));
                pojoQuestion.setTypes(rs.getString("types"));
                pojoQuestion.setIntitule(rs.getString("intitule"));
                if (rs.getString("obligatoire").equals("true")) {
                    pojoQuestion.setObligatoire(true);
                } else pojoQuestion.setObligatoire(false);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoQuestion == null) throw new RuntimeException("Question " + Id + " introuvable");
        return pojoQuestion;
    }

    @Override
    public void create(Question objectToInsertInDB) {
        //create = "insert into Question(types, intitule, obligatoire) values(?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);

            ps.setString(1, objectToInsertInDB.getTypes());
            ps.setString(2, objectToInsertInDB.getIntitule());
            if (objectToInsertInDB.getObligatoire())
                ps.setString(1, "true");
            else ps.setString(3, "false");

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(Question objectToUpdateInDB) {
        //update = "update Question set types=?, intitule=?, obligatoire=? where numQuestion=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setString(1, objectToUpdateInDB.getTypes());
            ps.setString(2, objectToUpdateInDB.getIntitule());
            if (objectToUpdateInDB.getObligatoire())
                ps.setString(1, "true");
            else ps.setString(3, "false");

            ps.setInt(4, objectToUpdateInDB.getNumQuestion());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Question objectToDeleteFromDB) {
        //delete = "delete from Question where numQuestion = ?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getNumQuestion());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Question> getList() {
        //find_all = "select * from Question";
        List<Question> questionList = new ArrayList<Question>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question pojoQuestion = new Question();
                pojoQuestion.setNumQuestion(rs.getInt("numQuestion"));
                pojoQuestion.setTypes(rs.getString("types"));
                pojoQuestion.setIntitule(rs.getString("intitule"));
                if (rs.getString("obligatoire").equals("true")) {
                    pojoQuestion.setObligatoire(true);
                } else pojoQuestion.setObligatoire(false);

                questionList.add(pojoQuestion);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return questionList;
    }

    @Override
    public List<Question> getListByMotcle(String mc) {
        //find_by_mc = "select * from Question where titre intitule like ?";
        List<Question> questionList = new ArrayList<Question>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);

            ps.setString(1, "%" + mc + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question pojoQuestion = new Question();
                pojoQuestion.setNumQuestion(rs.getInt("numQuestion"));
                pojoQuestion.setTypes(rs.getString("types"));
                pojoQuestion.setIntitule(rs.getString("intitule"));
                if (rs.getString("obligatoire").equals("true")) {
                    pojoQuestion.setObligatoire(true);
                } else pojoQuestion.setObligatoire(false);

                questionList.add(pojoQuestion);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return questionList;
    }

}