package DAOPackage;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Contact;
import POJO.Entreprise;
import POJO.Evenement;

public class DAO_Evenement extends DAO<Evenement> {
    private String find_by_id = "select * from evenement where NumEven=?";
    private String create = "insert into evenement(DateEven, Sujet, Description, idAdresse, AnneeScolaire) values(?,?,?,?,?)";
    private String update = "update evenement set DateEven=?, Sujet=?, Description=?, idAdresse=?, AnneeScolaire=?  where NumEven=?";
    private String delete = "delete from evenement where NumEven = ?";
    private String find_all = "select * from evenement order by DateEven asc";
    private String find_by_mc = "select * from evenement where Sujet like ? order by DateEven asc";

    public DAO_Evenement(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Evenement find(int Id) {
        //"select * from Evenement where NumEven=?";
        Evenement pojoEvent = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoEvent = new Evenement();
                pojoEvent.setNumEven(rs.getInt("NumEven"));
                pojoEvent.setDateEven(rs.getDate("DateEven").toLocalDate());
                pojoEvent.setSujet(rs.getString("Sujet"));
                pojoEvent.setDescription(rs.getString("Description"));

                //Ajout Adresse
                DAO_Adresse daoAdr = new DAO_Adresse(connection);

                pojoEvent.setAdrs(daoAdr.find(rs.getInt("idAdresse")));

                //Ajout Planning;
                DAO_Planning daoPlanning = new DAO_Planning(connection);

                pojoEvent.setPlan(daoPlanning.find(rs.getInt("AnneeScolaire")));
                //Ajout participant
                pojoEvent.setListContact(this.getAllParticipant(Id));
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoEvent == null) throw new RuntimeException("Evenement " + Id + " introuvable");
        return pojoEvent;
    }

    @Override
    public void create(Evenement objectToInsertInDB) {
        //"insert into evenement(DateEven, Sujet, Description, idAdresse, AnneeScolaire) values(?,?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setDate(1, Date.valueOf(objectToInsertInDB.getDateEven()));
            ps.setString(2, objectToInsertInDB.getSujet());
            ps.setString(3, objectToInsertInDB.getDescription());
            ps.setInt(4, objectToInsertInDB.getAdrs().getIdAdresse());
            ps.setInt(5, objectToInsertInDB.getPlan().getAnneeScolaire());

            ps.executeUpdate();
            ResultSet r = ps.executeQuery("SELECT LAST_INSERT_ID()");
            r.next();
            int id = r.getInt(1);
            ps.close();
            objectToInsertInDB.setNumEven(id);
            this.lierParticipant(objectToInsertInDB);

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void update(Evenement objectToUpdateInDB) {
        //"update = "update evenement set DateEven=?, Sujet=?, Description=?, idAdresse=?, AnneeScolaire=?  where NumEven=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setDate(1, Date.valueOf(objectToUpdateInDB.getDateEven()));
            ps.setString(2, objectToUpdateInDB.getSujet());
            ps.setString(3, objectToUpdateInDB.getDescription());
            ps.setInt(4, objectToUpdateInDB.getAdrs().getIdAdresse());
            ps.setInt(5, objectToUpdateInDB.getPlan().getAnneeScolaire());
            ps.setInt(6, objectToUpdateInDB.getNumEven());

            this.updateParticipant(objectToUpdateInDB);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Evenement objectToDeleteFromDB) {
        try {
            //delete = "delete from Evenement where NumEven = ?";
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getNumEven());

            this.deleteParticipant(objectToDeleteFromDB);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Evenement> getList() {
        //"select * from Evenement";
        ArrayList<Evenement> events = new ArrayList<Evenement>();

        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                Evenement pojoEvent = new Evenement();
                pojoEvent.setNumEven(rs.getInt("NumEven"));
                pojoEvent.setDateEven(rs.getDate("DateEven").toLocalDate());
                pojoEvent.setSujet(rs.getString("Sujet"));
                pojoEvent.setDescription(rs.getString("Description"));

                //Ajout Adresse
                DAO_Adresse daoAdr = new DAO_Adresse(connection);

                pojoEvent.setAdrs(daoAdr.find(rs.getInt("idAdresse")));

                //Ajout Planning;
                DAO_Planning daoPlanning = new DAO_Planning(connection);

                pojoEvent.setPlan(daoPlanning.find(rs.getInt("AnneeScolaire")));
                pojoEvent.setListContact(getListeContact(rs.getInt("NumEven")));

                //Ajout participant
                pojoEvent.setListContact(this.getAllParticipant(rs.getInt("NumEven")));

                events.add(pojoEvent);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return events;
    }

    private List<Contact> getListeContact(int NumEven) {
        List<Contact> liste = new ArrayList<Contact>();
        String getParticipant = "select c.prenom, c.nom,e.nom from participants p ,contact c,entreprise e where p.NumEven=? and p.idContact=c.idContact and e.idEntreprise=c.idEntreprise";
        try {
            PreparedStatement ps = this.connection.prepareStatement(getParticipant);
            ps.setInt(1, NumEven);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Contact pojoContact = new Contact();
                pojoContact.setPrenom(rs.getString(1));
                pojoContact.setNom(rs.getString(2));
                Entreprise e = new Entreprise();
                e.setNom(rs.getString(3));
                pojoContact.setEntrep(e);
                liste.add(pojoContact);

            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return liste;
    }

    @Override
    public List<Evenement> getListByMotcle(String mc) {
        //"select * from Evenement where Sujet like ?";
        List<Evenement> events = new ArrayList<Evenement>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ps.setString(1, "%" + mc + "%");

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                Evenement pojoEvent = new Evenement();
                pojoEvent.setNumEven(rs.getInt("NumEven"));
                pojoEvent.setDateEven(rs.getDate("DateEven").toLocalDate());
                pojoEvent.setSujet(rs.getString("Sujet"));
                pojoEvent.setDescription(rs.getString("Description"));

                //Ajout Adresse
                DAO_Adresse daoAdr = new DAO_Adresse(connection);

                pojoEvent.setAdrs(daoAdr.find(rs.getInt("idAdresse")));

                //Ajout Planning;
                DAO_Planning daoPlanning = new DAO_Planning(connection);

                pojoEvent.setPlan(daoPlanning.find(rs.getInt("AnneeScolaire")));

                //Ajout participant
                pojoEvent.setListContact(this.getAllParticipant(rs.getInt("NumEven")));

                events.add(pojoEvent);

            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return events;
    }

    //METHODE LIER AUX PARTICIPANTS D'UN EVENEMENT

    public List<Contact> getAllParticipant(int idEvent) {
        String query = "select * from contact where idContact in (select idContact from participants where NumEven = ? )";
        List<Contact> participants = new ArrayList<Contact>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idEvent);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Contact pojoContact = new Contact();
                pojoContact.setIdContact(rs.getInt("idContact"));
                pojoContact.setNom(rs.getString("nom"));
                pojoContact.setPrenom(rs.getString("prenom"));
                pojoContact.setTelephone(rs.getString("telephone"));
                pojoContact.setAdresseMail(rs.getString("AdresseMail"));
                pojoContact.setPoste(rs.getString("Poste"));

                //Ajout Entreprise
                DAO_Entreprise daoEntrep = new DAO_Entreprise(connection);
                pojoContact.setEntrep(daoEntrep.find(rs.getInt("idEntreprise")));

                participants.add(pojoContact);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return participants;
    }

    private void deleteParticipant(Evenement objectDelete) {
        String query = "delete from participants where NumEven =?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, objectDelete.getNumEven());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void lierParticipant(Evenement objectTolink) {
        String query = "insert into participants(NumEven, idContact) values(?,?)";
        List<Contact> listParticipantTobd = this.getAllParticipant(objectTolink.getNumEven());
        if (objectTolink.getListContact() != null) {
            for (Contact contact : objectTolink.getListContact()) {
                boolean ok = true;
                for (int i = 0; i < listParticipantTobd.size() - 1; i++)
                    if (contact.toString().equals(listParticipantTobd.get(i).toString())) ok = false;
                if (ok) {
                    try {

                        PreparedStatement ps = this.connection.prepareStatement(query);
                        ps.setInt(1, objectTolink.getNumEven());
                        ps.setInt(2, contact.getIdContact());

                        ps.executeUpdate();
                        ps.close();

                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    private void updateParticipant(Evenement objectToUpdate) {
        this.deleteParticipant(objectToUpdate);
        this.lierParticipant(objectToUpdate);
    }
}
