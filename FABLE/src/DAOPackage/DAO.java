package DAOPackage;

import java.sql.Connection;
import java.util.List;

public abstract class DAO<T> {

    protected Connection connection = null;

    public DAO(Connection connect) {
        this.connection = connect;
    }

    public abstract T find(int Id);

    public abstract void create(T objectToInsertInDB);

    public abstract void update(T objectToUpdateInDB);

    public abstract void delete(T objectToDeleteFromDB);

    public abstract List<T> getList();

    public abstract List<T> getListByMotcle(String mc);

}