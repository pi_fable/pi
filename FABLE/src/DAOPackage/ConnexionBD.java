package DAOPackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import POJO.Eleve;
import POJO.MaitreDeStage;
import POJO.Professeur;

public class ConnexionBD {
    private static Connection connectionBD = null;
    private String url = "jdbc:mysql://localhost:3306/pi_fable";
    private String username = "root";
    private String password = "";
    private PreparedStatement preparedStatement;
    private ResultSet resultset;

    public ConnexionBD() {

        try {

            Class.forName("com.mysql.jdbc.Driver");

            connectionBD = DriverManager.getConnection(url, username, password);

        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static Connection getInstance() {
        if (connectionBD == null) {

            new ConnexionBD();
        }
        return connectionBD;
    }

    public Connection getConnexion() {
        return connectionBD;
    }

    public Professeur ConnexionProfesseur(String email, String password) throws SQLException {
        preparedStatement = connectionBD.prepareStatement("SELECT * FROM professeur where email = ? and motDePasse = ?");
        preparedStatement.setString(1, email);
        preparedStatement.setString(2, password);
        resultset = preparedStatement.executeQuery();
        if (resultset.next()) {
            return new Professeur(resultset);
        }
        return null;

    }

    public Eleve ConnexionEleve(String email, String password) throws SQLException {
        preparedStatement = connectionBD.prepareStatement("SELECT * FROM eleve e,adresse a where email = ? and motDePasse = ? and e.idAdresse= a.idAdresse");
        preparedStatement.setString(1, email);
        preparedStatement.setString(2, password);
        resultset = preparedStatement.executeQuery();
        if (resultset.next()) {
            return new Eleve(resultset);
        }
        return null;

    }

    public MaitreDeStage ConnexionMaitreDeStage(String email, String password) throws SQLException {
        preparedStatement = connectionBD.prepareStatement("SELECT * FROM maitredestage m,contact c where c.AdresseMail = ? and m.motDePasse = ? and m.idContact=c.idContact");
        preparedStatement.setString(1, email);
        preparedStatement.setString(2, password);
        resultset = preparedStatement.executeQuery();
        if (resultset.next()) {
            return new MaitreDeStage(resultset);
        }
        return null;
    }

    public void close() throws SQLException {
        resultset.close();
        preparedStatement.close();
        connectionBD.close();
    }
}
