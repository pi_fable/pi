package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Adresse;

public class DAO_Adresse extends DAO<Adresse> {
    private String find_by_id = "select * from adresse where idAdresse=?";
    private String create = "insert into adresse(rue, NumRue, CodePostal, Ville, Pays) values(?,?,?,?,?)";
    private String update = "update adresse set rue=?, NumRue=?, CodePostal=?, Ville=?, Pays=?  where idAdresse=?";
    private String delete = "delete from adresse where idAdresse = ?";
    private String find_all = "select * from adresse";
    private String find_by_mc = "select * from adresse where Ville like ?";

    public DAO_Adresse(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Adresse find(int Id) {
        //find_by_id = "select * from adresse where idAdresse=?";

        Adresse pojoAdr = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoAdr = new Adresse();
                pojoAdr.setIdAdresse(rs.getInt("idAdresse"));
                pojoAdr.setRue(rs.getString("rue"));
                pojoAdr.setNumRue(rs.getInt("NumRue"));
                pojoAdr.setCodePostal(rs.getInt("CodePostal"));
                pojoAdr.setVille(rs.getString("Ville"));
                pojoAdr.setPays(rs.getString("Pays"));
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoAdr == null) throw new RuntimeException("Adresse " + Id + " introuvable");
        return pojoAdr;
    }

    @Override
    public void create(Adresse objectToInsertInDB) {
        //"insert into Adresse(rue, NumRue, CodePostal, Ville, Pays) values(?,?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setString(1, objectToInsertInDB.getRue());
            ps.setInt(2, objectToInsertInDB.getNumRue());
            ps.setInt(3, objectToInsertInDB.getCodePostal());
            ps.setString(4, objectToInsertInDB.getVille());
            ps.setString(5, objectToInsertInDB.getPays());

            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void update(Adresse objectToUpdateInDB) {
        //"update Adresse set rue=?, NumRue=?, CodePostal=?, Ville=?, Pays=?  where idAdresse=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setString(1, objectToUpdateInDB.getRue());
            ps.setInt(2, objectToUpdateInDB.getNumRue());
            ps.setInt(3, objectToUpdateInDB.getCodePostal());
            ps.setString(4, objectToUpdateInDB.getVille());
            ps.setString(5, objectToUpdateInDB.getPays());
            ps.setInt(6, objectToUpdateInDB.getIdAdresse());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Adresse objectToDeleteFromDB) {
        try {
            //delete = "delete from Adresse where idAdresse = ?";
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdAdresse());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Adresse> getList() {
        //find_all = "select * from Adresse";
        List<Adresse> adrs = new ArrayList<Adresse>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Adresse adr = new Adresse();
                adr.setIdAdresse(rs.getInt("idAdresse"));
                adr.setRue(rs.getString("rue"));
                adr.setNumRue(rs.getInt("NumRue"));
                adr.setCodePostal(rs.getInt("codePostal"));
                adr.setVille(rs.getString("Ville"));
                adr.setPays(rs.getString("Pays"));

                adrs.add(adr);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return adrs;
    }

    @Override
    public List<Adresse> getListByMotcle(String mc) {
        //find_by_mc = "select * from Adresse where Ville like ?";
        List<Adresse> adrs = new ArrayList<Adresse>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ps.setString(1, "%" + mc + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Adresse adr = new Adresse();
                adr.setIdAdresse(rs.getInt("idAdresse"));
                adr.setRue(rs.getString("rue"));
                adr.setNumRue(rs.getInt("NumRue"));
                adr.setCodePostal(rs.getInt("codePostal"));
                adr.setVille(rs.getString("Ville"));
                adr.setPays(rs.getString("Pays"));

                adrs.add(adr);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return adrs;
    }
}
