package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import POJO.Eleve;
import POJO.Feedback;
import POJO.PropositionDeStage;
import POJO.Technologie;

public class DAO_Eleve extends DAO<Eleve> {
	
	private String find_by_id = "select * from eleve where idEleve=?";
	private String create = "insert into eleve(Email, nom, prenom, motDePasse, isStudent, idStage, idAdresse, lienPhoto) values(?,?,?,?,?,?,?,?)";
	private String update = "update eleve set Email=?, nom=?, prenom=?, motDePasse=?, isStudent=?, idStage=?, idAdresse=?, lienPhoto=? where idEleve=?";
	private String delete = "delete from eleve where idEleve = ?";
	private String find_all = "select * from eleve";
	private String find_by_mc = "select * from eleve where nom like ? or prenom like ?";
	
	public DAO_Eleve(Connection connect) {
		super(connect);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Eleve find(int Id) {
		//find_by_id = "select * from Eleve where idEleve=?";
		Eleve pojoEleve = null;
		try {
			PreparedStatement ps = this.connection.prepareStatement(find_by_id);
			ps.setInt(1, Id);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				pojoEleve = new Eleve();
				pojoEleve.setIdEleve(rs.getInt("idEleve"));
				pojoEleve.setEmail(rs.getString("Email"));
				pojoEleve.setNom(rs.getString("nom"));
				pojoEleve.setPrenom(rs.getString("prenom"));
				pojoEleve.setMotDePasse(rs.getString("motDePasse"));
				
				if(rs.getString("isStudent").equals("true")){
					pojoEleve.setIsStudent(true);
				}
				else pojoEleve.setIsStudent(false);
				//Ajout Stage
				if(rs.getInt("idStage") != 0){
					DAO_Stage daoStage = new DAO_Stage(connection);				
					pojoEleve.setSonStage(daoStage.find(rs.getInt("idStage")));
				}
				//Ajout Adresse
				DAO_Adresse daoAdresse = new DAO_Adresse(connection);				
				pojoEleve.setSonAdresse(daoAdresse.find(rs.getInt("idAdresse")));
				
				//Ajout lienFichier
				pojoEleve.setLienPhoto(rs.getString("lienPhoto"));
				
				
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(pojoEleve==null) throw new RuntimeException("Eleve "+Id+" introuvable");
		return pojoEleve;
	}

    private String find_by_id = "select * from eleve where idEleve=?";
    private String create = "insert into eleve(Email, nom, prenom, motDePasse, isStudent, idStage, idAdresse, lienPhoto) values(?,?,?,?,?,?,?,?)";
    private String update = "update eleve set Email=?, nom=?, prenom=?, motDePasse=?, isStudent=?, idStage=?, idAdresse=?, lienPhoto=? where idEleve=?";
    private String delete = "delete from eleve where idEleve = ?";
    private String find_all = "select * from eleve";
    private String find_all_no_stage = "select * from eleve where nvl(idStage)=0";
    private String find_by_mc = "select * from eleve where nom like ? or prenom like ?";

    public DAO_Eleve(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Eleve find(int Id) {
        //find_by_id = "select * from Eleve where idEleve=?";
        Eleve pojoEleve = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoEleve = new Eleve();
                pojoEleve.setIdEleve(rs.getInt("idEleve"));
                pojoEleve.setEmail(rs.getString("Email"));
                pojoEleve.setNom(rs.getString("nom"));
                pojoEleve.setPrenom(rs.getString("prenom"));
                pojoEleve.setMotDePasse(rs.getString("motDePasse"));

                if (rs.getString("isStudent").equals("true")) {
                    pojoEleve.setIsStudent(true);
                } else pojoEleve.setIsStudent(false);
                //Ajout Stage
                if (rs.getInt("idStage") != 0) {
                    DAO_Stage daoStage = new DAO_Stage(connection);
                    pojoEleve.setSonStage(daoStage.find(rs.getInt("idStage")));
                }
                //Ajout Adresse
                DAO_Adresse daoAdresse = new DAO_Adresse(connection);
                pojoEleve.setSonAdresse(daoAdresse.find(rs.getInt("idAdresse")));

                //Ajout lienFichier
                pojoEleve.setLienPhoto(rs.getString("lienFichier"));


            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoEleve == null) throw new RuntimeException("Eleve " + Id + " introuvable");
        return pojoEleve;
    }

    @Override
    public void create(Eleve objectToInsertInDB) {
        //create = "insert into eleve(Email, nom, prenom, motDePasse, isStudent, idStage, idAdresse, lienPhoto) values(?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setString(1, objectToInsertInDB.getEmail());
            ps.setString(2, objectToInsertInDB.getNom());
            ps.setString(3, objectToInsertInDB.getPrenom());
            ps.setString(4, objectToInsertInDB.getMotDePasse());
            if (objectToInsertInDB.getIsStudent())
                ps.setString(5, "true");
            else ps.setString(5, "fals");
            if (objectToInsertInDB.getSonStage() != null)
                ps.setInt(6, objectToInsertInDB.getSonStage().getIdStage());
            else ps.setNull(6, Types.INTEGER);

            ps.setInt(7, objectToInsertInDB.getSonAdresse().getIdAdresse());
            ps.setString(8, objectToInsertInDB.getLienPhoto());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(Eleve objectToUpdateInDB) {
        //update = "update eleve set Email=?, nom=?, prenom=?, motDePasse=?, isStudent=?, idStage=?, idAdresse=? where idEleve=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setString(1, objectToUpdateInDB.getEmail());
            ps.setString(2, objectToUpdateInDB.getNom());
            ps.setString(3, objectToUpdateInDB.getPrenom());
            ps.setString(4, objectToUpdateInDB.getMotDePasse());
            if (objectToUpdateInDB.getIsStudent())
                ps.setString(5, "true");
            else ps.setString(5, "fals");

            if (objectToUpdateInDB.getSonStage() != null)
                ps.setInt(6, objectToUpdateInDB.getSonStage().getIdStage());
            else ps.setNull(6, Types.INTEGER);
            ps.setInt(7, objectToUpdateInDB.getSonAdresse().getIdAdresse());

            ps.setString(8, objectToUpdateInDB.getLienPhoto());

            ps.setInt(9, objectToUpdateInDB.getIdEleve());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Eleve objectToDeleteFromDB) {
        //delete = "delete from Eleve where IdEleve = ?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdEleve());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Eleve> getList() {
        //find_all = "select * from Eleve";
        List<Eleve> eleveList = new ArrayList<Eleve>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Eleve pojoEleve = new Eleve();
                pojoEleve.setIdEleve(rs.getInt("idEleve"));
                pojoEleve.setEmail(rs.getString("Email"));
                pojoEleve.setNom(rs.getString("nom"));
                pojoEleve.setPrenom(rs.getString("prenom"));
                pojoEleve.setMotDePasse(rs.getString("motDePasse"));

                if (rs.getString("isStudent").equals("true")) {
                    pojoEleve.setIsStudent(true);
                } else pojoEleve.setIsStudent(false);
                //Ajout Stage
                if (rs.getInt("idStage") != 0) {
                    DAO_Stage daoStage = new DAO_Stage(connection);
                    pojoEleve.setSonStage(daoStage.find(rs.getInt("idStage")));
                }
                //Ajout Adresse
                DAO_Adresse daoAdresse = new DAO_Adresse(connection);
                pojoEleve.setSonAdresse(daoAdresse.find(rs.getInt("idAdresse")));
                //Ajout lienFichier
                pojoEleve.setLienPhoto(rs.getString("lienPhoto"));

                eleveList.add(pojoEleve);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return eleveList;
    }

    @Override
    public List<Eleve> getListByMotcle(String mc) {
        //find_by_mc = "select * from Eleve where nom like ?";
        List<Eleve> eleveList = new ArrayList<Eleve>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Eleve pojoEleve = new Eleve();
                pojoEleve.setIdEleve(rs.getInt("idEleve"));
                pojoEleve.setEmail(rs.getString("Email"));
                pojoEleve.setNom(rs.getString("nom"));
                pojoEleve.setPrenom(rs.getString("prenom"));
                pojoEleve.setMotDePasse(rs.getString("motDePasse"));

                if (rs.getString("isStudent").equals("true")) {
                    pojoEleve.setIsStudent(true);
                } else pojoEleve.setIsStudent(false);
                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoEleve.setSonStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Adresse daoAdresse = new DAO_Adresse(connection);
                pojoEleve.setSonAdresse(daoAdresse.find(rs.getInt("idAdresse")));
                //Ajout lienFichier
                pojoEleve.setLienPhoto(rs.getString("lienFichier"));

                eleveList.add(pojoEleve);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return eleveList;
    }

    public List<Technologie> getAllTechnologie(int idProp) {
        String query = "select * from technologie where idTechnologie in (select idTechnologie from r_prop/tech where idProp = ? )";
        List<Technologie> techs = new ArrayList<Technologie>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idProp);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Technologie pojoTech = new Technologie();
                pojoTech.setIdTechnologie(rs.getInt("idTechnologie"));
                pojoTech.setNom(rs.getString("nom"));

                techs.add(pojoTech);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return techs;
    }

    public List<PropositionDeStage> getAllPropositionDeStageInteresse(int idEleve) {
        String query = "select * from propositionDeStage where idProp in (select idProp from eleveInteresse where idEleve = ?))";
        List<PropositionDeStage> propList = new ArrayList<PropositionDeStage>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idEleve);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PropositionDeStage pojoProp = new PropositionDeStage();
                pojoProp.setIdProp(rs.getInt("idProp"));
                pojoProp.setSujet(rs.getString("sujet"));
                pojoProp.setAnnee(rs.getInt("number"));
                pojoProp.setDescription(rs.getString("description"));
                pojoProp.setTypes(rs.getString("types"));
                pojoProp.setLienFichier(rs.getString("lienFichier"));

                //Ajout Entreprise
                DAO_Entreprise daoEntrep = new DAO_Entreprise(connection);
                pojoProp.setEntrep(daoEntrep.find(rs.getInt("idEntreprise")));

                propList.add(pojoProp);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return propList;

    }

    public List<Feedback> getAllFeedback(int idEleve) {
        String query = "select * from feedback where numFeedback in (select numFeedback from feedback where idEleve = ?))";
        List<Feedback> feedbackList = new ArrayList<Feedback>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idEleve);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Feedback pojoFeedback = new Feedback();
                pojoFeedback.setNumFeedback(rs.getInt("numFeedback"));
                pojoFeedback.setDateRemise(rs.getDate("dateRemise"));
                pojoFeedback.setMessage(rs.getString("message"));
                pojoFeedback.setLienFichier(rs.getString("lienFichier"));

                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoFeedback.setConcerneStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Eleve daoEleve = new DAO_Eleve(connection);
                pojoFeedback.setConserneEleve(daoEleve.find(rs.getInt("idEleve")));

                feedbackList.add(pojoFeedback);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return feedbackList;

    }

    public List<Eleve> getListWithoutStage() {
        //find_all = "select * from Eleve";
        List<Eleve> eleveList = new ArrayList<Eleve>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all_no_stage);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Eleve pojoEleve = new Eleve();
                pojoEleve.setIdEleve(rs.getInt("idEleve"));
                pojoEleve.setEmail(rs.getString("Email"));
                pojoEleve.setNom(rs.getString("nom"));
                pojoEleve.setPrenom(rs.getString("prenom"));
                pojoEleve.setMotDePasse(rs.getString("motDePasse"));

                if (rs.getString("isStudent").equals("true")) {
                    pojoEleve.setIsStudent(true);
                } else pojoEleve.setIsStudent(false);
                //Ajout Stage

                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoEleve.setSonStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Adresse daoAdresse = new DAO_Adresse(connection);
                pojoEleve.setSonAdresse(daoAdresse.find(rs.getInt("idAdresse")));
                //Ajout lienFichier
                pojoEleve.setLienPhoto(rs.getString("lienPhoto"));

                eleveList.add(pojoEleve);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return eleveList;
    }
}