package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Entreprise;
import POJO.Technologie;

public class DAO_Technologie extends DAO<Technologie> {
    private String find_by_id = "select * from technologie where idTech=?";
    private String create = "insert into technologie(nomTech) values(?)";
    private String update = "update technologie set nomTech=? where idTech=?";
    private String delete = "delete from technologie where idTech = ?";
    private String find_all = "select * from technologie";
    private String find_by_mc = "select * from technologie where nomTech like ?";

    public DAO_Technologie(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Technologie objectToInsertInDB) {
		//"insert into Technologie(nom) values(?)";
		try {
			PreparedStatement ps = this.connection.prepareStatement(create);
			ps.setString(1, objectToInsertInDB.getNom());
			
			ps.executeUpdate();
			ps.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getLastIdCreate(){
		int id=0;
		try {
			PreparedStatement ps=this.connection.prepareStatement("");
			ResultSet r=ps.executeQuery("SELECT LAST_INSERT_ID()");
			r.next();
			id=r.getInt(1);
			ps.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	@Override
	public void update(Technologie objectToUpdateInDB) {
		//update = "update Technologie set nom=? where idTech=?";
		try {
			PreparedStatement ps = this.connection.prepareStatement(update);
			ps.setString(1, objectToUpdateInDB.getNom());
			ps.setInt(2, objectToUpdateInDB.getIdTechnologie());
			
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    @Override
    public void create(Technologie objectToInsertInDB) {
        //"insert into Technologie(nom) values(?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setString(1, objectToInsertInDB.getNom());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void update(Technologie objectToUpdateInDB) {
        //update = "update Technologie set nom=? where idTech=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setString(1, objectToUpdateInDB.getNom());
            ps.setInt(2, objectToUpdateInDB.getIdTechnologie());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Technologie objectToDeleteFromDB) {
        try {
            //delete = "delete from Technologie where idTech = ?";
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdTechnologie());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Technologie> getList() {
        //find_all = "select * from Technologie";
        List<Technologie> techs = new ArrayList<Technologie>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Technologie pojoTech = new Technologie();
                pojoTech.setIdTechnologie(rs.getInt("idTech"));
                pojoTech.setNom(rs.getString("nomTech"));
                techs.add(pojoTech);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // TODO Auto-generated method stub
        return techs;
    }

    @Override
    public List<Technologie> getListByMotcle(String mc) {
        //find_by_mc = "select * from Technologie where nom like ?";
        List<Technologie> techs = new ArrayList<Technologie>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ps.setString(1, "%" + mc + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Technologie pojoTech = new Technologie();
                pojoTech.setIdTechnologie(rs.getInt("idTech"));
                pojoTech.setNom(rs.getString("nomTech"));
                techs.add(pojoTech);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return techs;
    }

    public List<Entreprise> getAllEntreprise(int idTech) {
        String query = "select * from entreprise where idEntreprise in (select idEntreprise from R_Entr/Techno where idTech = ? )";
        List<Entreprise> entreps = new ArrayList<Entreprise>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idTech);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Entreprise pojoEntrep = new Entreprise();
                pojoEntrep.setIdEntreprise(rs.getInt("idEntreprise"));
                pojoEntrep.setURLSite(rs.getString("URLSite"));
                pojoEntrep.setNom(rs.getString("nom"));

                //Ajout Adresse
                DAO_Adresse daoAdr = new DAO_Adresse(connection);

                pojoEntrep.setAdrs(daoAdr.find(rs.getInt("idAdresse")));

                entreps.add(pojoEntrep);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return entreps;
    }
}
