package DAOPackage;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Eleve;
import POJO.Feedback;
import POJO.Stage;

public class DAO_Stage extends DAO<Stage> {

    private String find_by_id = "select * from stage where idStage=?";
    private String create = "insert into stage(DateDebut, DateFin, Repertorier, idProp) values(?,?,?,?)";
    private String update = "update stage set DateDebut=?, DateFin=?, Repertorier=?, idProp=? where idStage=?";
    private String delete = "delete from stage where idStage = ?";
    private String find_all = "select * from stage";
    private String find_by_mc = "select * from stage where sujet like ?";

    public DAO_Stage(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Stage find(int Id) {
        //find_by_id = "select * from Stage where idStage=?";
        Stage pojoStage = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoStage = new Stage();
                pojoStage.setDateDebut(rs.getDate("DateDebut").toLocalDate());
                pojoStage.setDateFin(rs.getDate("DateFin").toLocalDate());
                pojoStage.setRepertorier(rs.getString("Repertorier").equals("true"));

                //Ajout Proposition
                DAO_PropositionDeStage daoPropStage = new DAO_PropositionDeStage(connection);
                pojoStage.setPropStage(daoPropStage.find(rs.getInt("idProp")));
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoStage == null) throw new RuntimeException("Stage " + Id + " introuvable");
        return pojoStage;
    }

    @Override
    public void create(Stage objectToInsertInDB) {
        //create = "insert into Stage(DateDebut, DateFin, Repertorier, idProp) values(?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setDate(1, Date.valueOf(objectToInsertInDB.getDateDebut()));
            ps.setDate(2, Date.valueOf(objectToInsertInDB.getDateFin()));
            if (objectToInsertInDB.getRepertorier())
                ps.setString(3, "true");
            else ps.setString(3, "fals");
            ps.setInt(4, objectToInsertInDB.getPropStage().getIdProp());
            ps.executeUpdate();
            ResultSet r = ps.executeQuery("SELECT LAST_INSERT_ID()");
            r.next();
            int id = r.getInt(1);
            objectToInsertInDB.setIdStage(id);
            ps.close();
            Eleve e = objectToInsertInDB.getEleveStage();
            e.setSonStage(objectToInsertInDB);
            DAO<Eleve> dao = new DAO_Eleve(connection);
            dao.update(e);

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(Stage objectToUpdateInDB) {
        //update = "update Stage set DateDebut=?, DateFin=?, Repertorier=?, idProp=? where idStage=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setDate(1, Date.valueOf(objectToUpdateInDB.getDateDebut()));
            ps.setDate(2, Date.valueOf(objectToUpdateInDB.getDateFin()));
            if (objectToUpdateInDB.getRepertorier())
                ps.setString(3, "true");
            else ps.setString(3, "fals");
            ps.setInt(4, objectToUpdateInDB.getPropStage().getIdProp());

            ps.setInt(5, objectToUpdateInDB.getIdStage());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Stage objectToDeleteFromDB) {
        //delete = "delete from Stage where idStage = ?";
        try {
            //delete = "delete from Entreprise where idEntreprise = ?";
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdStage());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Stage> getList() {
        //find_all = "select * from Stage";
        List<Stage> stageLisr = new ArrayList<Stage>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Stage pojoStage = new Stage();
                pojoStage.setDateDebut(rs.getDate("DateDebut").toLocalDate());
                pojoStage.setDateFin(rs.getDate("DateFin").toLocalDate());
                pojoStage.setRepertorier(rs.getString("Repertorier").equals("true"));

                //Ajout Proposition
                DAO_PropositionDeStage daoPropStage = new DAO_PropositionDeStage(connection);
                pojoStage.setPropStage(daoPropStage.find(rs.getInt("idProp")));

                stageLisr.add(pojoStage);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return stageLisr;
    }

    @Override
    public List<Stage> getListByMotcle(String mc) {
        //find_by_mc = "select * from Stage where sujet like ?";
        List<Stage> stageLisr = new ArrayList<Stage>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Stage pojoStage = new Stage();
                pojoStage.setDateDebut(rs.getDate("DateDebut").toLocalDate());
                pojoStage.setDateFin(rs.getDate("DateFin").toLocalDate());
                pojoStage.setRepertorier(rs.getString("Repertorier").equals("true"));

                //Ajout Proposition
                DAO_PropositionDeStage daoPropStage = new DAO_PropositionDeStage(connection);
                pojoStage.setPropStage(daoPropStage.find(rs.getInt("idProp")));

                stageLisr.add(pojoStage);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return stageLisr;
    }

    public Eleve getEleveStage(int idStage) {
        String query = "select * from Eleve where idStage =?";
        Eleve pojoEleve = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idStage);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoEleve = new Eleve();
                pojoEleve.setIdEleve(rs.getInt("idEleve"));
                pojoEleve.setEmail(rs.getString("Email"));
                pojoEleve.setNom(rs.getString("nom"));
                pojoEleve.setPrenom(rs.getString("prenom"));
                pojoEleve.setMotDePasse(rs.getString("motDePasse"));

                if (rs.getString("isStudent").equals("true")) {
                    pojoEleve.setIsStudent(true);
                } else pojoEleve.setIsStudent(false);
                //Ajout Stage
                pojoEleve.setSonStage(this.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Adresse daoAdresse = new DAO_Adresse(connection);
                pojoEleve.setSonAdresse(daoAdresse.find(rs.getInt("idAdresse")));

            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoEleve == null) throw new RuntimeException("probleme dans la BD getEleveStage pour stage : " + idStage);
        return pojoEleve;
    }

    public List<Eleve> getAllEleveInteresse(int idStage) {
        String query = "select * from Eleve where idEleve in (select idEleve from EleveInteresse in (select idProp from Stage where idStage = ?))";
        List<Eleve> eleveList = new ArrayList<Eleve>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idStage);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Eleve pojoEleve = new Eleve();
                pojoEleve.setIdEleve(rs.getInt("idEleve"));
                pojoEleve.setEmail(rs.getString("Email"));
                pojoEleve.setNom(rs.getString("nom"));
                pojoEleve.setPrenom(rs.getString("prenom"));
                pojoEleve.setMotDePasse(rs.getString("motDePasse"));

                if (rs.getString("isStudent").equals("true")) {
                    pojoEleve.setIsStudent(true);
                } else pojoEleve.setIsStudent(false);
                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoEleve.setSonStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Adresse daoAdresse = new DAO_Adresse(connection);
                pojoEleve.setSonAdresse(daoAdresse.find(rs.getInt("idAdresse")));

                eleveList.add(pojoEleve);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return eleveList;

    }

    public List<Feedback> getAllFeedback(int idStage) {
        String query = "select * from Feedback where numFeedback in (select numFeedback from Feedback where idStage = ?))";
        List<Feedback> feedbackList = new ArrayList<Feedback>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idStage);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Feedback pojoFeedback = new Feedback();
                pojoFeedback.setNumFeedback(rs.getInt("numFeedback"));
                pojoFeedback.setDateRemise(rs.getDate("dateRemise"));
                pojoFeedback.setMessage(rs.getString("message"));
                pojoFeedback.setLienFichier(rs.getString("lienFichier"));

                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoFeedback.setConcerneStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Eleve daoEleve = new DAO_Eleve(connection);
                pojoFeedback.setConserneEleve(daoEleve.find(rs.getInt("idEleve")));

                feedbackList.add(pojoFeedback);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return feedbackList;

    }


}
