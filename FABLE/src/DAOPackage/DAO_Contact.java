package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Contact;
import POJO.Evenement;

public class DAO_Contact extends DAO<Contact> {

    private String find_by_id = "select * from contact where idContact=?";
    private String create = "insert into contact(nom, prenom, telephone, AdresseMail, Poste, idEntreprise ) values(?,?,?,?,?,?)";
    private String update = "update contact set nom=?, prenom=?, telephone=?, AdresseMail=?, Poste=?, idEntreprise=? where idContact=?";
    private String delete = "delete from contact where idContact = ?";
    private String find_all = "select * from contact";
    private String find_by_mc = "select * from contact where nom like ?";

    public DAO_Contact(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Contact find(int Id) {
        //find_by_id = "select * from Contact where idContact=?";
        Contact pojoContact = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoContact = new Contact();
                pojoContact.setIdContact(rs.getInt("idContact"));
                pojoContact.setNom(rs.getString("nom"));
                pojoContact.setPrenom(rs.getString("prenom"));
                pojoContact.setTelephone(rs.getString("telephone"));
                pojoContact.setAdresseMail(rs.getString("AdresseMail"));
                pojoContact.setPoste("Poste");

                //Ajout Entreprise
                System.out.println(rs.getInt("idEntreprise"));
                /*
				if(rs.getInt("idEntreprise") != null){////????????????????????????????????????????????
					DAO_Entreprise daoEntreprise = new DAO_Entreprise(connection);				
					pojoContact.setEntrep(daoEntreprise.find(rs.getInt("idEntreprise")));
				}
				*/

                //Ajout evenement
                pojoContact.setListEvent(this.getAllEvenement(Id));

            }
            ps.close();
        } catch (NullPointerException e) {
            throw new RuntimeException("Valeur null ");
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoContact == null) throw new RuntimeException("Contact " + Id + " introuvable");
        return pojoContact;
    }

    @Override
    public void create(Contact objectToInsertInDB) {
        //create = "insert into Contact(nom, prenom, telephone, AdresseMail, Poste, idEntreprise ) values(?,?,?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setString(1, objectToInsertInDB.getNom());
            ps.setString(2, objectToInsertInDB.getPrenom());
            ps.setString(3, objectToInsertInDB.getTelephone());
            ps.setString(4, objectToInsertInDB.getAdresseMail());
            ps.setString(5, objectToInsertInDB.getPoste());
            ps.setInt(6, objectToInsertInDB.getEntrep().getIdEntreprise());

            ps.executeUpdate();
            ps.close();
            ResultSet r = ps.executeQuery("SELECT LAST_INSERT_ID()");
            r.next();
            int id = r.getInt(1);
            objectToInsertInDB.setIdContact(id);
            this.lierParticipant(objectToInsertInDB);

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NullPointerException e) {
            throw new RuntimeException("Valeur null ");
        }
    }


    @Override
    public void update(Contact objectToUpdateInDB) {
        //update = "update Contact set nom=?, prenom=?, telephone=?, AdresseMail=?, Poste=?, idEntreprise=? where idContact=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setString(1, objectToUpdateInDB.getNom());
            ps.setString(2, objectToUpdateInDB.getPrenom());
            ps.setString(3, objectToUpdateInDB.getTelephone());
            ps.setString(4, objectToUpdateInDB.getAdresseMail());
            ps.setString(5, objectToUpdateInDB.getPoste());
            ps.setInt(6, objectToUpdateInDB.getEntrep().getIdEntreprise());

            ps.setInt(7, objectToUpdateInDB.getIdContact());

            this.updateParticipant(objectToUpdateInDB);

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Contact objectToDeleteFromDB) {
        //delete = "delete from Contact where idContact = ?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdContact());

            this.deleteEvenement(objectToDeleteFromDB);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Contact> getList() {
        //find_all = "select * from Contact";
        List<Contact> contactList = new ArrayList<Contact>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Contact pojoContact = new Contact();
                pojoContact.setIdContact(rs.getInt("idContact"));
                pojoContact.setNom(rs.getString("nom"));
                pojoContact.setPrenom(rs.getString("prenom"));
                pojoContact.setTelephone(rs.getString("telephone"));
                pojoContact.setAdresseMail(rs.getString("AdresseMail"));
                pojoContact.setPoste("Poste");

                //Ajout Entreprise
                DAO_Entreprise daoEntreprise = new DAO_Entreprise(connection);
                pojoContact.setEntrep(daoEntreprise.find(rs.getInt("idEntreprise")));

                //Ajout participant
                pojoContact.setListEvent(this.getAllEvenement(rs.getInt("idContact")));

                contactList.add(pojoContact);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return contactList;
    }

    @Override
    public List<Contact> getListByMotcle(String mc) {
        //find_by_mc = "select * from Contact where nom like ?";
        List<Contact> contactList = new ArrayList<Contact>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Contact pojoContact = new Contact();
                pojoContact.setIdContact(rs.getInt("idContact"));
                pojoContact.setNom(rs.getString("nom"));
                pojoContact.setPrenom(rs.getString("prenom"));
                pojoContact.setTelephone(rs.getString("telephone"));
                pojoContact.setAdresseMail(rs.getString("AdresseMail"));
                pojoContact.setPoste("Poste");

                //Ajout Entreprise
                DAO_Entreprise daoEntreprise = new DAO_Entreprise(connection);
                pojoContact.setEntrep(daoEntreprise.find(rs.getInt("idEntreprise")));

                //Ajout participant
                pojoContact.setListEvent(this.getAllEvenement(rs.getInt("idContact")));

                contactList.add(pojoContact);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return contactList;
    }

    //METHODE LIER AUX EVENEMENTS AUQUEL PARTICIPE UN CONTACT

    public List<Evenement> getAllEvenement(int idContact) {
        String query = "select * from evenement where NumEven in (select NumEven from participants where idContact = ? )";
        ArrayList<Evenement> events = new ArrayList<Evenement>();

        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idContact);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Evenement pojoEvent = new Evenement();
                pojoEvent.setNumEven(rs.getInt("NumEven"));
                pojoEvent.setDateEven(rs.getDate("DateEven").toLocalDate());
                pojoEvent.setSujet(rs.getString("Sujet"));
                pojoEvent.setDescription(rs.getString("Description"));

                //Ajout Adresse
                DAO_Adresse daoAdr = new DAO_Adresse(connection);

                pojoEvent.setAdrs(daoAdr.find(rs.getInt("idAdresse")));

                //Ajout Planning;
                DAO_Planning daoPlanning = new DAO_Planning(connection);

                pojoEvent.setPlan(daoPlanning.find(rs.getInt("AnneeScolaire")));

                //Ajout participant
                DAO_Evenement daoEven = new DAO_Evenement(connection);
                pojoEvent.setListContact(daoEven.getAllParticipant(rs.getInt("NumEven")));

                events.add(pojoEvent);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return events;
    }

    private void deleteEvenement(Contact objectDelete) {
        String query = "delete from participants where NumEven =? and idContact=?";
        for (Evenement Even : objectDelete.getListEvent()) {
            try {
                PreparedStatement ps = this.connection.prepareStatement(query);
                ps.setInt(1, Even.getNumEven());
                ps.setInt(2, objectDelete.getIdContact());

                ps.executeUpdate();
                ps.close();

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void lierParticipant(Contact objectTolink) {
        String query = "insert into participants(NumEven, idContact) values(?,?)";
        List<Evenement> listEvenementTobd = this.getAllEvenement(objectTolink.getIdContact());
        if (objectTolink.getListEvent() != null) {
            for (Evenement even : objectTolink.getListEvent()) {
                boolean ok = true;
                for (int i = 0; i < listEvenementTobd.size() - 1; i++)
                    if (even.toString().equals(listEvenementTobd.get(i).toString())) ok = false;
                if (ok) {
                    try {

                        PreparedStatement ps = this.connection.prepareStatement(query);
                        ps.setInt(1, even.getNumEven());
                        ps.setInt(2, objectTolink.getIdContact());


                        ps.executeUpdate();
                        ps.close();

                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    private void updateParticipant(Contact objectToUpdate) {
        this.deleteEvenement(objectToUpdate);
        this.lierParticipant(objectToUpdate);
    }

}
