package DAOPackage;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Formulaire;

public class DAO_Formulaire extends DAO<Formulaire> {

    private String find_by_id = "select * from formulaire where idForm=?";
    private String create = "insert into formulaire(titre, periode, dateLimite) values(?,?,?)";
    private String update = "update formulaire set titre=?, periode=?, dateLimite=? where idForm=?";
    private String delete = "delete from formulaire where idForm = ?";
    private String find_all = "select * from formulaire";
    private String find_by_mc = "select * from formulaire where titre like ?";

    public DAO_Formulaire(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Formulaire find(int Id) {
        //find_by_id = "select * from Formulaire where idForm=?";
        Formulaire pojoFormulaire = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoFormulaire = new Formulaire();
                pojoFormulaire.setIdForm(rs.getInt("idForm"));
                pojoFormulaire.setTitre(rs.getString("titre"));
                pojoFormulaire.setPeriode(rs.getString("periode"));
                pojoFormulaire.setDateLimite(rs.getDate("dateLimite").toLocalDate());

            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoFormulaire == null) throw new RuntimeException("Formulaire " + Id + " introuvable");
        return pojoFormulaire;
    }

    @Override
    public void create(Formulaire objectToInsertInDB) {
        //create = "insert into Formulaire(titre, periode, dateLimite) values(?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setString(1, objectToInsertInDB.getTitre());
            ps.setString(2, objectToInsertInDB.getPeriode());
            ps.setDate(3, Date.valueOf(objectToInsertInDB.getDateLimite()));

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(Formulaire objectToUpdateInDB) {
        //update = "update Formulaire set titre=?, periode=?, dateLimite=? where idForm=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setString(1, objectToUpdateInDB.getTitre());
            ps.setString(2, objectToUpdateInDB.getPeriode());
            ps.setDate(3, Date.valueOf(objectToUpdateInDB.getDateLimite()));

            ps.setInt(4, objectToUpdateInDB.getIdForm());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Formulaire objectToDeleteFromDB) {
        //delete = "delete from Formulaire where idForm = ?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdForm());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Formulaire> getList() {
        //find_by_mc = "select * from Formulaire";
        List<Formulaire> formulaireList = new ArrayList<Formulaire>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Formulaire pojoFormulaire = new Formulaire();
                pojoFormulaire.setIdForm(rs.getInt("idForm"));
                pojoFormulaire.setTitre(rs.getString("titre"));
                pojoFormulaire.setPeriode(rs.getString("periode"));
                pojoFormulaire.setDateLimite(rs.getDate("dateLimite").toLocalDate());

                formulaireList.add(pojoFormulaire);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return formulaireList;
    }

    @Override
    public List<Formulaire> getListByMotcle(String mc) {
        //find_by_mc = "select * from Formulaire where titre like ?";
        List<Formulaire> formulaireList = new ArrayList<Formulaire>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ps.setString(1, "%" + mc + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Formulaire pojoFormulaire = new Formulaire();
                pojoFormulaire.setIdForm(rs.getInt("idForm"));
                pojoFormulaire.setTitre(rs.getString("titre"));
                pojoFormulaire.setPeriode(rs.getString("periode"));
                pojoFormulaire.setDateLimite(rs.getDate("dateLimite").toLocalDate());

                formulaireList.add(pojoFormulaire);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return formulaireList;
    }

}