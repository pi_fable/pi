package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Feedback;

public class DAO_Feedback extends DAO<Feedback> {

    private String find_by_id = "select * from feedback where numFeedback=?";
    private String create = "insert into feedback(dateRemise, message, lienFichier, idStage, idEleve) values(?,?,?,?,?)";
    private String update = "update feedback set dateRemise=?, message=?, lienFichier=?, idStage=?, idEleve=? where numFeedback=?";
    private String delete = "delete from feedback where numFeedback = ?";
    private String find_all = "select * from feedback";
    private String find_by_mc = "select * from feedback where dateRemise like ?"; //ATTENTION

    public DAO_Feedback(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Feedback find(int Id) {
        //find_by_id = "select * from Feedback where numFeedback=?";
        Feedback pojoFeedback = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoFeedback = new Feedback();
                pojoFeedback.setNumFeedback(rs.getInt("numFeedback"));
                pojoFeedback.setDateRemise(rs.getDate("dateRemise"));
                pojoFeedback.setMessage(rs.getString("message"));
                pojoFeedback.setLienFichier(rs.getString("lienFichier"));

                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoFeedback.setConcerneStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Eleve daoEleve = new DAO_Eleve(connection);
                pojoFeedback.setConserneEleve(daoEleve.find(rs.getInt("idEleve")));

            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoFeedback == null) throw new RuntimeException("Feedback " + Id + " introuvable");
        return pojoFeedback;
    }

    @Override
    public void create(Feedback objectToInsertInDB) {
        //create = "insert into Feedback(dateRemise, message, lienFichier, idStage, idEleve) values(?,?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setDate(1, objectToInsertInDB.getDateRemise());
            ps.setString(2, objectToInsertInDB.getMessage());
            ps.setString(3, objectToInsertInDB.getLienFichier());
            ps.setInt(4, objectToInsertInDB.getConcerneStage().getIdStage());
            ps.setInt(5, objectToInsertInDB.getConserneEleve().getIdEleve());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(Feedback objectToUpdateInDB) {
        //update = "update Feedback set dateRemise=?, message=?, lienFichier=?, idStage=?, idEleve=? where numFeedback=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setDate(1, objectToUpdateInDB.getDateRemise());
            ps.setString(2, objectToUpdateInDB.getMessage());
            ps.setString(3, objectToUpdateInDB.getLienFichier());
            ps.setInt(4, objectToUpdateInDB.getConcerneStage().getIdStage());
            ps.setInt(5, objectToUpdateInDB.getConserneEleve().getIdEleve());

            ps.setInt(6, objectToUpdateInDB.getNumFeedback());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Feedback objectToDeleteFromDB) {
        //delete = "delete from Feedback where numFeedback = ?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getNumFeedback());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Feedback> getList() {
        //find_all = "select * from Feedback";
        List<Feedback> feedbackList = new ArrayList<Feedback>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Feedback pojoFeedback = new Feedback();
                pojoFeedback.setNumFeedback(rs.getInt("numFeedback"));
                pojoFeedback.setDateRemise(rs.getDate("dateRemise"));
                pojoFeedback.setMessage(rs.getString("message"));
                pojoFeedback.setLienFichier(rs.getString("lienFichier"));

                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoFeedback.setConcerneStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Eleve daoEleve = new DAO_Eleve(connection);
                pojoFeedback.setConserneEleve(daoEleve.find(rs.getInt("idEleve")));

                feedbackList.add(pojoFeedback);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return feedbackList;
    }

    @Override
    public List<Feedback> getListByMotcle(String mc) {
        //find_by_mc = "select * from Feedback where dateRemise like ?"; //ATTENTION
        List<Feedback> feedbackList = new ArrayList<Feedback>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Feedback pojoFeedback = new Feedback();
                pojoFeedback.setNumFeedback(rs.getInt("numFeedback"));
                pojoFeedback.setDateRemise(rs.getDate("dateRemise"));
                pojoFeedback.setMessage(rs.getString("message"));
                pojoFeedback.setLienFichier(rs.getString("lienFichier"));

                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoFeedback.setConcerneStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Eleve daoEleve = new DAO_Eleve(connection);
                pojoFeedback.setConserneEleve(daoEleve.find(rs.getInt("idEleve")));

                feedbackList.add(pojoFeedback);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return feedbackList;
    }

}
