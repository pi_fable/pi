package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Entreprise;
import POJO.SecteurActivite;
import POJO.Technologie;

public class DAO_Entreprise extends DAO<Entreprise> {
    private String find_by_id = "select * from entreprise where idEntreprise=?";
    private String create = "insert into entreprise(URLSite, nom, idAdresse) values(?,?,?)";
    private String update = "update entreprise set URLSite=?, nom=?, idAdresse=?  where idEntreprise=?";
    private String delete = "delete from entreprise where idEntreprise = ?";
    private String find_all = "select * from entreprise";
    private String find_by_mc = "select * from entreprise where nom like ?";

    public DAO_Entreprise(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Entreprise objectToInsertInDB) {
		//create = "insert into entreprise(URLSite, nom, idAdresse) values(?,?,?)";
		try {
			PreparedStatement ps = this.connection.prepareStatement(create);
			ps.setString(1, objectToInsertInDB.getURLSite());
			ps.setString(2, objectToInsertInDB.getNom());
			ps.setInt(3, objectToInsertInDB.getAdrs().getIdAdresse());
			
			ps.executeUpdate();
			
			ResultSet r=ps.executeQuery("SELECT LAST_INSERT_ID()");
			
			r.next();
			int id=r.getInt(1);
			objectToInsertInDB.setIdEntreprise(id);
			this.lierSectAct(objectToInsertInDB);
			this.lierTech(objectToInsertInDB);
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

            if (rs.next()) {
                pojoEntrep = new Entreprise();
                pojoEntrep.setIdEntreprise(rs.getInt("idEntreprise"));
                pojoEntrep.setURLSite(rs.getString("URLSite"));
                pojoEntrep.setNom(rs.getString("nom"));

                //Ajout Adresse
                DAO_Adresse daoAdr = new DAO_Adresse(connection);

                //ajout liste activi avec verif liste vide
                pojoEntrep.setAdrs(daoAdr.find(rs.getInt("idAdresse")));

	@Override
	public List<Entreprise> getListByMotcle(String mc) {
		//find_by_mc = "select * from entreprise where nom like ?";
		List<Entreprise> entreps = new ArrayList<Entreprise>();
		try {
			PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
			ps.setString(1, "%"+mc+"%");
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Entreprise pojoEntrep = new Entreprise();
				pojoEntrep.setIdEntreprise(rs.getInt("idEntreprise"));
				pojoEntrep.setURLSite(rs.getString("URLSite"));
				pojoEntrep.setNom(rs.getString("nom"));
				
				//Ajout Adresse
				DAO_Adresse daoAdr = new DAO_Adresse(connection);
				
				pojoEntrep.setAdrs(daoAdr.find(rs.getInt("idAdresse")));
				
				//ajout des technologie
				pojoEntrep.setListTechno(this.getAllTechnologie(rs.getInt("idEntreprise")));
				//ajout des secteur d'activiter
				pojoEntrep.setListSectAct(this.getAllSecteurActivite(rs.getInt("idEntreprise")));
				
				entreps.add(pojoEntrep);
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO Auto-generated method stub
		return entreps;
	}
	
	//METHODE LIER AU TECHNOLOGIE D'UNE ENTREPRISE
	
	
	public List<Technologie> getAllTechnologie(int idEntrep){
		String query = "select * from technologie where idTech in (select idTech from r_entr_techno where idEntreprise = ? )";
		List<Technologie> techs = new ArrayList<Technologie>();
		try {
			PreparedStatement ps = this.connection.prepareStatement(query);
			ps.setInt(1, idEntrep);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Technologie pojoTech = new Technologie();
				pojoTech.setIdTechnologie(rs.getInt("idTech"));
				pojoTech.setNom(rs.getString("nomTech"));
				
				techs.add(pojoTech);
			}
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// TODO Auto-generated method stub
			return techs;
	}
	private void deletelierTech(Entreprise objectDelete){
		String query = "delete from r_entr_techno where idEntreprise =?";
		//String delete = "delete from entreprise where idEntreprise = ?"
		
			try {
				PreparedStatement ps = this.connection.prepareStatement(query);
				ps.setInt(1, objectDelete.getIdEntreprise());
				
				
				ps.executeUpdate();
				ps.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		
	}
	private void updatelierTech(Entreprise objectToUpdate){
		this.deletelierTech(objectToUpdate);
		this.lierTech(objectToUpdate);
	}
	private void lierTech(Entreprise objectTolink){
		String query = "insert into r_entr_techno(idEntreprise, idTech) values(?,?)";
		List<Technologie> listtech = this.getAllTechnologie(objectTolink.getIdEntreprise());
		if(objectTolink.getListTechno()!=null){
			for (Technologie tech : objectTolink.getListTechno()) {
				boolean ok = true;
				
				for(int i = 0; i<listtech.size(); i++) 
					if(tech.toString().equals(listtech.get(i).toString()))ok = false;
				if(ok){
					try {
						PreparedStatement ps = this.connection.prepareStatement(query);
						ps.setInt(1, objectTolink.getIdEntreprise());
						ps.setInt(2,  tech.getIdTechnologie());
						
						ps.executeUpdate();
						ps.close();
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}	
			}
		}
		
	}
	
	//METHODE LIER AU SECTEUR D'ACTIVITE D'UNE ENTREPRISE
	
	public List<SecteurActivite> getAllSecteurActivite(int idEntrep){
		String query = "select * from secteuractivite where idActivite in (select idActivite from listesecteur where idEntreprise = ? )";
		List<SecteurActivite> sectActs = new ArrayList<SecteurActivite>();
		try {
			PreparedStatement ps = this.connection.prepareStatement(query);
			ps.setInt(1, idEntrep);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
					SecteurActivite pojoSectAct = new SecteurActivite();
					pojoSectAct.setIdActivite(rs.getInt("idActivite"));
					pojoSectAct.setNomActivite(rs.getString("nomActivite"));
					
					sectActs.add(pojoSectAct);
			}
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// TODO Auto-generated method stub
			return sectActs;
	}
	private void deletelierSectAct(Entreprise objectDelete){
		String query = "delete from listesecteur where idEntreprise =?";
		//String delete = "delete from entreprise where idEntreprise = ?"
		
			try {
				PreparedStatement ps = this.connection.prepareStatement(query);
				ps.setInt(1, objectDelete.getIdEntreprise());
				
				
				ps.executeUpdate();
				ps.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		
	}
	private void updatelierSectAct(Entreprise objectToUpdate){
		this.deletelierSectAct(objectToUpdate);
		this.lierSectAct(objectToUpdate);
	}
	private void lierSectAct(Entreprise objectTolink){
		String query = "insert into listesecteur(idEntreprise, idActivite) values(?,?)";
		List<SecteurActivite> listSecteurActivite = this.getAllSecteurActivite(objectTolink.getIdEntreprise());
		if(objectTolink.getListSectAct()!=null){
			for (SecteurActivite act : objectTolink.getListSectAct()) {
				boolean ok = true;
				for(int i = 0; i<listSecteurActivite.size(); i++) 
					if(act.toString().equals(listSecteurActivite.get(i).toString()))ok = false;
				if(ok){
					try {
						
						PreparedStatement ps = this.connection.prepareStatement(query);
						ps.setInt(1, objectTolink.getIdEntreprise());
						ps.setInt(2,  act.getIdActivite());
						
						ps.executeUpdate();
						ps.close();
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
	}
}
