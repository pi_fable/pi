package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Professeur;

public class DAO_Professeur extends DAO<Professeur> {

    private String find_by_id = "select * from professeur where idProf=?";
    private String create = "insert into professeur(nom, prenom, email, motDePasse) values(?,?,?,?)";
    private String update = "update professeur set nom=?, prenom=?, email=?, motDePasse=? where idProf=?";
    private String delete = "delete from professeur where idProf = ?";
    private String find_all = "select * from professeur";
    private String find_by_mc = "select * from professeur where nom like ?";

    public DAO_Professeur(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Professeur find(int Id) {
        //find_by_id = "select * from Professeur where idProf=?";
        Professeur pojoProfesseur = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoProfesseur = new Professeur();
                pojoProfesseur.setIdProf(rs.getInt("idProf"));
                pojoProfesseur.setNom(rs.getString("nom"));
                pojoProfesseur.setPrenom(rs.getString("prenom"));
                pojoProfesseur.setEmail(rs.getString("email"));
                pojoProfesseur.setMotDePasse(rs.getString("motDePasse"));
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoProfesseur == null) throw new RuntimeException("Question " + Id + " introuvable");
        return pojoProfesseur;
    }

    @Override
    public void create(Professeur objectToInsertInDB) {
        //create = "insert into Professeur(nom, prenom, email, motDePasse) values(?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);

            ps.setString(1, objectToInsertInDB.getNom());
            ps.setString(2, objectToInsertInDB.getPrenom());
            ps.setString(3, objectToInsertInDB.getEmail());
            ps.setString(4, objectToInsertInDB.getMotDePasse());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(Professeur objectToUpdateInDB) {
        //update = "update Professeur set nom=?, prenom=?, email=?, motDePasse=? where idProf=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);

            ps.setString(1, objectToUpdateInDB.getNom());
            ps.setString(2, objectToUpdateInDB.getPrenom());
            ps.setString(3, objectToUpdateInDB.getEmail());
            ps.setString(4, objectToUpdateInDB.getMotDePasse());

            ps.setInt(5, objectToUpdateInDB.getIdProf());

            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Professeur objectToDeleteFromDB) {
        //delete = "delete from Professeur where idProf = ?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdProf());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<Professeur> getList() {
        //find_all = "select * from Professeur";
        List<Professeur> professeurList = new ArrayList<Professeur>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Professeur pojoProfesseur = new Professeur();
                pojoProfesseur.setIdProf(rs.getInt("idProf"));
                pojoProfesseur.setNom(rs.getString("nom"));
                pojoProfesseur.setPrenom(rs.getString("prenom"));
                pojoProfesseur.setEmail(rs.getString("email"));
                pojoProfesseur.setMotDePasse(rs.getString("motDePasse"));

                professeurList.add(pojoProfesseur);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return professeurList;
    }

    @Override
    public List<Professeur> getListByMotcle(String mc) {
        //find_by_mc = "select * from Professeur where nom like ?";
        List<Professeur> professeurList = new ArrayList<Professeur>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);

            ps.setString(1, "%" + mc + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Professeur pojoProfesseur = new Professeur();
                pojoProfesseur.setIdProf(rs.getInt("idProf"));
                pojoProfesseur.setNom(rs.getString("nom"));
                pojoProfesseur.setPrenom(rs.getString("prenom"));
                pojoProfesseur.setEmail(rs.getString("email"));
                pojoProfesseur.setMotDePasse(rs.getString("motDePasse"));

                professeurList.add(pojoProfesseur);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return professeurList;
    }

}