package DAOPackage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import POJO.Eleve;
import POJO.PropositionDeStage;
import POJO.Technologie;

public class DAO_PropositionDeStage extends DAO<PropositionDeStage> {

    private String find_by_id = "select * from propositiondestage where idProp=?";
    private String create = "insert into propositiondestage(sujet, annee, description, types, idEntreprise, lienFichier) values(?,?,?,?,?,?)";
    private String update = "update propositiondestage set sujet=?, annee=?, description=?, types=?, idEntreprise=?, lienFichier=? where idProp=?";
    private String delete = "delete from propositiondestage where idProp = ?";
    private String find_all = "select * from propositiondestage";
    private String find_by_mc = "select * from propositiondestage where sujet like ?";

    public DAO_PropositionDeStage(Connection connect) {
        super(connect);
        // TODO Auto-generated constructor stub
    }

    @Override
    public PropositionDeStage find(int Id) {
        //find_by_id = "select * from PropositionDeStage where idProp=?";
        PropositionDeStage pojoProp = null;
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_id);
            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                pojoProp = new PropositionDeStage();
                pojoProp.setIdProp(rs.getInt("idProp"));
                pojoProp.setSujet(rs.getString("sujet"));
                pojoProp.setAnnee(rs.getInt("annee"));
                pojoProp.setDescription(rs.getString("description"));
                pojoProp.setTypes(rs.getString("types"));
                pojoProp.setLienFichier(rs.getString("lienFichier"));

                //Ajout Entreprise
                DAO_Entreprise daoEntrep = new DAO_Entreprise(connection);
                pojoProp.setEntrep(daoEntrep.find(rs.getInt("idEntreprise")));

                //Ajout Technologie
                pojoProp.setListTechno(this.getAllTechnologie(rs.getInt("idProp")));
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (pojoProp == null) throw new RuntimeException("PropositionDeStage " + Id + " introuvable");
        return pojoProp;
    }

    public int getLastIdCreate() {
        int id = 0;
        try {
            PreparedStatement ps = this.connection.prepareStatement("");
            ResultSet r = ps.executeQuery("SELECT LAST_INSERT_ID()");
            r.next();
            id = r.getInt(1);
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public void create(PropositionDeStage objectToInsertInDB) {
        //create = "insert into PropositionDeStage(sujet, annee, description, types, idEntreprise, lienFichier) values(?,?,?,?,?,?)";
        try {
            PreparedStatement ps = this.connection.prepareStatement(create);
            ps.setString(1, objectToInsertInDB.getSujet());
            ps.setInt(2, objectToInsertInDB.getAnnee());
            ps.setString(3, objectToInsertInDB.getDescription());
            ps.setString(4, objectToInsertInDB.getTypes());
            ps.setInt(5, objectToInsertInDB.getEntrep().getIdEntreprise());
            ps.setString(6, objectToInsertInDB.getLienFichier());

            ps.executeUpdate();

            ResultSet r = ps.executeQuery("SELECT LAST_INSERT_ID()");
            r.next();
            int id = r.getInt(1);
            objectToInsertInDB.setIdProp(id);
            this.lierTech(objectToInsertInDB);
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void update(PropositionDeStage objectToUpdateInDB) {
        //update = "update PropositionDeStage set sujet=?, annee=?, description=?, types=?, idEntreprise=?, lienFichier=? where idProp=?";
        try {
            PreparedStatement ps = this.connection.prepareStatement(update);
            ps.setString(1, objectToUpdateInDB.getSujet());
            ps.setInt(2, objectToUpdateInDB.getAnnee());
            ps.setString(3, objectToUpdateInDB.getDescription());
            ps.setString(4, objectToUpdateInDB.getTypes());
            ps.setInt(5, objectToUpdateInDB.getEntrep().getIdEntreprise());
            ps.setString(6, objectToUpdateInDB.getLienFichier());

            ps.setInt(7, objectToUpdateInDB.getIdProp());

            this.updatelierTech(objectToUpdateInDB);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void delete(PropositionDeStage objectToDeleteFromDB) {
        //delete = "delete from PropositionDeStage where idProp = ?";
        try {
            //delete = "delete from Entreprise where idEntreprise = ?";
            PreparedStatement ps = this.connection.prepareStatement(delete);
            ps.setInt(1, objectToDeleteFromDB.getIdProp());
            if (objectToDeleteFromDB.getListTechno() != null) {
                this.deletelierTech(objectToDeleteFromDB);
            }
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public List<PropositionDeStage> getList() {
        // find_all = "select * from PropositionDeStage";
        List<PropositionDeStage> propList = new ArrayList<PropositionDeStage>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_all);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PropositionDeStage pojoProp = new PropositionDeStage();
                pojoProp.setIdProp(rs.getInt("idProp"));
                pojoProp.setSujet(rs.getString("sujet"));
                pojoProp.setAnnee(rs.getInt("annee"));
                pojoProp.setDescription(rs.getString("description"));
                pojoProp.setTypes(rs.getString("types"));
                pojoProp.setLienFichier(rs.getString("lienFichier"));

                //Ajout Entreprise
                DAO_Entreprise daoEntrep = new DAO_Entreprise(connection);
                pojoProp.setEntrep(daoEntrep.find(rs.getInt("idEntreprise")));

                propList.add(pojoProp);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return propList;
    }

    @Override
    public List<PropositionDeStage> getListByMotcle(String mc) {
        //find_by_mc = "select * from PropositionDeStage where sujet like ?";
        List<PropositionDeStage> propList = new ArrayList<PropositionDeStage>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(find_by_mc);
            ps.setString(1, "%" + mc + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PropositionDeStage pojoProp = new PropositionDeStage();
                pojoProp.setIdProp(rs.getInt("idProp"));
                pojoProp.setSujet(rs.getString("sujet"));
                pojoProp.setAnnee(rs.getInt("number"));
                pojoProp.setDescription(rs.getString("description"));
                pojoProp.setTypes(rs.getString("types"));
                pojoProp.setLienFichier(rs.getString("lienFichier"));

                //Ajout Entreprise
                DAO_Entreprise daoEntrep = new DAO_Entreprise(connection);
                pojoProp.setEntrep(daoEntrep.find(rs.getInt("idEntreprise")));

                propList.add(pojoProp);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return propList;
    }

    public List<Technologie> getAllTechnologie(int idProp) {
        String query = "select * from technologie where idTech in (select idTech from r_prop_tech where idProp = ? )";
        List<Technologie> techs = new ArrayList<Technologie>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idProp);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Technologie pojoTech = new Technologie();
                pojoTech.setIdTechnologie(rs.getInt("idTech"));
                pojoTech.setNom(rs.getString("nomTech"));

                techs.add(pojoTech);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return techs;
    }

    public List<Eleve> getAllEleveInteresse(int idProp) {
        String query = "select * from eleve where idEleve in (select idEleve from eleveinteresse where idProp = ? )";
        List<Eleve> eleveList = new ArrayList<Eleve>();
        try {
            PreparedStatement ps = this.connection.prepareStatement(query);
            ps.setInt(1, idProp);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Eleve pojoEleve = new Eleve();
                pojoEleve.setIdEleve(rs.getInt("idEleve"));
                pojoEleve.setEmail(rs.getString("Email"));
                pojoEleve.setNom(rs.getString("nom"));
                pojoEleve.setPrenom(rs.getString("prenom"));
                pojoEleve.setMotDePasse(rs.getString("motDePasse"));

                if (rs.getString("isStudent").equals("true")) {
                    pojoEleve.setIsStudent(true);
                } else pojoEleve.setIsStudent(false);
                //Ajout Stage
                DAO_Stage daoStage = new DAO_Stage(connection);
                pojoEleve.setSonStage(daoStage.find(rs.getInt("idStage")));

                //Ajout Adresse
                DAO_Adresse daoAdresse = new DAO_Adresse(connection);
                pojoEleve.setSonAdresse(daoAdresse.find(rs.getInt("idAdresse")));

                eleveList.add(pojoEleve);
            }
            ps.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return eleveList;

    }

    //METHODE LIER AU TECHNOLOGIE D'UNE ENTREPRISE

    private void deletelierTech(PropositionDeStage objectDelete) {
        String query = "delete from r_prop_tech where idProp =? and idTech=?";
        //String delete = "delete from Entreprise where idEntreprise = ?"
        for (Technologie tech : objectDelete.getListTechno()) {
            try {
                PreparedStatement ps = this.connection.prepareStatement(query);
                ps.setInt(1, objectDelete.getIdProp());
                ps.setInt(2, tech.getIdTechnologie());

                ps.executeUpdate();
                ps.close();

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void lierTech(PropositionDeStage objectTolink) {
        String query = "insert into r_prop_tech(idProp, idTech) values(?,?)";
        List<Technologie> listtech = this.getAllTechnologie(objectTolink.getIdProp());
        if (objectTolink.getListTechno() != null) {
            for (Technologie tech : objectTolink.getListTechno()) {
                boolean ok = true;
                for (int i = 0; i < listtech.size(); i++)
                    if (tech.toString().equals(listtech.get(i).toString())) ok = false;
                if (ok) {
                    try {
                        PreparedStatement ps = this.connection.prepareStatement(query);
                        ps.setInt(1, objectTolink.getIdProp());
                        ps.setInt(2, tech.getIdTechnologie());

                        ps.executeUpdate();
                        ps.close();

                    } catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void updatelierTech(PropositionDeStage objectToUpdate) {
        this.deletelierTech(objectToUpdate);
        this.lierTech(objectToUpdate);
    }
}

