package Metier;

import java.io.File;
import java.sql.Connection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import DAOPackage.DAO;
import DAOPackage.DAO_Adresse;
import DAOPackage.DAO_Contact;
import DAOPackage.DAO_Eleve;
import DAOPackage.DAO_Entreprise;
import DAOPackage.DAO_Evaluation;
import DAOPackage.DAO_Evenement;
import DAOPackage.DAO_Feedback;
import DAOPackage.DAO_Formulaire;
import DAOPackage.DAO_MaitreDeStage;
import DAOPackage.DAO_Planning;
import DAOPackage.DAO_Professeur;
import DAOPackage.DAO_PropositionDeStage;
import DAOPackage.DAO_Question;
import DAOPackage.DAO_Reponse;
import DAOPackage.DAO_SecteurActivite;
import DAOPackage.DAO_Stage;
import DAOPackage.DAO_Technologie;
import POJO.Adresse;
import POJO.Contact;
import POJO.Eleve;
import POJO.Entreprise;
import POJO.Evaluation;
import POJO.Evenement;
import POJO.Feedback;
import POJO.Formulaire;
import POJO.MaitreDeStage;
import POJO.Planning;
import POJO.Professeur;
import POJO.PropositionDeStage;
import POJO.Question;
import POJO.Reponse;
import POJO.SecteurActivite;
import POJO.Stage;
import POJO.Technologie;

public class Controlleur implements InterfaceControlleur {

    private Connection connect;

    public Controlleur(Connection connect) {
        super();
        this.connect = connect;
    }

    @Override
    public Adresse getAdresse(int id) {
        return new DAO_Adresse(connect).find(id);
    }

    public List<Adresse> getAllAdresse() {
        return new DAO_Adresse(connect).getList();
    }

    @Override
    public boolean removeAdresse(Adresse a) {
        new DAO_Adresse(connect).delete(a);
        return false;
    }

    @Override
    public boolean updateAdresse(Adresse a) {
        new DAO_Adresse(connect).update(a);
        return false;
    }

    @Override
    public boolean addAdresse(Adresse a) {
        new DAO_Adresse(connect).create(a);
        return false;
    }

    @Override
    public Contact getContact(int id) {
        return new DAO_Contact(connect).find(id);
    }

    @Override
    public List<Contact> getAllContact() {
        return new DAO_Contact(connect).getList();
    }

    @Override
    public boolean updateContact(Contact c) {
        new DAO_Contact(connect).update(c);
        return false;
    }

    @Override
    public boolean removeContact(Contact c) {
        new DAO_Contact(connect).delete(c);
        return false;
    }

    @Override
    public boolean addContact(Contact c) {
        new DAO_Contact(connect).create(c);
        return false;
    }

    @Override
    public Eleve getProfil(int id) {
        return new DAO_Eleve(connect).find(id);
    }

    @Override
    public boolean addEleve(Eleve e) {
        new DAO_Eleve(connect).create(e);
        return false;
    }

    @Override
    public boolean addEleve(File f) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean removeEleve(Eleve e) {
        new DAO_Eleve(connect).delete(e);
        return false;
    }

	@Override
	public Entreprise getEntreprise(int id) {
		return new DAO_Entreprise(connect).find(id);
	}
	public int getLastidCreateTech(){
		return new DAO_Technologie(connect).getLastIdCreate();
<<<<<<< HEAD
=======
	}
	
	public int getLastidCreateAct(){
		return new DAO_SecteurActivite(connect).getLastIdCreate();
	}
	@Override
	public boolean addEntreprise(Entreprise e,String[] tech,String[]act) {
		if(tech.length>0)lierTech(tech,e);
		if(act.length>0)lierAct(act, e);
		
		DAO<Adresse>dao=new DAO_Adresse(connect);
		List<Adresse> adresses=dao.getList();
		boolean ok=false;
		int i;
		for (i = 0; i < adresses.size() && !ok; i++) {
			if(adresses.get(i).equals(e.getAdrs()))ok=true;
		}
		if(!ok){
			dao.create(e.getAdrs());
			e.getAdrs().setIdAdresse(adresses.get(adresses.size()-1).getIdAdresse()+1);
		}
		else e.getAdrs().setIdAdresse(adresses.get(i-1).getIdAdresse());
		
		
		new DAO_Entreprise(connect).create(e);
		return false;
	}
	
	@Override
	public boolean updateEntreprise(Entreprise e,String[] tech,String[]act) {
		if(tech.length>0)lierTech(tech,e);
		if(act.length>0)lierAct(act, e);
		
		DAO<Adresse>dao=new DAO_Adresse(connect);
		List<Adresse> adresses=dao.getList();
		boolean ok=false;
		int i;
		for (i = 0; i < adresses.size() && !ok; i++) {
			if(adresses.get(i).equals(e.getAdrs()))ok=true;
		}
		if(!ok){
			dao.create(e.getAdrs());
			e.getAdrs().setIdAdresse(adresses.get(adresses.size()-1).getIdAdresse()+1);
		}
		else e.getAdrs().setIdAdresse(adresses.get(i-1).getIdAdresse());
		
		
		new DAO_Entreprise(connect).update(e);
		return false;
	}
	private void lierTech(String [] tabTech, Entreprise e){
		
		List<Technologie> listTechno=getAllTechnologie();
		List<Technologie> techEntreprise=new ArrayList<Technologie>();
		int i=0;
		int j;
		while(i<tabTech.length){
			j=0;
			while(j<listTechno.size() && !tabTech[i].equals(listTechno.get(j).getNom())){
				j++;
			}
			if(j==listTechno.size()){
				Technologie t=new Technologie(tabTech[i], 0);
				addTechnologie(t);
				int id=getLastidCreateTech();
				t.setIdTechnologie(id);
				techEntreprise.add(t);
			}
			else{
				techEntreprise.add(listTechno.get(j));
			}
			
			i++;
		}
		e.setListTechno(techEntreprise);
	}
	
	
	
	private void lierAct(String [] tabAct, Entreprise e){
		List<SecteurActivite> listAct=getAllSecteurActivite();
		List<SecteurActivite> actEntreprise=new ArrayList<SecteurActivite>();
		int i=0;
		int j;
		while(i<tabAct.length){
			j=0;
			while(j<listAct.size() && !tabAct[i].equals(listAct.get(j).getNomActivite())){
				j++;
			}
			if(j==listAct.size()){
				SecteurActivite s=new SecteurActivite(0, tabAct[i]);
				addSecteurActivite(s);
				int id=getLastidCreateAct();
				s.setIdActivite(id);
				actEntreprise.add(s);
			}
			else{
				actEntreprise.add(listAct.get(j));
			}
			
			i++;
		}
		
		e.setListSectAct(actEntreprise);
	}
	@Override
	public boolean removeEntreprise(int id) {
		DAO<Entreprise>dao=new DAO_Entreprise(connect);
		dao.delete(dao.find(id));
		
		return false;
	}

	@Override
	public List<Entreprise> getAllEntreprise() {
		return new DAO_Entreprise(connect).getList();
>>>>>>> master
	}
	
	public int getLastidCreateAct(){
		return new DAO_SecteurActivite(connect).getLastIdCreate();
	}
	@Override
	public boolean addEntreprise(Entreprise e,String[] tech,String[]act) {
		if(tech.length>0)lierTech(tech,e);
		if(act.length>0)lierAct(act, e);
		
		DAO<Adresse>dao=new DAO_Adresse(connect);
		List<Adresse> adresses=dao.getList();
		boolean ok=false;
		int i;
		for (i = 0; i < adresses.size() && !ok; i++) {
			if(adresses.get(i).equals(e.getAdrs()))ok=true;
		}
		if(!ok){
			dao.create(e.getAdrs());
			e.getAdrs().setIdAdresse(adresses.get(adresses.size()-1).getIdAdresse()+1);
		}
		else e.getAdrs().setIdAdresse(adresses.get(i-1).getIdAdresse());
		
		
		new DAO_Entreprise(connect).create(e);
		return false;
	}
<<<<<<< aud
    @Override
    public List<Eleve> getAllEleve() {
        return new DAO_Eleve(connect).getList();
    }

    @Override
    public boolean updateEleve(Eleve e) {
        new DAO_Eleve(connect).update(e);
        return false;
    }

    @Override
    public Entreprise getEntreprise(int id) {
        return new DAO_Entreprise(connect).find(id);
    }

=======
	
	@Override
	public boolean updateEntreprise(Entreprise e,String[] tech,String[]act) {
		if(tech.length>0)lierTech(tech,e);
		if(act.length>0)lierAct(act, e);
		
		DAO<Adresse>dao=new DAO_Adresse(connect);
		List<Adresse> adresses=dao.getList();
		boolean ok=false;
		int i;
		for (i = 0; i < adresses.size() && !ok; i++) {
			if(adresses.get(i).equals(e.getAdrs()))ok=true;
		}
		if(!ok){
			dao.create(e.getAdrs());
			e.getAdrs().setIdAdresse(adresses.get(adresses.size()-1).getIdAdresse()+1);
		}
		else e.getAdrs().setIdAdresse(adresses.get(i-1).getIdAdresse());
		
		
		new DAO_Entreprise(connect).update(e);
		return false;
	}
	private void lierTech(String [] tabTech, Entreprise e){
		
		List<Technologie> listTechno=getAllTechnologie();
		List<Technologie> techEntreprise=new ArrayList<Technologie>();
		int i=0;
		int j;
		while(i<tabTech.length){
			j=0;
			while(j<listTechno.size() && !tabTech[i].equals(listTechno.get(j).getNom())){
				j++;
			}
			if(j==listTechno.size()){
				Technologie t=new Technologie(tabTech[i], 0);
				addTechnologie(t);
				int id=getLastidCreateTech();
				t.setIdTechnologie(id);
				techEntreprise.add(t);
			}
			else{
				techEntreprise.add(listTechno.get(j));
			}
			
			i++;
		}
		e.setListTechno(techEntreprise);
	}
	
	
	
	private void lierAct(String [] tabAct, Entreprise e){
		List<SecteurActivite> listAct=getAllSecteurActivite();
		List<SecteurActivite> actEntreprise=new ArrayList<SecteurActivite>();
		int i=0;
		int j;
		while(i<tabAct.length){
			j=0;
			while(j<listAct.size() && !tabAct[i].equals(listAct.get(j).getNomActivite())){
				j++;
			}
			if(j==listAct.size()){
				SecteurActivite s=new SecteurActivite(0, tabAct[i]);
				addSecteurActivite(s);
				int id=getLastidCreateAct();
				s.setIdActivite(id);
				actEntreprise.add(s);
			}
			else{
				actEntreprise.add(listAct.get(j));
			}
			
			i++;
		}
		
		e.setListSectAct(actEntreprise);
	}
>>>>>>> entreprise
	@Override
	public boolean removeEntreprise(int id) {
		DAO<Entreprise>dao=new DAO_Entreprise(connect);
		dao.delete(dao.find(id));
		
		return false;
	}
    @Override
    public boolean addEntreprise(Entreprise e) {
        new DAO_Entreprise(connect).create(e);
        return false;
    }

    @Override
    public boolean updateEntreprise(Entreprise e) {
        new DAO_Entreprise(connect).update(e);
        return false;
    }

    @Override
    public boolean removeEntreprise(Entreprise e) {
        new DAO_Entreprise(connect).delete(e);
        return false;
    }

    @Override
    public List<Entreprise> getAllEntreprise() {
        return new DAO_Entreprise(connect).getList();
    }

    @Override
    public Evaluation getEvaluation(int id) {
        return new DAO_Evaluation(connect).find(id);
    }

    @Override
    public List<Evaluation> getAllEvaluation() {
        return new DAO_Evaluation(connect).getList();
    }

    @Override
    public boolean addEvaluation(Evaluation e) {
        new DAO_Evaluation(connect).create(e);
        return false;
    }

    @Override
    public boolean updateEvaluation(Evaluation e) {
        new DAO_Evaluation(connect).update(e);
        return false;
    }

    @Override
    public boolean removeEvaluation(Evaluation e) {
        new DAO_Evaluation(connect).delete(e);
        return false;
    }

    @Override
    public Evenement getEvenement(int id) {
        return new DAO_Evenement(connect).find(id);
    }

    @Override
    public List<Evenement> getAllEvenement(int annee) {
        return new DAO_Evenement(connect).getList();
    }

    @Override
    public boolean addEvenement(Evenement e, String[] participant) {
        DAO<Adresse> dao = new DAO_Adresse(connect);
        List<Adresse> adresses = dao.getList();
        boolean ok = false;
        int i;
        for (i = 0; i < adresses.size() && !ok; i++) {
            if (adresses.get(i).equals(e.getAdrs())) ok = true;
        }
        if (!ok) {
            dao.create(e.getAdrs());
            e.getAdrs().setIdAdresse(adresses.get(adresses.size() - 1).getIdAdresse() + 1);
        } else e.getAdrs().setIdAdresse(adresses.get(i - 1).getIdAdresse());
        List<Contact> participants = null;
        if (participant != null) {
            DAO<Contact> dao2 = new DAO_Contact(connect);

            participants = new ArrayList<Contact>();
            for (String cont : participant) {

                participants.add(dao2.find(Integer.parseInt(cont)));
            }
        }
        e.setListContact(participants);
        try {
            new DAO_Evenement(connect).create(e);
            return true;
        } catch (RuntimeException re) {
            // TODO: handle exception
            return false;
        }

    }

    @Override
    public boolean updateEvenement(Evenement e, String[] participant) {
        DAO<Adresse> dao = new DAO_Adresse(connect);
        List<Adresse> adresses = dao.getList();
        boolean ok = false;
        int i;
        for (i = 0; i < adresses.size() && !ok; i++) {
            if (adresses.get(i).equals(e.getAdrs())) ok = true;
        }
        if (!ok) {
            dao.create(e.getAdrs());
            e.getAdrs().setIdAdresse(adresses.get(adresses.size() - 1).getIdAdresse() + 1);
        } else e.getAdrs().setIdAdresse(adresses.get(i - 1).getIdAdresse());
        List<Contact> participants = null;
        if (participant != null) {
            DAO<Contact> dao2 = new DAO_Contact(connect);

            participants = new ArrayList<Contact>();
            for (String cont : participant) {

                participants.add(dao2.find(Integer.parseInt(cont)));
            }
        }
        e.setListContact(participants);
        System.err.println("test");
        try {
            new DAO_Evenement(connect).update(e);
            System.err.println("true");
            return true;
        } catch (RuntimeException re) {
            System.err.println("false");
            return false;
        }


    }

    @Override
    public boolean removeEvenement(int id) {
        DAO<Evenement> dao = new DAO_Evenement(connect);
        try {
            dao.delete(dao.find(id));
            return true;
        } catch (RuntimeException re) {
            return false;
        }


    }

    @Override
    public Feedback getFeedback(int id) {
        return new DAO_Feedback(connect).find(id);
    }

    @Override
    public List<Feedback> getAllFeedback(int id) {
        return new DAO_Feedback(connect).getList();
    }

    @Override
    public boolean addFeedback(Feedback e) {
        new DAO_Feedback(connect).create(e);
        return false;
    }

    @Override
    public boolean updateFeedback(Feedback e) {
        new DAO_Feedback(connect).update(e);
        return false;
    }

    @Override
    public boolean removeFeedback(Feedback e) {
        new DAO_Feedback(connect).delete(e);
        return false;
    }

    @Override
    public Formulaire getFormulaire(int id) {
        return new DAO_Formulaire(connect).find(id);
    }

    @Override
    public List<Formulaire> getAllFormulaire() {
        return new DAO_Formulaire(connect).getList();
    }

    @Override
    public boolean addFormulaire(Formulaire e) {
        new DAO_Formulaire(connect).create(e);
        return false;
    }

    @Override
    public boolean updateFormulaire(Formulaire e) {
        new DAO_Formulaire(connect).update(e);
        return false;
    }

    @Override
    public boolean removeFormulaire(Formulaire e) {
        new DAO_Formulaire(connect).delete(e);
        return false;
    }

    @Override
    public MaitreDeStage getMaitreDeStage(int id) {
        return new DAO_MaitreDeStage(connect).find(id);
    }

    @Override
    public List<MaitreDeStage> getAllMaitreDeStage() {
        return new DAO_MaitreDeStage(connect).getList();
    }

    @Override
    public boolean addMaitreDeStage(MaitreDeStage e) {
        new DAO_MaitreDeStage(connect).create(e);
        return false;
    }

    @Override
    public boolean updateMaitreDeStage(MaitreDeStage e) {
        new DAO_MaitreDeStage(connect).update(e);
        return false;
    }

    @Override
    public boolean removeMaitreDeStage(MaitreDeStage e) {
        new DAO_MaitreDeStage(connect).delete(e);
        return false;
    }

    @Override
    public Planning getPlanning(int annee) {
        return new DAO_Planning(connect).find(annee);
    }

    @Override
    public List<Planning> getAllPlanning() {
        return new DAO_Planning(connect).getList();
    }

    @Override
    public boolean addPlanning(Planning e) {
        new DAO_Planning(connect).create(e);
        return false;
    }

    @Override
    public boolean updatePlanning(Planning e) {
        new DAO_Planning(connect).update(e);
        return false;
    }

    @Override
    public boolean removePlanning(Planning e) {
        new DAO_Planning(connect).delete(e);
        return false;
    }

    @Override
    public Professeur getProfesseur(int id) {
        return new DAO_Professeur(connect).find(id);
    }

    @Override
    public List<Professeur> getAllProfesseur() {
        return new DAO_Professeur(connect).getList();
    }

    @Override
    public boolean addProfesseur(Professeur e) {
        new DAO_Professeur(connect).create(e);
        return false;
    }

    @Override
    public boolean updateProfesseur(Professeur e) {
        new DAO_Professeur(connect).update(e);
        return false;
    }

    @Override
    public boolean removeProfesseur(Professeur e) {
        new DAO_Professeur(connect).delete(e);
        return false;
    }

    @Override
    public PropositionDeStage getPropositionDeStage(int id) {
        return new DAO_PropositionDeStage(connect).find(id);
    }

    @Override
    public List<PropositionDeStage> getAllPropositionDeStage() {
        return new DAO_PropositionDeStage(connect).getList();
    }

    @Override
    public boolean addPropositionDeStage(PropositionDeStage e) {


        try {
            new DAO_PropositionDeStage(connect).create(e);
            return true;
        } catch (RuntimeException re) {
            return false;
        }
    }

    @Override
    public boolean updatePropositionDeStage(PropositionDeStage e) {


        try {
            new DAO_PropositionDeStage(connect).update(e);
            return true;
        } catch (RuntimeException re) {
            return false;
        }
    }

    @Override
    public boolean removePropositionDeStage(int id) {

        DAO<PropositionDeStage> dao = new DAO_PropositionDeStage(connect);
        try {
            dao.delete(dao.find(id));
            return true;
        } catch (RuntimeException e) {
            return false;
        }

    }

    @Override
    public Question getQuestion(int id) {
        return new DAO_Question(connect).find(id);
    }

    @Override
    public List<Question> getAllQuestion(int idForm) {
        return new DAO_Question(connect).getList();
    }

    @Override
    public boolean addQuestion(Question e) {
        new DAO_Question(connect).create(e);
        return false;
    }

    @Override
    public boolean updateQuestion(Question e) {
        new DAO_Question(connect).update(e);
        return false;
    }

    @Override
    public boolean removeQuestion(Question e) {
        new DAO_Question(connect).delete(e);
        return false;
    }

    @Override
    public Reponse getReponse(int id) {
        return new DAO_Reponse(connect).find(id);
    }

    @Override
    public List<Reponse> getAllReponse(int numEva) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean addReponse(Reponse e) {
        new DAO_Reponse(connect).create(e);
        return false;
    }

    @Override
    public boolean updateReponse(Reponse e) {
        new DAO_Reponse(connect).update(e);
        return false;
    }

    @Override
    public boolean removeReponse(Reponse e) {
        new DAO_Reponse(connect).delete(e);
        return false;
    }

    @Override
    public SecteurActivite getSecteurActivite(int id) {
        return new DAO_SecteurActivite(connect).find(id);
    }

    @Override
    public List<SecteurActivite> getAllSecteurActivite() {
        return new DAO_SecteurActivite(connect).getList();
    }

    @Override
    public List<SecteurActivite> getAllSecteurActivite(int idEntreprise) {
        return new DAO_Entreprise(connect).getAllSecteurActivite(idEntreprise);
    }

    @Override
    public boolean addSecteurActivite(SecteurActivite e) {
        new DAO_SecteurActivite(connect).create(e);
        return false;
    }

    @Override
    public boolean updateSecteurActivite(SecteurActivite e) {
        new DAO_SecteurActivite(connect).update(e);
        return false;
    }

    @Override
    public boolean removeSecteurActivite(SecteurActivite e) {
        new DAO_SecteurActivite(connect).delete(e);
        return false;
    }

    @Override
    public Stage getStage(int id) {
        return new DAO_Stage(connect).find(id);
    }

    @Override
    public List<Stage> getAllStage() {
        return new DAO_Stage(connect).getList();
    }

    @Override
    public boolean addStage(Stage e) {
        new DAO_Stage(connect).create(e);
        return false;
    }

    @Override
    public boolean updateStage(Stage e) {
        new DAO_Stage(connect).update(e);
        return false;
    }

    @Override
    public boolean removeStage(int id) {
        DAO<Stage> dao = new DAO_Stage(connect);
        try {
            dao.delete(dao.find(id));
            return true;
        } catch (RuntimeException e) {
            return false;
        }

    }

    @Override
    public Technologie getTechnologie(int id) {
        return new DAO_Technologie(connect).find(id);
    }

    @Override
    public List<Technologie> getAllTechnologie() {
        return new DAO_Technologie(connect).getList();
    }

    @Override
    public List<Technologie> getAllTechnologieEntreprise(int idEntreprise) {
        return new DAO_Entreprise(connect).getAllTechnologie(idEntreprise);
    }

    @Override
    public List<Technologie> getAllTechnologiePropStage(int idProp) {
        return new DAO_PropositionDeStage(connect).getAllTechnologie(idProp);
    }

    @Override
    public boolean addTechnologie(Technologie e) {
        new DAO_Technologie(connect).create(e);
        return false;
    }

    @Override
    public boolean updateTechnologie(Technologie e) {
        new DAO_Technologie(connect).update(e);
        return false;
    }

    @Override
    public boolean removeTechnologie(Technologie e) {
        new DAO_Technologie(connect).delete(e);
        return false;
    }

    @Override
    public List<Entreprise> rechercherEntreprise(String[] param) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<PropositionDeStage> rechercherPropositionDeStage(String[][] param) {
        // TODO Auto-generated method stub
        DAO<PropositionDeStage> dao = new DAO_PropositionDeStage(connect);
        List<PropositionDeStage> sta;
        if(param[0][0] != null) {
            sta = dao.getListByMotcle(param[0][0]);
            int i =0;
            while(i < sta.size()){
                if(param[1][0] != null && Integer.parseInt(param[1][0]) != sta.get(i).getAnnee())
                    sta.remove(i);
                else if(param[2].length >0 ){
                    List<Technologie> tech = sta.get(i).getListTechno();
                    boolean ok = false;
                    for (String tmp : param[2]) {
                        for (int j = 0; j < tech.size() && !ok; j++)
                            if(tmp.equals(tech.get(j).getNom())) ok = true;
                        if(ok) break;
                    }
                    if(!ok) sta.remove(i);
                }
                else if(!param[3][0].equalsIgnoreCase(sta.get(i).getEntrep().getAdrs().getPays()))
                    sta.remove(i);
            }
            return sta;
        }
        return null;

    }

    @Override
    public List<Stage> rechercherStage(String[] param) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Contact> rechercherContact(String[] param) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Connection getConnection() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean connect(String email, String password) {
        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public boolean disconnect(int idUti, String type) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean validerStage(Stage s, Eleve e, MaitreDeStage m) {
        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public boolean interetStage(Eleve e, PropositionDeStage p) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<Evenement> searchEvenementBy(String sujet, String annee, String dateDeb, String dateFin) {
        DAO_Planning dao = new DAO_Planning(connect);
        DAO_Evenement dao2 = new DAO_Evenement(connect);
        List<Evenement> even;

        if (sujet != "") {
            even = dao2.getListByMotcle(sujet);
            String param[] = {annee, dateDeb, dateFin};
            even = filtrerEven(param, even);
        } else if (annee != "") {
            even = dao.getAllEvenement(Integer.parseInt(annee));
            String param[] = {"", dateDeb, dateFin};
            even = filtrerEven(param, even);
        } else {
            even = dao2.getList();
            String param[] = {"", dateDeb, dateFin};
            even = filtrerEven(param, even);
        }
        return even;
    }

    public String convert(String date) {
        String[] tmp = date.split("-");
        date = tmp[2] + "-" + tmp[1] + "-" + tmp[0];
        return date;
    }

    private List<Evenement> filtrerEven(String[] param, List<Evenement> even) {
        LocalDate d1 = null;
        LocalDate d2 = null;
        if (param[1] != "") d1 = LocalDate.parse(convert(param[1]));
        if (param[2] != "") d2 = LocalDate.parse(convert(param[2]));
        int i = 0;
        while (i < even.size()) {
            Evenement evenement = even.get(i);
            if (param[0] != "" && evenement.getDateEven().getYear() != Integer.parseInt(param[0]))
                even.remove(evenement);
            else if ((d1 != null && evenement.getDateEven().isBefore(d1)) | (d2 != null && evenement.getDateEven().isAfter(d2)))
                even.remove(evenement);
            else i++;
        }
        return even;
    }

    public int getLastIdCreateProp() {
        return new DAO_PropositionDeStage(connect).getLastIdCreate();
    }

    @Override
    public MaitreDeStage findByIdContact(int idContact) {
        // TODO Auto-generated method stub
        return new DAO_MaitreDeStage(connect).findByIdContact(idContact);
    }
}
