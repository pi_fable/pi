package Metier;

import java.util.List;

import DAOPackage.ConnexionBD;
import POJO.Eleve;
import POJO.Stage;

public class TestDAO_Controlleur {

    public static void main(String[] args) {

        System.out.println("Connection BD");
        ConnexionBD connect = new ConnexionBD();
        System.out.println("Cr�ation Controlleur");
        Controlleur test = new Controlleur(connect.getConnexion());

		/*
		//Test Planning :
		
		//appel Planning
		//creer un Planning
		Planning planningTest = new Planning(2018);
		test.addPlanning(planningTest); //OK
		//System.out.println(planningTest);
		//recherche 1 Planning et update:
		planningTest = test.getPlanning(2018);
		System.out.println(planningTest);
		planningTest.setAnneeScolaire(2016);
		//test.updatePlanning(planningTest); //probleme update (normal, juste un id dans la BD)
		//recherche tous les Planning:
		System.out.println(test.getAllPlanning());
		//supprimer un Planning
		List<Planning> listPlanning = test.getAllPlanning();
		planningTest = listPlanning.get(listPlanning.size()-1);
		test.removePlanning(planningTest);
		System.out.println(test.getAllPlanning());
		*/
		/*
		//Test Adresse :
			
		//appel Adresse
		//creer une Adresse
		Adresse adrsTest = new Adresse(0, "rueTest", 400, 20, "Libramont", "Belgique");
		test.addAdresse(adrsTest);
		System.out.println(adrsTest);
		//recherche 1 Adresse et update:
		adrsTest = test.getAdresse(3);
		System.out.println(adrsTest);
		adrsTest.setNumRue(20);
		adrsTest.setPays("Lointain");
		//recherche toutes les Adresses:
		System.out.println(test.getAllAdresse());
		//supprimer une Adresse
		List<Adresse> listAdresse = test.getAllAdresse();
		adrsTest = listAdresse.get(listAdresse.size()-1);
		test.removeAdresse(adrsTest);
		System.out.println(test.getAllAdresse());
		*/
		/*
		//Test Evenement :
		
		//appel Evenement
		//creer un Evenement
		Adresse adrs = test.getAdresse(2);
		Planning plan = test.getPlanning(2017);
		
		Evenement evenementTest = new Evenement(0, new Date(20, 04, 2017), "sujetTest", "descriptionTest", adrs, plan);
		System.out.println(evenementTest);
		test.addEvenement(evenementTest);
		System.out.println(evenementTest);
		//recherche 1 Evenement et update:
		System.out.println("recherche 4eme evenement :");
		evenementTest = test.getEvenement(4);
		System.out.println("Changement de variable et ajout de participant");
		System.out.println(evenementTest);
		evenementTest.setDescription("descriptionChange");
		Contact contact1 = test.getContact(1);
		Contact contact2 = test.getContact(2);
		
		List<Contact> listContact = evenementTest.getListContact();
		listContact.add(contact1);
		listContact.add(contact2);
		
		evenementTest.setListContact(listContact);
		System.out.println("Envois update de : ");
		test.updateEvenement(evenementTest);
		//recherche tous les contacts:
		System.out.println(test.getAllEvenement());
		//supprimer un contact
		List<Evenement> listEvenement = test.getAllEvenement();
		evenementTest = listEvenement.get(listEvenement.size()-1);
		System.out.println("suppresion de :\n"+evenementTest);
		test.removeEvenement(evenementTest);
		System.out.println(test.getAllEvenement());
		*/
		/*
		//Test Entreprise :
		System.out.println("Test Entreprise");
		//appel Entreprise
		//creer une Entreprise
		Entreprise testEntreprise = new Entreprise(0, "blabmaUrl.com", "nomEntrepriseTest", test.getAdresse(3));
		
		//ajout des secteur d'activiter
		System.out.println("Ajout secteur d'activiter");
		testEntreprise.setListSectAct(test.getAllSecteurActivite());
		
		//ajout des technologie
		System.out.println("Liste toutes Entreprise\n"+test.getAllTechnologie());
		System.out.println("Ajout technologie");
		testEntreprise.setListTechno(test.getAllTechnologie());
		System.out.println("Ajout Entreprise en BD");
		test.addEntreprise(testEntreprise);
		System.out.println(testEntreprise);
		//recherche 1 Entreprise et update:
		System.out.println("recherche 1 Entreprise :");
		testEntreprise = test.getEntreprise(1);
		System.out.println(testEntreprise);
		System.out.println("Update en BD:");
		testEntreprise.setNom("NomChanger");
		
		SecteurActivite act = new SecteurActivite(0, "nomActivite");
		test.addSecteurActivite(act);
		List<SecteurActivite> listSecteurActivite = testEntreprise.getListSectAct();
		listSecteurActivite.add(act);
		testEntreprise.setListSectAct(listSecteurActivite);
		
		Technologie tech = new Technologie("nomTech", 0);
		test.addTechnologie(tech);
		List<Technologie> listTechnologie = testEntreprise.getListTechno();
		listTechnologie.add(tech);
		testEntreprise.setListTechno(listTechnologie);
		
		test.updateEntreprise(testEntreprise);

		//recherche toutes les Entreprise:
		System.out.println(test.getAllEntreprise());
		
		
		//supprimer une Entreprise
		List<Entreprise> listEntreprise = test.getAllEntreprise();
		testEntreprise = listEntreprise.get(listEntreprise.size()-1);
		System.out.println("Delete en BD de :"+testEntreprise);
		test.removeEntreprise(testEntreprise);
		System.out.println(test.getAllEntreprise());
		*/
		/*
		//Test Contact :
		
		//appel contact
		//creer un contact
		Contact contactTest = new Contact(0, "NomTestAjout", "PrenomTest", "0470121212", "blabla@gmail.com", "postTest", null);
		test.addContact(contactTest);
		System.out.println(contactTest);
		//recherche 1 contact et update:
		contactTest = test.getContact(2);
		System.out.println(contactTest);
		
		contactTest.setNom("changeNom");
		contactTest.setPrenom("ChangePrenom");
		List<Evenement> envs = contactTest.getListEvent();
		envs.remove(envs.size()-1);
		contactTest.setListEvent(envs);
		test.updateContact(contactTest);
		//recherche tous les contacts:
		System.out.println(test.getAllContact());
		//supprimer un contact
		List<Contact> listConct = test.getAllContact();
		contactTest = listConct.get(listConct.size()-1);
		test.removeContact(contactTest);
		System.out.println(test.getAllContact());
		
		*/
		/*
		//Test proposition de stage :
		
		//appel PropositionDeStage
		//creer une proposition
		PropositionDeStage propoTest = new PropositionDeStage(0, "sujetTest", 2017, "descriptionTest", "typeTest", test.getEntreprise(2), "lienFichierTest");
		propoTest.setListTechno(test.getAllTechnologie());
		propoTest.setListTechno(test.getAllTechnologie());
		test.addPropositionDeStage(propoTest);
		System.out.println(propoTest);
		//recherche 1 proposition et update:
		propoTest = test.getPropositionDeStage(2);
		System.out.println(propoTest);
		
		propoTest.setDescription("descriptionChange");
		propoTest.setSujet("sujetChange");
		propoTest.setListTechno(test.getAllTechnologie());
		List<Technologie> technos = propoTest.getListTechno();
		technos.remove(technos.size()-1);
		
		System.out.println(technos);
		
		propoTest.setListTechno(technos);
		test.updatePropositionDeStage(propoTest);
		//recherche tous les proposition:
		System.out.println(test.getAllPropositionDeStage());
		//supprimer un proposition
		List<PropositionDeStage> listPropositionDeStage = test.getAllPropositionDeStage();
		propoTest = listPropositionDeStage.get(listPropositionDeStage.size()-1);
		test.removePropositionDeStage(propoTest);
		System.out.println(test.getAllPropositionDeStage());
		*/
		/*
		//Test Stage
		
		//Test proposition de stage :
		
		//appel stage
		//creer une stage
		
		Stage stageTest = new Stage(0, LocalDate.now(), LocalDate.now(), false, test.getPropositionDeStage(2));
		
		test.addStage(stageTest);
		System.out.println(stageTest);
		//recherche 1 proposition et update:
		stageTest = test.getStage(3);
		System.out.println(stageTest);
		
		stageTest.setRepertorier(true);
		test.updateStage(stageTest);
		//recherche tous les proposition:
		System.out.println(test.getAllStage());
		//supprimer un proposition
		List<Stage> listStage = test.getAllStage();
		stageTest = listStage.get(listStage.size()-1);
		test.removeStage(stageTest);
		System.out.println(test.getAllStage());
		
		*/

        //Test Eleve
        //appel Eleve
        //creer une Eleve

        Eleve eleveTest = new Eleve(0, "emailTest", "nomTest", "prenomTest", "motDePasseTest", true, null, test.getAdresse(3));

        test.addEleve(eleveTest);
        System.out.println(eleveTest);
        //recherche 1 eleve et update:
        eleveTest = test.getProfil(3);
        System.out.println(eleveTest);

        eleveTest.setEmail("emailChange");
        eleveTest.setIsStudent(false);
        test.updateEleve(eleveTest);
        //recherche tous les proposition:
        System.out.println(test.getAllEleve());
        //supprimer un proposition
        List<Eleve> listEleve = test.getAllEleve();
        eleveTest = listEleve.get(listEleve.size() - 1);
        test.removeEleve(eleveTest);
        System.out.println(test.getAllEleve());

    }

}
