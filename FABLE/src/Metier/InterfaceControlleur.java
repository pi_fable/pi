package Metier;

import java.io.File;
import java.sql.Connection;
import java.util.List;

import POJO.Adresse;
import POJO.Contact;
import POJO.Eleve;
import POJO.Entreprise;
import POJO.Evaluation;
import POJO.Evenement;
import POJO.Feedback;
import POJO.Formulaire;
import POJO.MaitreDeStage;
import POJO.Planning;
import POJO.Professeur;
import POJO.PropositionDeStage;
import POJO.Question;
import POJO.Reponse;
import POJO.SecteurActivite;
import POJO.Stage;
import POJO.Technologie;

public interface InterfaceControlleur {
<<<<<<< aud

    //Adresse

    public Adresse getAdresse(int id);

    public List<Adresse> getAllAdresse();

    public boolean removeAdresse(Adresse a);

    public boolean updateAdresse(Adresse a);

    public boolean addAdresse(Adresse a);

    //Contact

    public Contact getContact(int id);

    public List<Contact> getAllContact();

    public boolean updateContact(Contact c);

    public boolean removeContact(Contact c);

    public boolean addContact(Contact c);

    //Eleve

    public Eleve getProfil(int id);

    public boolean addEleve(Eleve e);

    public boolean addEleve(File f);

    public boolean removeEleve(Eleve e);

    public List<Eleve> getAllEleve();

    public boolean updateEleve(Eleve e);

    //Entreprise
    public Entreprise getEntreprise(int id);

    public boolean addEntreprise(Entreprise e);

    public boolean updateEntreprise(Entreprise e);

    public boolean removeEntreprise(Entreprise e);

    public List<Entreprise> getAllEntreprise();


    //Evaluation


    public Evaluation getEvaluation(int id);

    public List<Evaluation> getAllEvaluation();

    public boolean addEvaluation(Evaluation e);

    public boolean updateEvaluation(Evaluation e);

    public boolean removeEvaluation(Evaluation e);

    //Evenement

    public Evenement getEvenement(int id);

    public List<Evenement> getAllEvenement(int annee);

    public boolean addEvenement(Evenement e, String[] participant);

    public boolean updateEvenement(Evenement e, String[] participant);

    public boolean removeEvenement(int id);

    public List<Evenement> searchEvenementBy(String sujet, String annee, String dateDeb, String dateFin);

    //Feedback

    public Feedback getFeedback(int id);

    public List<Feedback> getAllFeedback(int id);

    public boolean addFeedback(Feedback e);

    public boolean updateFeedback(Feedback e);

    public boolean removeFeedback(Feedback e);

    //Formulaire

    public Formulaire getFormulaire(int id);

    public List<Formulaire> getAllFormulaire();

    public boolean addFormulaire(Formulaire e);

    public boolean updateFormulaire(Formulaire e);

    public boolean removeFormulaire(Formulaire e);


    //MaitreDeStage

    public MaitreDeStage getMaitreDeStage(int id);

    public List<MaitreDeStage> getAllMaitreDeStage();

    public boolean addMaitreDeStage(MaitreDeStage e);

    public boolean updateMaitreDeStage(MaitreDeStage e);

    public boolean removeMaitreDeStage(MaitreDeStage e);

    public MaitreDeStage findByIdContact(int idContact);
    //Planning

    public Planning getPlanning(int annee);

    public List<Planning> getAllPlanning();

    public boolean addPlanning(Planning e);

    public boolean updatePlanning(Planning e);

    public boolean removePlanning(Planning e);

    //Professeur

    public Professeur getProfesseur(int id);

    public List<Professeur> getAllProfesseur();

    public boolean addProfesseur(Professeur e);

    public boolean updateProfesseur(Professeur e);

    public boolean removeProfesseur(Professeur e);

    //PropositionDeStage

    public PropositionDeStage getPropositionDeStage(int id);

    public List<PropositionDeStage> getAllPropositionDeStage();

    public boolean addPropositionDeStage(PropositionDeStage e);

    public boolean updatePropositionDeStage(PropositionDeStage e);

    public boolean removePropositionDeStage(int id);

    //Question

    public Question getQuestion(int id);

    public List<Question> getAllQuestion(int idForm);

    public boolean addQuestion(Question e);

    public boolean updateQuestion(Question e);

    public boolean removeQuestion(Question e);

    //Reponse

    public Reponse getReponse(int id);

    public List<Reponse> getAllReponse(int numEva);

    public boolean addReponse(Reponse e);

    public boolean updateReponse(Reponse e);

    public boolean removeReponse(Reponse e);

    //SecteurActivite

    public SecteurActivite getSecteurActivite(int id);

    public List<SecteurActivite> getAllSecteurActivite();

    public List<SecteurActivite> getAllSecteurActivite(int idEntreprise);

    public boolean addSecteurActivite(SecteurActivite e);

    public boolean updateSecteurActivite(SecteurActivite e);

    public boolean removeSecteurActivite(SecteurActivite e);

    //Stage

    public Stage getStage(int id);

    public List<Stage> getAllStage();

    public boolean addStage(Stage e);

    public boolean updateStage(Stage e);

    public boolean removeStage(int id);

    //Technologie

    public Technologie getTechnologie(int id);

    public List<Technologie> getAllTechnologie();

    public List<Technologie> getAllTechnologieEntreprise(int idEntreprise);

    public List<Technologie> getAllTechnologiePropStage(int idProp);

    public boolean addTechnologie(Technologie e);

    public boolean updateTechnologie(Technologie e);

    public boolean removeTechnologie(Technologie e);

    //Recherche

    public List<Entreprise> rechercherEntreprise(String[] param);

    public List<PropositionDeStage> rechercherPropositionDeStage(String[][] param);

    public List<Stage> rechercherStage(String[] param);

    public List<Contact> rechercherContact(String[] param);


    //Utilisateur
    public Connection getConnection();

    public boolean connect(String email, String password);

    public boolean disconnect(int idUti, String type);

    //Stage (professeur)

    public boolean validerStage(Stage s, Eleve e, MaitreDeStage m);

    //PropositionDeStage (Eleve)

    public boolean interetStage(Eleve e, PropositionDeStage p);


=======
		
	//Adresse
	
	public Adresse getAdresse(int id);
	public List<Adresse>getAllAdresse();
	public boolean removeAdresse(Adresse a);
	public boolean updateAdresse(Adresse a);
	public boolean addAdresse(Adresse a);
	
	//Contact
	
	public Contact getContact(int id);
	public List<Contact>getAllContact();
	public boolean updateContact(Contact c);
	public boolean removeContact(Contact c);
	public boolean addContact(Contact c);
	
	//Eleve
	
	public Eleve getProfil(int id);
	public boolean addEleve(Eleve e);
	public boolean addEleve(File f);
	public boolean removeEleve(Eleve e);
	public List<Eleve> getAllEleve();
	public boolean updateEleve(Eleve e);
	
	//Entreprise
	public Entreprise getEntreprise(int id);
	public boolean addEntreprise(Entreprise e,String[] tech,String[]act);
	public boolean updateEntreprise(Entreprise e,String[] tech,String[]act);
	public boolean removeEntreprise(int id);
	public List<Entreprise>getAllEntreprise();
	
	
	//Evaluation
	
	
	public Evaluation getEvaluation(int id);
	public List<Evaluation>getAllEvaluation();
	public boolean addEvaluation(Evaluation e);
	public boolean updateEvaluation(Evaluation e);
	public boolean removeEvaluation(Evaluation e);
	
	//Evenement
	
	public Evenement getEvenement(int id);
	public List<Evenement>getAllEvenement(int annee);
	public boolean addEvenement(Evenement e,String[]participant);
	public boolean updateEvenement(Evenement e,String[]participant);
	public boolean removeEvenement(int id);
	public List<Evenement>searchEvenementBy(String sujet,String annee,String dateDeb,String dateFin);
	
	//Feedback
	
	public Feedback getFeedback(int id);
	public List<Feedback>getAllFeedback(int id);
	public boolean addFeedback(Feedback e);
	public boolean updateFeedback(Feedback e);
	public boolean removeFeedback(Feedback e);
	
	//Formulaire
	
	public Formulaire getFormulaire(int id);
	public List<Formulaire>getAllFormulaire();
	public boolean addFormulaire(Formulaire e);
	public boolean updateFormulaire(Formulaire e);
	public boolean removeFormulaire(Formulaire e);
	
	
	//MaitreDeStage
	
	public MaitreDeStage getMaitreDeStage(int id);
	public List<MaitreDeStage>getAllMaitreDeStage();
	public boolean addMaitreDeStage(MaitreDeStage e);
	public boolean updateMaitreDeStage(MaitreDeStage e);
	public boolean removeMaitreDeStage(MaitreDeStage e);
	public MaitreDeStage findByIdContact(int idContact);
	//Planning
	
	public Planning getPlanning(int annee);
	public List<Planning>getAllPlanning();
	public boolean addPlanning(Planning e);
	public boolean updatePlanning(Planning e);
	public boolean removePlanning(Planning e);
	
	//Professeur
	
	public Professeur getProfesseur(int id);
	public List<Professeur>getAllProfesseur();
	public boolean addProfesseur(Professeur e);
	public boolean updateProfesseur(Professeur e);
	public boolean removeProfesseur(Professeur e);
	
	//PropositionDeStage
	
	public PropositionDeStage getPropositionDeStage(int id);
	public List<PropositionDeStage>getAllPropositionDeStage();
	public boolean addPropositionDeStage(PropositionDeStage e);
	public boolean updatePropositionDeStage(PropositionDeStage e);
	public boolean removePropositionDeStage(int id);
	
	//Question
	
	public Question getQuestion(int id);
	public List<Question>getAllQuestion(int idForm);
	public boolean addQuestion(Question e);
	public boolean updateQuestion(Question e);
	public boolean removeQuestion(Question e);
	
	//Reponse
	
	public Reponse getReponse(int id);
	public List<Reponse>getAllReponse(int numEva);
	public boolean addReponse(Reponse e);
	public boolean updateReponse(Reponse e);
	public boolean removeReponse(Reponse e);
	
	//SecteurActivite
	
	public SecteurActivite getSecteurActivite(int id);
	public List<SecteurActivite>getAllSecteurActivite();
	public List<SecteurActivite>getAllSecteurActivite(int idEntreprise);
	public boolean addSecteurActivite(SecteurActivite e);
	public boolean updateSecteurActivite(SecteurActivite e);
	public boolean removeSecteurActivite(SecteurActivite e);
	
	//Stage
	
	public Stage getStage(int id);
	public List<Stage>getAllStage();
	public boolean addStage(Stage e);
	public boolean updateStage(Stage e);
	public boolean removeStage(int id);
	
	//Technologie
	
	public Technologie getTechnologie(int id);
	public List<Technologie>getAllTechnologie();
	public List<Technologie>getAllTechnologieEntreprise(int idEntreprise);
	public List<Technologie>getAllTechnologiePropStage(int idProp);
	public boolean addTechnologie(Technologie e);
	public boolean updateTechnologie(Technologie e);
	public boolean removeTechnologie(Technologie e);
	
	//Recherche
	
	public List<Entreprise> rechercherEntreprise(String[]param);
	public List<PropositionDeStage> rechercherPropositionDeStage(String[]param);
	public List<Stage> rechercherStage(String[]param);
	public List<Contact> rechercherContact(String[]param);
	
	//Utilisateur
	public Connection getConnection();
	public boolean connect(String email,String password);
	public boolean disconnect(int idUti,String type);
	
	//Stage (professeur)
	
	public boolean validerStage(Stage s,Eleve e,MaitreDeStage m);
	
	//PropositionDeStage (Eleve)
	
	public boolean interetStage(Eleve e,PropositionDeStage p);
		
	
	
	
>>>>>>> entreprise
}
